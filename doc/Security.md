## Examples of usage

@see:   easy\web\Security

use htmLawed.

例子：

  Safest, allowing only safe HTML markup --

    $config = array('safe'=>1);


  Simplest, allowing all valid HTML markup except javascript: --



  Allowing all valid HTML markup including javascript: --

    $config = array('schemes'=>'*:*');


  Allowing only safe HTML and the elements a, em, and strong --

    $config = array('safe'=>1, 'elements'=>'a, em, strong');


  Not allowing elements script and object --

    $config = array('elements'=>'* -script -object');


  Not allowing attributes id and style --

    $config = array('deny_attribute'=>'id, style');


  Permitting only attributes title and href --

    $config = array('deny_attribute'=>'* -title -href');


  Remove bad/disallowed tags altogether instead of converting them to entities --

    $config = array('keep_bad'=>0);


  Allowing attribute title only in a and not allowing attributes id, style, or scriptable on* attributes like onclick --

    $config = array('deny_attribute'=>'title, id, style, on*');
    $spec = 'a=title';


  Allowing a custom attribute, vFlag, in img and permitting custom use of the standard attribute, rel, in input --

    $spec = 'img=vFlag; input=rel';


  Some case-studies are presented below.

  1. A blog administrator wants to allow only a, em, strike, strong and u in comments, but needs strike and u transformed to span for better XHTML 1-strict compliance, and, he wants the a links to point only to http or https resources:

    $processed = htmLawed($in, array('elements'=>'a, em, strike, strong, u', 'make_tag_strict'=>1, 'safe'=>1, 'schemes'=>'*:http, https'), 'a=href');

  2. An author uses a custom-made web application to load content on his web-site. He is the only one using that application and the content he generates has all types of HTML, including scripts. The web application uses htmLawed primarily as a tool to correct errors that creep in while writing HTML and to take care of the occasional bad characters in copy-paste text introduced by Microsoft Office. The web application provides a preview before submitted input is added to the content. For the previewing process, htmLawed is set up as follows:

    $processed = htmLawed($in, array('css_expression'=>1, 'keep_bad'=>1, 'make_tag_strict'=>1, 'schemes'=>'*:*', 'valid_xhtml'=>1));

  For the final submission process, keep_bad is set to 6. A value of 1 for the preview process allows the author to note and correct any HTML mistake without losing any of the typed text.

  3. A data-miner is scraping information in a specific table of similar web-pages and is collating the data rows, and uses htmLawed to reduce unnecessary markup and white-spaces:

    $processed = htmLawed($in, array('elements'=>'tr, td', 'tidy'=>-1), 'tr, td =');
