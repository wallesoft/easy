<?php

use PHPUnit\framework\TestCase;

use Easy as easy;
use easy\web\Response;
/**
 * response test
 */
class ResponseTest extends TestCase
{

  public function testSend()
  {
    $data = ['a'=>1,'b'=>2];
    $response = new Response(); //easy::$app->getResponse();
    $response->format = 'json';
    $response->data = $data;
    $response->prepare();
    $this->assertSame('{"a":1,"b":2}',$response->content);
  }
}
