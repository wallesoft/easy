<?php
use PHPUnit\framework\TestCase;

//use Easy;
use easy\base\Module;
use easy\web\Application;
class ModuleTest extends TestCase{
  public function testSetByConfig(){
    $config = [
    'appPath' => dirname(__FILE__),
    'modules' => [
      'test' => [
        'class' =>'testModule'
      ]
    ]
  ];
  $app = new Application($config);
  $module = $app->getModule('test');
  print_r($module);
  $this->assertSame('test',$module->id);

  }

}

class TestModule extends Module{

}
