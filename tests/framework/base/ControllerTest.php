<?php
use PHPUnit\framework\TestCase;

use Easy as easy;
use easy\base\Controller;
class ControllerTest extends TestCase{
  public function testRunAction(){
    $controller = new TestController('test',easy::$app);
    $result = $controller->runAction('test1');
    $this->assertEquals('test1',$result);
    $result = $controller->runAction('test2',['id'=>1,'uid'=>2]);
    $this->assertEquals(['test2','id'=>1],$result);
    $result = $controller->runAction('test1',['id'=>1]);
    $this->assertEquals('test1',$result);
  }
}

class TestController extends Controller{
  public function actionTest1(){
    return 'test1';
  }
  public function actionTest2($id){
    return ['test2','id'=>$id];
  }
}
 ?>
