<?php
use PHPUnit\framework\TestCase;

use Easy as easy;
use easy\helpers\Json;

/**
 * test helper json
 */
class JsonTest extends TestCase
{
  public function testEncode(){
    //empty data encode
    $dataArray = [];
    $result = Json::encode($dataArray);
    $this->assertEquals('[]',$result);

     //test encode basic  like string
     $data = '1';
     $this->assertEquals('"1"',Json::encode($data));

     //test Array
     $data = [1, 2];
     $this->assertEquals('[1,2]',Json::encode($data));
     $data = ['a'=>1, 'b'=>2];
     $this->assertEquals('{"a":1,"b":2}',Json::encode($data));

     //test object
     $data = new \stdClass();
     $data->a = 1;
     $data->b = 2;
     $this->assertEquals('{"a":1,"b":2}',Json::encode($data));

     //test empty
     $data = new \stdClass();
     $this->assertEquals('{}',Json::encode($data));

  }
  public function testDecode(){
    // test null
    $data = '';
    $result = Json::decode($data);
    $this->assertNull($result);

    //test string
    $data = '"1"';
    $result = Json::decode($data);
    $this->assertSame('1',$result);

    //test json
    $data = '{"a":1,"b":2}';
    $result = Json::decode($data);
    $this->assertSame(['a'=>1,'b'=>2], $result);

    //test exception
    $data = '{"a":1 ,"b":2';
    Json::decode($data);

  }
}
