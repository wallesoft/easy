<?php
use PHPUnit\framework\TestCase;

use Easy as easy;
use easy\helpers\Helper;
class HelperTest extends TestCase
{
    public function testReadFileList(){
      $path = 'd:/wwwroot/easy/framework/di';
      $list = Helper::readFileList($path);
      $this->assertSame(['d:/wwwroot/easy/framework/di'.DIRECTORY_SEPARATOR.'ServiceLocator.php'],$list);
    }
}
