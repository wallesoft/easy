<?php
/**
 *  File 缓存
 *
 * @author jackhunx
 * @since  v1.0
 *
 */

namespace easy\caching;

use easy;
use easy\helpers\File;
use easy\helpers\Helper;
class FileCache extends Cache
{
  /**
   * cache 存储路径
   * @var string
   */
    public $path;
    /**
     * 缓存扩展名
     * @var string
     */
    public $extention='.php';

    /**
     * 读取缓存
     * @param  string  $key      key of cache
     * @param  boolean|callback $callback 是否使用回调及回调函数参数
     * @return mixed              去得的缓存
     */
    public function read($key,$callback = false){
      $value = $this->get($key);
      if($value === false && $callback !== false){
          $callback = $this->getCallback($callback);
          $value = call_user_func($callback,$key);
          $this->write($key,$value);

      }
      return $value;
    }
    //获取缓存
    public function get($key,$path=null){
      $filename = $this->_path($key);
      if (is_file($filename)){
  			return require($filename);
  		}else{
  			return false;
  		}
    }
    /**
     * @todo 添加过期时间的支持
     */
    public function write($key,$value)
    {
      $filename = $this->_path($key);
      if (false == File::write($filename,$value)){
        return false;
      }else{
        return true;
      }
    }
    /**
     * 删除缓存
     * @param  string $key 缓存标签
     * @return bool      true or false
     */
    public function rm($key)
    {
      $filename=$this->_path($key);
      if (is_file($filename)) {
  			@unlink($filename);
  		}else{
  			return false;
  		}
  		return true;
    }
    /**
     * 删除指定目录及子目录下的缓存文件
     */
    public function rmFiles($dir){
      if(strpo($dir,'..')!==false) return false;
      $path = $this->path.DIRECTORY_SEPARATOR.$dir;
      if(is_dir($path)){
        $file_list = Helper::readFileList($path);
        if (!empty($file_list)){
          foreach ($file_list as $v){
            if (basename($v) != 'index.html')@unlink($v);
          }
        }
      }else{
        if (basename($path) != 'index.html')@unlink($path);
      }
      return false;
    }
    /**
     * 获取缓存文件
     */
    private function _path($key)
    {
      return $this->path.'/'.$key.$this->extention;
    }
}
