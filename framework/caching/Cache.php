<?php

/**
 * 缓存操作类
 *
 * @author jackhunx
 * @since  v1.0
 */

namespace easy\caching;

use easy;
use easy\base\Component;
use easy\base\Exception;
class Cache extends Component
{
  /**
   * 回调处理
   * @param  array $callback [class,function]
   * @return callable           callabel array use for call_user_func
   */
  public function getCallback($callback){
      $class = easy::createObject($callback[0]);
      $callback = [$class,$callback[1]];
      if(!is_callable($callback)){
        // TODO:  此处需要优化
        throw new Exception('Error: Cache callback error !!!');
      }
      return $callback;

  }
  // /**
  //  * 回调类名
  //  *
  //  * 在配置文件中设置此参数 或者在有需要请求作回调时通过 setCallbackClass()方法设置
  //  * set p.s : 'app\model\callback',or array
  //  * @todo 是否可以再优化
  //  * @var string|array
  //  */
  // public $callBackClass = '';
  //
  // //private $_callback;
  // /**
  //  * 是否设置回调class
  //  * @return bool [description]
  //  */
  //
  // public function getCallbackClass(){
  //   if(!$this->callBackClass){
  //     return false;
  //   }
  //   return true;
  // }
  // /**
  //  * [setCallbackClass description]
  //  * @param [type] $class [description]
  //  */
  // public function setCallbackClass($class){
  //
  // }
  // /**
  //  * 由于callbackclass配置时不能确定其命名空间及位置，通过
  //  * 此方法获取并引用callbackclass
  //  */
  // public function getCallbackClassInstance($callbackClass = false){
  //   // is not set callbackclass in config return false
  //   if(!isset($this->callBackClass)){
  //     return false;
  //   }
  //
  //   if($callbackClass !== false){
  //
  //   }
  //   return easy::createObject($this->callBackClass);
  // }
}
