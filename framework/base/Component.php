<?php

namespace easy\base;

use easy;
use easy\base\Exception;
class Component extends EObject{

    /**
     * 设置相关component组件的相关参数
     *
     * 自动过滤Application没有的参数，设置的参数为可写参数
     * 1.当参数存在对应的方法 get.$name 和 set.$name 时 则参数为可读可写.
     * 2.当只存在get.$name 时，表明此参数为只读属性，设置此参数将出错误.
     * 3.如果没有过对应的set.$name 和 get.$name 方法则自动过滤此配置不做处理.
     * @param $name set name
     * @param $value set value
     * @throws Exception
     */
    public function __set($name,$value){
        $setter = "set".$name;
        if(method_exists($this,$setter)){
            //设置属性
            $this->$setter($value);
            return;
        }else{
            //

        }
        if(method_exists($this,'get'.$name)){
            throw new Exception("无法设置只读属性:".$name);
        }


    }
    /**
     * magic 方法
     *
     * @param  string $name 公共变量名称
     * @return mixed       变量值
     * @todo 抛出异常处理一下
     */
    public function __get($name){
      $method = 'get'.$name;
      if(method_exists($this,$method)){
        return $this->$method();
      }elseif(method_exists($this,'get')){
        return $this->get($name);
      }else{

       throw new Exception($this->className().' Attribute:{'.$name.'} is not available! ');
      }

    }
}
