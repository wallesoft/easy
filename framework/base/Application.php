<?php
/**
 * @link http://git.oschina.com/jackhunx/easyPHP
 */

/**
 * @since 1.0.0
 */

namespace easy\base;

use easy;
use easy\web\Security;

abstract class Application extends Module
{

    /**
     * @var string 应用控制器的默认存储路径
     */
    private $_controllerPath;
    /**
     * @var string 应用视图模板的存储路径
     */
    private $_viewPath;
    /**
     * @var array 路由
     */
    protected $_router;
    /**
     * @var string 布局文件的存储路径
     */
    private $_layoutPath;
    /**
     * charset.
     *
     * @var string  编码
     */
    public $charset;
    /**
     * 当前活动的controller 实例
     * @var [type]
     */
    public $controller;
      /**
       * 配置参数存储.
       *
       * @var array
       */
    private $_settingConfig = [];
   /**
    * @var string 应用控制器所在目录的命名空间
    * 通过此命名空间来寻找相应的控制器
    * 默认 `app\controllers`.
    */
   public $controllerNamespace = 'app\\controllers';

    public function __construct($config = null)
    {
        easy::$app = $this;

        //做一些初始化工作
        $this->preInit($config);
        //component 初始化配置要放在前面，否则将无法初始化各个组件
        Component::__construct($config);

        $this->_settingConfig = $this->parseconf($config);
    }

    /**
     * 运行应用
     */
    public function run()
    {
        //初始化，设置路径
        $this->init();
        /*
         * @todo if $this->_router 不是数组
         */

        $this->handleRequest(easy::$app->getRequest());
    }
    /**
     * resolve the request
     * @var [type]
     */
    abstract public function handleRequest($request);
    /**
     * 初始化前操作
     * 配置相关的应用路径 编码 以及相关组建的配置合并为初始化前做准备
     * @param $config 引用$config
     */
    public function preInit(&$config)
    {
        if (isset($config['appPath'])) {
            $this->setBasePath($config['appPath']);
            unset($config['appPath']);
        } else {
            die('config [appPath] is required');
        }
        if (isset($config['charset'])) {
            $this->_charset = $config['charset'];
        }

        // merge core components with custom components
      foreach ($this->coreComponents() as $id => $component) {
          if (!isset($config['components'][$id])) {
              $config['components'][$id] = $component;
          } elseif (is_array($config['components'][$id]) && !isset($config['components'][$id]['class'])) {
              $config['components'][$id]['class'] = $component['class'];
          }
      }
    }
   /**
    * 获取数据库app配置.
    *
    * @return [type] [description]
    */
   private function parseConf($config)
   {
       //do something under

      //去除系统敏感配置 components
       unset($config['components']);

       return $config; //array_merge_recursive($setting, $config);
   }

    public function init()
    {
        //解析路由
        //$urlManager = new easy\web\UrlManager();
        //$this->_router = $urlManager->parsesRequest(new easy\web\Request());
    }

    /**
     * 设置模板路径
     * @param $path view 模板的路径
     */
    public function setViewPath($path)
    {
        $this->_viewPath = Easy::getAlias($path);
    }
    /**
     * 设置控制器的路径
     * @todo需要合理化优化
     */
    public function setControllerPath()
    {
        $this->_controllerPath = Easy::getAlias('@app').DIRECTORY_SEPARATOR.'controllers';
    }

    /**
     * 运行一个类
     * @param $routerInstance
     */
    public function runObject($routerInstance)
    {
        call_user_func($routerInstance);
    }


    /**
     * 返回request 类
     *
     * @return object request实例
     */
    public function getRequest()
    {
        return $this->get('request');
    }
    /**
     * 获取urlManager 实例.
     *
     * @return object [description]
     */
    public function getUrlManager()
    {
        return $this->get('urlManager');
    }
    /**
     * 获取数据路链接实例
     * @return db connection instance
     */
    public function getDb()
    {
        return $this->get('db');
    }
    /**
     * 语言实例类.
     *
     * @return instance 初始化后的语言类
     */
    public function getLanguage()
    {
        return $this->get('language');
    }
    /**
     * 获取缓存类.
     *
     * @return instance 初始化后的cache缓存
     */
    public function getCache()
    {
        return $this->get('cache');
    }
    /**
     * 获取配置信息.
     *
     * @return array 配置信息
     */
    public function getConfig($key)
    {
        if (strpos($key, '.')) {
            $key = explode('.', $key);
            if (isset($key[2])) {
                return $this->_settingConfig[$key[0]][$key[1]][$key[2]];
            } else {
                return $this->_settingConfig[$key[0]][$key[1]];
            }
        } else {
            return $this->_settingConfig[$key];
        }
    }
    /**
     * set config
     * @param array $config 要设置的配置信息
     */
    public function setConfig(array $config){
      $this->_settingConfig = array_merge_recursive($this->_settingConfig,$config);
    }
    /**
     * 设置基本路径
     * @param string $path 实际路径
     */
    public function setBasePath($path){
      parent::setBasePath($path);
      easy::setAlias('@app',$this->getBasePath());
    }
    /**
     * 以数组的方式返回应用的相关配置 不包括相关组件的配置
     * return $this->_settingConfig;.
     */
    public function getAllConfig()
    {
        return $this->_settingConfig;
    }
    /**
     * 设置布局文件的路径
     * @param $path
     */
    public function setLayoutPath($path)
    {
        $this->_layoutPath = Easy::getAlias($path);
    }

    /**
     * 获取布局文件的路径
     * @return string
     */
    public function getLayoutPath()
    {
        if ($this->_layoutPath !== null) {
            return $this->_layoutPath;
        } else {
            return $this->_layoutPath = $this->getBasePath().DIRECTORY_SEPARATOR.'views/layouts';
        }
    }

    /**
     * Returns the configuration of core application components.
     * 返回系统组件的默认配置
     * @see set()
     */
    public function coreComponents()
    {
        return [
            'urlManager' => ['class' => 'easy\web\UrlManager'],
            'session' => ['class' => 'easy\web\Session'],
            'request'=>['class'=>'easy\web\Request'],
        ];
    }
    /**
     * 获取上一步来源地址
     *
     * @todo 此方法可能弃用  将以 easy\web\Request::getReferer()替代
     * @return string 返回结果
     */
    public function getReferer()
    {
        return empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
    }
}
