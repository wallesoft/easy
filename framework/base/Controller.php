<?php
/**
 * 实现方式：
 *  1.变量传递  通过将变量存储在相应的
 *
 */
namespace easy\base;

use easy;
use easy\base\Component;
use easy\helpers\Url;
/**
 * base Controller.
 *
 * @todo 做一些优化措施
 */
class Controller extends Component
{
    /**
     * controller id.
     * @var [type]
     */
    public $id;
    /**
     * 默认 action.
     */
    public $defaultAction = 'Index';
    /**
     * 当前执行的action
     */
    public $action;
    /**
     * 布局
     * @var string layout
     */
    public $layout;
    /**
     * 此controller属于哪个module
     * @var [type]
     */
    public $module;
    /**
     * @var View类 用来渲染view和view 文件.
      */
    private $_view;
    //action params
    public $actionParams;
    /**
     * 设置相关的属性并执行父类方法
     * @param [type] $id     [description]
     * @param [type] $module [description]
     * @param [type] $config [description]
     */
    public function __construct($id,$module,$config=[]){
      $this->id = $id;
      $this->module = $module;
      parent::__construct($config);
    }
    /**
     * 设置action
     */
    public function setAction($action){
      $this->action = $action === '' ? $this->defaultAction : $action;
    }
    public function getAction(){
      return $this->action;
    }
    /**
     * view file 在以controller id 命名的文件夹，此文件夹在相应的module的view模板目录下
     * @return [type] [description]
     */
    public function getViewPath(){
      return $this->module->getViewPath() . DIRECTORY_SEPARATOR ;
    }
    /**
     * 输出变量名.
     *
     * @param mixed $output 输出的变量名称
     * @param void  $input  输出变量值
     */
    public function output($output, $input = '')
    {
        $this->getView()->output($output, $input);
    }
    /**
     * 渲染 如果没有layout  只渲染view
     * @param  string  $view 模板路径
     * @param  integer $time [description]
     * @return [type]        [description]
     */
    public function render($view)
    {
        //1.获取view 实例
        //2、获取layout
        //3、渲染

        $layoutFile = $this->findLayoutFile($this->getView());
        if($layoutFile !== false){
          $this->getView()->render($view, $layoutFile);
        }else{
          $this->getView()->render($view);
        }

    }
    /**
     * 不加载布局文件直接渲染
     * @param  [type] $view [description]
     * @return [type]       [description]
     */
    public function renderPartial($view){
        return $this->getView()->render($view);
    }
    /**
     * 不显示信息直接跳转
     * @see easy\helpers\Url::to
     * @todo 如果需要信息提示的情况下如何跳转
     * @param string $url
     */
    public function redirect($url = '', $scheme = false, $host = false){
      $url=Url::to($url,$scheme,$host);
      // TODO: 此处需改进 添加进去route规则
      if (empty($url)) {
        if(!empty($_REQUEST['ref_url'])) {
          $url = $_REQUEST['ref_url'];
        } else {
          $url = easy::$app->request->getReferer();
        }
      }

      header('Location: '.$url);
      exit();
    }
    /**
     * 获取模板文件
     * @return easy\web\View object
     */
    public function getView()
    {
        if ($this->_view === null) {
            $this->_view = Easy::$app->getView();
        }

        return $this->_view;
    }

    /**
     * 设置模板文件
     * @param $view
     */
    public function setView($view)
    {
        $this->_view = $view;
    }
    /**
     * 执行actoin方法
     * @param  string $actionID action id
     * @param  [type] $params   [description]
     * @return [type]           [description]
     */
    public function runAction($actionID,$params=[]){
      $action = $this->createActionById($actionID);
      if($this->beforeAction($action)){
        //run action with params
        $result = $this->runWithParams($action,$params);
        $result = $this->afterAction($action,$result);
      }
      return $result;
    }
    public function runWithParams($action,$params){
      $args = $this->bindActionParams($action,$params);
      return call_user_func_array([$this,$action],$args);
    }
    /**
     * [bindActionParams description]
     * @param  [type] $params [description]
     * @return [type]         [description]
     */
    public function bindActionParams($action,$params){
      $method = new \ReflectionMethod($this,$action);
      $args = []; //传递的参数
      $missing = []; //缺少的参数
      $actionParams = [];
      foreach($method->getParameters() as $param){
        $name = $param->getName();
        if(array_key_exists($name,$params)){
            if($param->isArray()){
              $args[] = $actionParams[$name] = (array) $params[$name];
            } elseif (!is_array($params[$name])){
              $args[] = $actionParams[$name] = $params[$name];
            } else {
              throw new \Exception('Invalid data request for param {$name}');
          }
          unset($params[$name]);
        } elseif ($param->isDefaultValueAvailable()){
          $args[] = $actionParams[$name] = $param->getDefaultValue();
        } else {
          $missing[] = $name;
        }
      }
      if(!empty($missing)){
        throw new \Exception('missing action params '.implode(',',$missing));
      }
      $this->actionParams = $actionParams;
      return $args;
    }
   /**
    * ```php
    * public function beforeAction($action)
    * {
    *     // your custom code here, if you want the code to run before action filters,
    *     // which are triggered on the [[EVENT_BEFORE_ACTION]] event, e.g. PageCache or AccessControl
    *
    *     if (!parent::beforeAction($action)) {
    *         return false;
    *     }
    *
    *     // other custom code here
    *
    *     return true; // or false to not run the action
    * }
    * ```
    */
    public function beforeAction($action){
      //@todo do sth
      return true;
    }
    /**
     * ```php
     * public function afterAction($action, $result)
     * {
     *     $result = parent::afterAction($action, $result);
     *     // your custom code here
     *     return $result;
     * }
     * ```
     * action 执行完成后的方法
     * @return [type] [description]
     */
    public function afterAction($action,$result){
      //TODO
      return $result;
    }
    /**
     * 通过action id 创建 action
     * 如果 id === null 将返回默认的action [index]
     * 如果更改默认的action 请在控制器中调用用 $this->defaultAction = 'newAction'
     * @param  string $id action id
     * @return return action     [description]
     */
    public function createActionById($actionID){
      if($actionID === ''){
        $actionID = $this->defaultAction;
      }
      $action = 'action'.str_replace(' ', '', ucwords(str_replace(['-','_'], ' ', $actionID)));
      // if action exist
      if(method_exists($this,$action)){
        return $action;
      }else{
      // TODO: 请求不存在时
          easy\helpers\Helper::requestNotFound();
      }


    }
    /**
     * 获取layout文件
     * @param  object $view View object
     * @return string       loyout path
     */
    public function findLayoutFile($view){
        $module = $this->module;
        if(is_string($this->layout)){
          $layout = $this->layout;
        }elseif($this->layout === null){
            // 查找上层module中是否设置layout
            while($module !== null && $module->layout === null){
              //上层layout为空时是否还有上级module
              $module = $module->module;
              //当循环到没有或者做上层或layout不为空是跳出
            }
            if($module !==null && is_string($module->layout)){
              $layout = $module->layout;
            }else{
              $module = easy::$app;
              $layout = easy::$app->layout;
            }
        }
        if(!isset($layout)){
          return false;
        }
        if (strncmp($layout, '@', 1) === 0) {
            $file = easy::getAlias($layout);
        } elseif (strncmp($layout, '/', 1) === 0) {
            $file = easy::$app->getLayoutPath() . DIRECTORY_SEPARATOR . substr($layout, 1);
        } else {
            $file = $module->getLayoutPath() . DIRECTORY_SEPARATOR . $layout;
        }

        if (pathinfo($file, PATHINFO_EXTENSION) !== '') {
            return $file;
        }
        $path = $file . '.' . $view->defaultExtension;
        if ($view->defaultExtension !== 'php' && !is_file($path)) {
            $path = $file . '.php';
        }

        return $path;
    }
}
