<?php
/**
 * @link http://git.oschina.com/jackhunx/easyPHP
 */
namespace easy\base;

use easy;

/**
 * Class Object
 *
 * @package easy\base
 */
class EObject
{

    /**
     *
     * @param null $config
     */
    public function __construct($config=null){

        if(!empty($config)){
            easy::configure($this,$config);
        }
        $this->init();
    }
    public function init(){

    }
    /**
     * Returns the fully qualified name of this class.
     * @return string the fully qualified name of this class.
     */
    public static function className()
    {
        return get_called_class();
    }

    public function getClassName(){
        return get_class($this);
    }
}
