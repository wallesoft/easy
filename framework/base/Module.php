<?php

/**
 * Module 基础类.
 *
 * 此类借鉴yii2 中module处理方式，具体处理方法参考yii2 Module
 * app 中module 需继承此类或此类的子类
 */
namespace easy\base;

use easy;
use easy\di\ServiceLocator;
use easy\base\Exception;
class Module extends ServiceLocator
{
    public $id;
    /**
     * default router if action not given or not find the action
     * set action as [default].
     *
     * @var string
     */
    public $defaultRoute = 'default';

    /**
     * if controller layout not set will find here
     */
    public $layout;
    /**
     * parent moudle
     * @var [type]
     */
    public $module;
    /**
     * controller namespace if not set default is under app\module\controllers;.
     *
     * @var [type]
     */
    public $controllerNamespace;
  /**
   * 基本地址
   * @var [type]
   */
    private $_basePath;
    /**
     * child modules.
     *
     * @var array
     */
    private $_modules;
    /**
     * view path.
     *
     * @var [type]
     */
    private $_viewPath;
    /**
     * layout path
     * @var [type]
     */
    private $_layoutPath;


  public function setBasePath($path){
    $path = easy::getAlias($path);
    $p = realpath($path);
    if($p!==false && is_dir($p)){
      $this->_basePath = $p;
    }else{
      // TODO: change Exception
      throw new Exception("The dir is not exist:".$path);
    }
  }
  /**
   * Returns the root directory of the module.
   * It defaults to the directory containing the module class file.
   *
   * @return string the root directory of the module
   */
  public function getBasePath()
  {
      if ($this->_basePath === null) {
          $class = new \ReflectionClass($this);
          $this->_basePath = dirname($class->getFileName());
      }
      return $this->_basePath;
  }
    /**
     * initialize the module
     * if not set controllerNamespace here is initialize [controllerNamespace].
     *
     * @return [type] [description]
     */
    public function init()
    {
        if ($this->controllerNamespace === null) {
            $class = get_class($this);
            if (($pos = strrpos($class, '\\')) !== false) {
                $this->controllerNamespace = substr($class, 0, $pos).'\\controllers';
            }
        }
    }
    /**
     * set child modules;.
     *
     * @param array $modules child modules;
     */
    public function setModules($modules)
    {
        foreach ($modules as $id => $module) {
            $this->_modules[$id] = $module;
        }
    }
    public function getModules()
    {
        return $this->_modules;
    }
/**
 * 通过路由运行一个action.
 *
 * @param string $route 路由 p.s module/Controller/Action
 *
 * @return mixed 执行结果
 */
public function runAction($route,$params = [])
{
    //create controller
  $parts = $this->createController($route);
    if (is_array($parts)) {
        list($controller, $actionID) = $parts;
        easy::$app->controller = $controller;
        //将ation赋值个执行controller
        $controller->setAction($actionID);
        //run action
        $controller->runAction($actionID,$params);
    } else {
        if($route == '') $route = 'default';
        easy::$app->view->showException('Unable to resolve the request "'.$route.'".');
    }
}
/**
 * 执行controller.
 *
 * module比controller具有优先级
 *
 * @param [type] $route [description]
 *
 * @return [type] [description]
 */
public function createController($route)
{
    // 处理一下route
    if ($route === '') {
        $route = $this->defaultRoute;
    }
    // double slashes or leading/ending slashes may cause substr problem
    $route = trim($route, '/');
    if (strpos($route, '//') !== false) {
        return false;
    }
    if (strpos($route, '/') !== false) {
        list($id, $route) = explode('/', $route, 2);
    } else {
        $id = $route;
        $route = '';
    }
    //module take precedence
    $module = $this->getModule($id);

    if ($module !== null) {
        return $module->createController($route);
    }
    $controller = $this->createControllerByID($id);
    return $controller === null ? false : [$controller, $route];
}
    /**
     * get module if module is exist.
     *
     * @param string $id   module is
     * @param bool   $load if module is exist load or not load
     *
     * @return Module|null the module instance ,null if the module does not exist
     */
    public function getModule($id, $load = true)
    {
        // TODO: if is sub-module do sth
      if (isset($this->_modules[$id])) {
          // if is a module instanceof class [Module] return module
        if ($this->_modules[$id] instanceof self) {
            return $this->_modules[$id];
        } elseif ($load) {
            // if not an instance and load is true load it
            if(is_string($this->_modules[$id])) {
              $module = easy::createObject($this->_modules[$id],['id'=>$id,'module'=>$this]);
            }elseif(is_array($this->_modules[$id]) && isset($this->_modules[$id]['class'])) {
              $config = ['id'=>$id,'module'=>$this];
              $module = easy::createObject(array_merge($this->_modules[$id],$config));
            }

            return $this->_modules[$id] = $module;
        }
      }
      // if not set a child module by this $id return null;
      return null;
    }
    /**
     * 通过id创建controller.
     *
     * @param string $id controller id
     *
     * @return null|controller controller or null if controller id is invalid
     */
    public function createControllerByID($id)
    {
        $className = $id;
      // check className
      if (!preg_match('%^[a-z][a-zA-Z0-9\\-_]*$%', $className)) {
          return null;
      }

        $className = str_replace(' ', '', ucwords(str_replace(['-', '_'], ' ', $className))).'Controller';
        $className = ltrim($this->controllerNamespace.'\\'.$className, '\\');
        if (is_subclass_of($className, 'easy\base\Controller')) {
            $controller = easy::createObject($className, [$id, $this]);

            return get_class($controller) === $className ? $controller : null;
        } else {
            return null;
        }
    }
    /**
     * defalut veiw path is under Views directory.
     *
     * @return [type] [description]
     */
    public function getViewPath()
    {
        if ($this->_viewPath !== null) {
            return $this->_viewPath;
        } else {
            // return default
        return $this->_viewPath = $this->getBasePath().DIRECTORY_SEPARATOR.'views';
        }
    }
    /**
     * [setViewPath description].
     *
     * @param [type] $path [description]
     */
    public function setViewPath($path)
    {
      $this->_viewPath = easy::getAlias($path);
    }
    /**
     * get layout path
     * @return string layout path
     */
    public function getLayoutPath()
    {
      if($this->_layoutPath !== null){
        return $this->_layoutPath;
      }else{
        return $this->_layoutPath = $this->getViewPath().DIRECTORY_SEPARATOR.'layouts';
      }
    }
    /**
     * set layout path
     * @param alias $path easy alias.
     */
    public function setLayoutPath($path)
    {
      $this->_layoutPath = easy::getAlias($path);
    }
    // function get
    public function get($id,$throwException=true){
      if(!isset($this->module)){
        return parent::get($id,$throwException);
      }
      $component = parent::get($id, false);
        if ($component === null) {
            $component = $this->module->get($id, $throwException);
        }
        return $component;
    }
}
