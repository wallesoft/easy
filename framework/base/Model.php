<?php
namespace easy\base;

use easy;
use easy\db\ActiveQuery;
use easy\db\QueryBuild;
use easy\helpers\Cache;
class Model extends Component{
  protected $pk;
  protected $fields = [];
  protected $db;
  //获取主键
  public function getPk(){
    return $this->pk;
  }
  public function insert($data = '',$attr='',$replace = false){
    $query = easy::createObject(ActiveQuery::className(),[get_called_class()]);
    $query->setInsertValues($data);
    $this->db = self::getDb();
    $queryBuild = new QueryBuild($this->db);
    $sql = $queryBuild->buildInsert($query,$option,$replace);
    return $this->db->execute($sql);
  }
  public function getLastId(){
    return $this->db->lastInsertId();
  }
  public static function getDb(){
    return easy::$app->getDb();
  }
  /**
 * 直接SQL查询,返回查询结果
 *
 * @param string $sql
 * @return array
 */
public function query($sql){
  return $this->dbConnection->getAll($sql);
}

/**
 * 执行SQL，用于 更新、写入、删除操作
 *
 * @param string $sql
 * @return
 */
  public function execute($sql){
    return $this->dbConnection->execute($sql);
  }

  /**
   * 开始事务
   *
   * @param string $host
   */
  public  function beginTransaction($host = 'master'){
    $this->dbConnection->beginTransaction($host);
  }

  /**
   * 提交事务
   *
   * @param string $host
   */
  public function commit($host = 'master'){
    $this->dbConnection->commit($host);
  }

  /**
   * 回滚事务
   *
   * @param string $host
   */
  public function rollback($host = 'master'){
    $this->dbConnection->rollback($host);
  }
}
