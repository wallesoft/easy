<?php
/**
 * @since 1.0.0
 */
namespace easy\web;

use easy;

class Application extends easy\base\Application
{

    public $layout = 'main';
    //params
    public $params = [];
    /**
     * 初始化
     * @return [type] [description]
     */
    public function init(){
      $request = $this->getRequest();
        easy::setAlias('@webroot', dirname($request->getScriptFile()));
        easy::setAlias('@web', $request->getBaseUrl());
        /**
         * @todo  session 打开方式更改
         */
        $session = $this->getSession();
        if($session->isOpen){
              $session->open();
          }
        //$sessino->setSavePath('@app');
        parent::init();
    }
    /**
     * handle the request
     * @param  object $request Request
     */
    public function handleRequest($request){
      //解析路由 和 params
      $urlManager = new easy\web\urlManager();
      list($this->_router,$this->params) = $urlManager->parsesRequest($request);
      $this->runAction($this->_router,$this->params);
    }
    /**
     * 返回session实例
     *
     * 此方法返回一个session实例，也可以用
     * easy::$app->session 获取
     * @return [type] [description]
     */
    public function getSession(){
      return $this->get('session');
    }
    /**
     * get view object.
     *
     * @return View object
     */
    public function getView()
    {
        return $this->get('view');
    }

    public function getCookie(){
      return $this->get('cookie');
    }
    public function getPage(){
      return $this->get('page');
    }
    public function getResponse(){
      return $this->get('response');
    }
    /**
     * @see easy\helper\Ulr::to()
     */
    public function createUrl($param,$scheme=false,$host=false){
      return easy\helpers\Url::to($param,$scheme,$host);
    }
    public function coreComponents(){
      return array_merge(parent::coreComponents(),[
        'cookie'=>['class'=>'easy\helpers\Cookie'],
        'page'=>['class'=>'easy\web\Page'],
        'security'=>['class'=>'easy\web\Security'],
        'language'=>['class'=>'easy\web\Language'],
        'view'=>['class'=>'easy\web\View'],
        'url'=>['class'=>'easy\web\Url'],
        'response'=>['class'=>'easy\web\Response'],
      ]
    );
    }
}
