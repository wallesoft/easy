<?php

namespace easy\web;

use easy;
use easy\base\Component;
use easy\helpers\Exception;
class Response extends Component{



  const FORMAT_HTML = 'html';
  const FORMAT_JSON = 'json';
  const FORMAT_JSONP = 'jsonp';
  const FORMAT_XML = 'xml';
    /**
      * @var array list of HTTP status codes and the corresponding texts
      */
     public static $httpStatuses = [
         100 => 'Continue',
         101 => 'Switching Protocols',
         102 => 'Processing',
         118 => 'Connection timed out',
         200 => 'OK',
         201 => 'Created',
         202 => 'Accepted',
         203 => 'Non-Authoritative',
         204 => 'No Content',
         205 => 'Reset Content',
         206 => 'Partial Content',
         207 => 'Multi-Status',
         208 => 'Already Reported',
         210 => 'Content Different',
         226 => 'IM Used',
         300 => 'Multiple Choices',
         301 => 'Moved Permanently',
         302 => 'Found',
         303 => 'See Other',
         304 => 'Not Modified',
         305 => 'Use Proxy',
         306 => 'Reserved',
         307 => 'Temporary Redirect',
         308 => 'Permanent Redirect',
         310 => 'Too many Redirect',
         400 => 'Bad Request',
         401 => 'Unauthorized',
         402 => 'Payment Required',
         403 => 'Forbidden',
         404 => 'Not Found',
         405 => 'Method Not Allowed',
         406 => 'Not Acceptable',
         407 => 'Proxy Authentication Required',
         408 => 'Request Time-out',
         409 => 'Conflict',
         410 => 'Gone',
         411 => 'Length Required',
         412 => 'Precondition Failed',
         413 => 'Request Entity Too Large',
         414 => 'Request-URI Too Long',
         415 => 'Unsupported Media Type',
         416 => 'Requested range unsatisfiable',
         417 => 'Expectation failed',
         418 => 'I\'m a teapot',
         421 => 'Misdirected Request',
         422 => 'Unprocessable entity',
         423 => 'Locked',
         424 => 'Method failure',
         425 => 'Unordered Collection',
         426 => 'Upgrade Required',
         428 => 'Precondition Required',
         429 => 'Too Many Requests',
         431 => 'Request Header Fields Too Large',
         449 => 'Retry With',
         450 => 'Blocked by Windows Parental Controls',
         451 => 'Unavailable For Legal Reasons',
         500 => 'Internal Server Error',
         501 => 'Not Implemented',
         502 => 'Bad Gateway or Proxy Error',
         503 => 'Service Unavailable',
         504 => 'Gateway Time-out',
         505 => 'HTTP Version not supported',
         507 => 'Insufficient storage',
         508 => 'Loop Detected',
         509 => 'Bandwidth Limit Exceeded',
         510 => 'Not Extended',
         511 => 'Network Authentication Required',
     ];
     public $statusCode = 200; // status code
     public $statusText = 'OK';
     protected $msg = ''; //  return msg

     protected $headers = []; // response header

     public $version; // version 1.0 or 1.1

     public $format; // format  like json jsonp xml etc.

     public $formatters = []; // set formater class

     public $data; //mixed response data;

     public $content; //string the response content . [[data]] is not null, it will be converted to [[content]]

     public function init(){
       if ($this->version === null) {
           if (isset($_SERVER['SERVER_PROTOCOL']) && $_SERVER['SERVER_PROTOCOL'] === 'HTTP/1.0') {
               $this->version = '1.0';
           } else {
               $this->version = '1.1';
           }
       }
       $this->formatters = array_merge($this->defaultFormatters(),$this->formatters);
     }
     /**
      * set headers
      * @param string $name    name
      * @param string  $content content
      */
     public function setHeaders($name,$content){
       $this->headers[$name] = $content;
     }
     public function setStatusCode($value, $text = null)
     {
       if ($value === null){
         $value  = 200;
       }
       $this->statusCode = $value;
       if ($text === null) {
            $this->statusText = isset(static::$httpStatuses[$this->_statusCode]) ? static::$httpStatuses[$this->_statusCode] : '';
        } else {
            $this->statusText = $text;
        }

       return $this;
     }
     public function getStatusCode(){
       return $this->statusCode;
     }
     public function send()
     {
       $this->prepare();
       $this->sendHeaders();
       $this->sendContent();
     }

     protected function sendContent(){
       //暴力and直接
       echo $this->content;
       return;
     }
     protected function sendHeaders(){
      if(headers_sent()){
        return;
      }
      if($this->headers){
        $replace = true;
        foreach($this->headers as $name=>$value){
            header("$name: $value", $replace);
            $replace = false;
        }
      }
      $statusCode = $this->getStatusCode();
       header("HTTP/{$this->version} {$statusCode} {$this->statusText}");
     }
     /**
     * @return array the formatters that are supported by default
     */
    protected function defaultFormatters()
    {
        return [
            self::FORMAT_HTML => [
                'class' => 'easy\web\HtmlResponseFormatter',
            ],
            self::FORMAT_XML => [
                'class' => 'easy\web\XmlResponseFormatter',
            ],
            self::FORMAT_JSON => [
                'class' => 'easy\web\JsonResponseFormatter',
            ],
            self::FORMAT_JSONP => [
                'class' => 'easy\web\JsonResponseFormatter',
                'useJsonp' => true,
            ],
        ];
    }
   public function prepare(){
     if (isset($this->formatters[$this->format])) {
       $formatter = $this->formatters[$this->format];
       if (!is_object($formatter)) {
         $this->formatters[$this->format] = $formatter = easy::createObject($formatter);
       }
       if ($formatter instanceof ResponseFormatterInterface){
         $formatter->format($this);
       } else {
         Exception::throwException("Response formatter is invalid ,It must implement the ResponseFormatterInterface.");
       }
     } else {
       Exception::throwException("Unsupported response formatter: {$this->format}");
     }
   }
}
