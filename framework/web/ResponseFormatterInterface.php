<?php

namespace easy\web;

/**
 *  Response formatter InterfaceName
 */
interface ResponseFormatterInterface
{
  /**
   * format response
   * @param  Response $response respone to be formatted.
   */
  public function format($response);
}
