<?php
/**
 * class Request.
 *
 * @todo 考虑一些特殊情况的处理
 */
namespace easy\web;

use easy;
use easy\base\Component;
class Request extends Component
{
    //$_GET 的字符串存储
    private $_queryParams;
    private $_scriptFile;
    /**
     * 解析请求的url网址
     *
     * @return string 请求的字符串
     */
    public function resolveRequestUri()
    {
        if (isset($_SERVER['HTTP_X_REWRITE_URL'])) {
            $requestUri = $_SERVER['HTTP_X_REWRITE_URL'];
        } elseif (isset($_SERVER['REQUEST_URI'])) {
            $requestUri = $_SERVER['REQUEST_URI'];
        } elseif (isset($_SERVER['ORIG_PATH_INFO'])) { // IIS 5.0 CGI
            $requestUri = $_SERVER['ORIG_PATH_INFO'];
            if (!empty($_SERVER['QUERY_STRING'])) {
                $requestUri .= '?'.$_SERVER['QUERY_STRING'];
            }
        } else {
            $requestUri = '';
        }

        return $requestUri;
    }
    /**
     * 获取请求的字符串.
     *
     * @return string 请求的字符串
     */
    public function getQueryString()
    {
        return isset($_SERVER['QUERY_STRING']) ? $_SERVER['QUERY_STRING'] : '';
    }
    /**
     * [getQueryParams description]
     * This is a cool functionxw.
     *
     * @author nickf
     *
     * @version [version]
     *
     * @return [type] [description]
     */
    public function getQueryParams()
    {
        if ($this->_queryParams === null) {
            return $_GET;
        }

        return $this->_queryParams;
    }
    public function setQueryParams($values)
    {
        $this->_queryPamras = $value;
    }
    //通过GET请求返回相应的参数
    public function getQueryParam($name, $defaultValue = null)
    {
        $params = $this->getQueryParams();

        return isset($params[$name]) ? $params[$name] : $defaultValue;
    }
    /**
     * Returns the entry script file path.
     * The default implementation will simply return `$_SERVER['SCRIPT_FILENAME']`.
     *
     * @return string the entry script file path
     */
    public function getScriptFile()
    {
        return isset($this->_scriptFile) ? $this->_scriptFile : $_SERVER['SCRIPT_FILENAME'];
    }
    private $_scriptUrl;
    /**
     * Returns the relative URL of the entry script.
     * The implementation of this method referenced Zend_Controller_Request_Http in Zend Framework.
     *
     * @return string the relative URL of the entry script
     *
     * @throws InvalidConfigException if unable to determine the entry script URL
     */
    public function getScriptUrl()
    {
        if ($this->_scriptUrl === null) {
            $scriptFile = $this->getScriptFile();
            $scriptName = basename($scriptFile);
            if (basename($_SERVER['SCRIPT_NAME']) === $scriptName) {
                $this->_scriptUrl = $_SERVER['SCRIPT_NAME'];
            } elseif (basename($_SERVER['PHP_SELF']) === $scriptName) {
                $this->_scriptUrl = $_SERVER['PHP_SELF'];
            } elseif (isset($_SERVER['ORIG_SCRIPT_NAME']) && basename($_SERVER['ORIG_SCRIPT_NAME']) === $scriptName) {
                $this->_scriptUrl = $_SERVER['ORIG_SCRIPT_NAME'];
            } elseif (($pos = strpos($_SERVER['PHP_SELF'], '/'.$scriptName)) !== false) {
                $this->_scriptUrl = substr($_SERVER['SCRIPT_NAME'], 0, $pos).'/'.$scriptName;
            } elseif (!empty($_SERVER['DOCUMENT_ROOT']) && strpos($scriptFile, $_SERVER['DOCUMENT_ROOT']) === 0) {
                $this->_scriptUrl = str_replace('\\', '/', str_replace($_SERVER['DOCUMENT_ROOT'], '', $scriptFile));
            } else {
                throw new InvalidConfigException('Unable to determine the entry script URL.');
            }
        }

        return $this->_scriptUrl;
    }

    private $_hostInfo;

    public function getHostInfo()
    {
        if ($this->_hostInfo === null) {
            $secure = $this->getIsSecureConnection();
            $http = $secure ? 'https' : 'http';
            if (isset($_SERVER['HTTP_HOST'])) {
                $this->_hostInfo = $http.'://'.$_SERVER['HTTP_HOST'];
            } else {
                $this->_hostInfo = $http.'://'.$_SERVER['SERVER_NAME'];
                $port = $secure ? $this->getSecurePort() : $this->getPort();
                if (($port !== 80 && !$secure) || ($port !== 443 && $secure)) {
                    $this->_hostInfo .= ':'.$port;
                }
            }
        }

        return $this->_hostInfo;
    }
    /**
     * Returns the currently requested absolute URL.
     * This is a shortcut to the concatenation of [[hostInfo]] and [[url]].
     *
     * @return string the currently requested absolute URL
     */
    public function getAbsoluteUrl()
    {
        return $this->getHostInfo().$this->getUrl();
    }
    private $_baseUrl;
    /**
     * Returns the relative URL for the application.
     * This is similar to [[scriptUrl]] except that it does not include the script file name,
     * and the ending slashes are removed.
     *
     * @return string the relative URL for the application
     *
     * @see setScriptUrl()
     */
    public function getBaseUrl()
    {
        if ($this->_baseUrl === null) {
            $this->_baseUrl = rtrim(dirname($this->getScriptUrl()), '\\/');
        }

        return $this->_baseUrl;
    }

    private $_url;

    /**
     * Returns the currently requested relative URL.
     * This refers to the portion of the URL that is after the [[hostInfo]] part.
     * It includes the [[queryString]] part if any.
     *
     * @return string the currently requested relative URL. Note that the URI returned is URL-encoded
     *
     * @throws InvalidConfigException if the URL cannot be determined due to unusual server configuration
     */
    public function getUrl()
    {
        if ($this->_url === null) {
            $this->_url = $this->resolveRequestUri();
        }

        return $this->_url;
    }

    private $_port;

    /**
     * Returns the port to use for insecure requests.
     * Defaults to 80, or the port specified by the server if the current
     * request is insecure.
     *
     * @return int port number for insecure requests
     *
     * @see setPort()
     */
    public function getPort()
    {
        if ($this->_port === null) {
            $this->_port = !$this->getIsSecureConnection() && isset($_SERVER['SERVER_PORT']) ? (int) $_SERVER['SERVER_PORT'] : 80;
        }

        return $this->_port;
    }

    /**
     * Return if the request is sent via secure channel (https).
     *
     * @return bool if the request is sent via secure channel (https)
     */
    public function getIsSecureConnection()
    {
        return isset($_SERVER['HTTPS']) && (strcasecmp($_SERVER['HTTPS'], 'on') === 0 || $_SERVER['HTTPS'] == 1)
                    || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strcasecmp($_SERVER['HTTP_X_FORWARDED_PROTO'], 'https') === 0;
    }
    /**
     * Returns the server port number.
     *
     * @return int server port number
     */
    public function getServerPort()
    {
        return (int) $_SERVER['SERVER_PORT'];
    }
  /**
   * check if is form submit.
   *
   * 如果要用其他方法返回想用的数据 请在application中根据接口返回重新添加类并优化
   * 此函数应当保持相应的独立并与app没有粘连
   *
   * @param  bool $token   是否验证token
   * @param  bool $captcha 是否验证验证码
   * @param  string  $return  alert or num
   *
   * @return bool          true or false
   */
  public function isSubmit($token = false, $check_captcha = false, $return = 'num')
  {
      $submit = isset($_POST['form_submit']) ? $_POST['form_submit'] : $_GET['form_submit'];
      if ($submit != 'ok') {
          return false;
      }
      if ($token && !easy::$app->security->checkToken()) {
            // 此处只返回相应表示 -11 表示非法请求 token验证不通过
            return -11;
      }
      //此处检验验证码，如果验证码不对销毁原来验证码并返回 -12
      if ($check_captcha) {
          if (!easy\helpers\Captcha::checkSeccode($_POST['ehash'], $_POST['captcha'])) {
              easy::$app->cookie->setCookie('seccode'.$_POST['ehash'], '', -3600);                  return -12;
          }

          easy::$app->cookie->setCookie('seccode'.$_POST['ehash'], '', -3600);
      }

      return true;
  }

	/**
	 * 取得IP
	 *
	 *
	 * @return string 字符串类型的返回结果
	 */
	public function getIp(){
		if (@$_SERVER['HTTP_CLIENT_IP'] && $_SERVER['HTTP_CLIENT_IP']!='unknown') {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (@$_SERVER['HTTP_X_FORWARDED_FOR'] && $_SERVER['HTTP_X_FORWARDED_FOR']!='unknown') {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
  /**
   * 获取上一步来源地址
   *
   * @return string 返回结果
   */
  public function getReferer()
  {
      return empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
  }
}
