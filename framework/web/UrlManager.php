<?php
/**
 * class UrlManager.
 */
namespace easy\web;

use easy;
use easy\base\Component;

class UrlManager extends Component
{
    /**
     * 是否美化url.
     *
     * @var bool default false
     */
    public $enablePrettyUrl = false;
    /**
     * 是否显示请求文件.
     *
     * @var bool
     */
    public $showScriptName = true;
    /**
     * 当 $enablePrettyUrl = true 时，根据此规则美化url.
     *
     * @todo 写一个配置此文件的示例
     *
     * @var [type]
     */
    public $rules = [];
    //router param
    public $routeParam = 'r';
/**
 * @var array
 *
 * 存储请求的参数例如[param=>'value',...]
 */
    //private $parameters = [];
    public $router;
    public $params;
    private $_baseUrl;
    private $_scriptUrl;
    private $_hostInfo;

    /**
     * @param $request the Request componet
     */
    public function parsesRequest($request)
    {
        $this->router = $request->getQueryParam($this->routeParam,''); // parse router
        //parse params
        $this->params = $this->parsesParams($this->getRequestString($request));
        return [$this->router,$this->params];

    }

    /**
     * @param $request the Request componet
     *
     * @return string
     */
    public function getRequestString($request)
    {
        return $request->getQueryString();
    }

    /**
     * @param null $requestString the string from url where  r='controller/action'
     *
     * 解析路由 通过URL请求$this->routerParam的值解析出Controller和Action
     */
    protected function parsesRouter($requestString = null)
    {
        if ($requestString == null) {
            $this->router = [];
        }
        //判断字符串中有无'/'分隔符
        $pos = strpos($requestString, '/');
        if ($pos === false) {
            //just like... index.php?r=controller..
            $this->router = ['controller' => $requestString];
        } else {
            $this->router['controller'] = substr($requestString, 0, $pos);
            $this->router['action'] = substr($requestString, $pos + 1);
        }
    }

    /**
     * @param null $queryString
     */
    protected function parsesParams($queryString = null)
    {
        if ($queryString == null) {
            return $this->params = null;
        }
        $pos = strpos($queryString, '&');
        if ($pos === false) {
            parse_str($queryString, $params);
            if (isset($params[$this->routeParam])) {
                unset($params[$this->routeParam]);
            }

            return $this->params = $params;
            //isset()
        }
        parse_str($queryString, $params);
        if (isset($params[$this->routeParam])) {
            unset($params[$this->routeParam]);
        }
        $this->params = $params;
    }

    /**
     * @return array like ['router'=>['controller'=>'userController','action'=>'login'],'params'=['..'=>'...']]
     */
    public function getRouter()
    {
        return $this->router;
    }

    public function getParams()
    {
        return $this->params;
    }
    /**
     * 根据给定的路由规则和请求参数
     * 添加对锚位的支持 '#'=>'value'.
     *
     * @param string|array $params (e.g. ['site/index','param'=>'value',..] or 'site/index')
     *
     * @return string 生成的url
     */
    public function createUrl($params)
    {
        $params = (array) $params;
        $anchor = isset($params['#']) ? '#'.$params['#'] : '';
        unset($params['#'], $params[$this->routeParam]);

        $route = trim($params[0], '/'); //过滤掉首位 ‘/’ 字符
      unset($params[0]);
        $baseUrl = $this->showScriptName || !$this->enablePrettyUrl ? $this->getScriptUrl() : $this->getBaseUrl();
       /**
        * @todo  下面处理处理url美化的相关操作
        */
       $url = "$baseUrl?{$this->routeParam}=".urlencode($route);
        if (!empty($params) && ($query = http_build_query($params)) !== '') {
            $url .= '&'.$query;
        }

        return $url.$anchor;
    }

  /**
   * 创建绝对地址
   *
   * @param  array|string $params see $this->createUrl()
   * @param  string $scheme https or http 方式
   *
   * @return string         创建的url
   *
   * @see createUrl()
   */
  public function createAbsoluteUrl($params, $scheme = null)
  {
      $params = (array) $params;
      $url = $this->createUrl($params);
      if (strpos($url, '://') === false) {
          $url = $this->getHostInfo().$url;
      }
      if (is_string($scheme) && ($pos = strpos($url, '://')) !== false) {
          $url = $scheme.substr($url, $pos);
      }

      return $url;
  }
    public function getHostInfo()
    {
        if ($this->_hostInfo === null) {
            $request = easy::$app->getRequest();
            if ($request instanceof Request) {
                $this->_hostInfo = $request->getHostInfo();
            } else {
                easy\helpers\Helper::throwException('Please configure UrlManager::hostInfo correctly as you are running a console application.');
            }
        }

        return $this->_hostInfo;
    }

    public function getBaseUrl()
    {
        if ($this->_baseUrl === null) {
            $request = easy::$app->getRequest();
            if ($request instanceof Request) {
                $this->_baseUrl = $request->getBaseUrl();
            } else {
                easy\helpers\Helper::throwException('Please configure UrlManager::baseUrl correctly as you are running a console application.');
            }
        }

        return $this->_baseUrl;
    }
    public function getScriptUrl()
    {
        if ($this->_scriptUrl === null) {
            $request = easy::$app->getRequest();
            if ($request instanceof Request) {
                $this->_scriptUrl = $request->getScriptUrl();
            } else {
                easy\helpers\Helper::throwException('Please configure UrlManager::scriptUrl correctly as you are running a console application.');
            }
        }

        return $this->_scriptUrl;
    }
}
