<?php
/**
 * 语言条用类
 *
 * 语言调用，根据网站配置跳用语言配置包
 *
 * @author jackhunx
 * @since v1.0
 */
namespace easy\web;

use Easy;
use easy\base\Component;

class Language extends Component
{
    /**
     * 语言类型
     * @var [type]
     */
    public $language = '';
    /**
     * 语言存储路径
     * @var [type]
     */
    public $savePath;
    /**
     * 变量内容
     * @var array
     */
    protected $language_content=[];
    /**
  	 * 通过语言包文件设置语言内容
  	 *
  	 * @param string $file 语言包文件，可以按照逗号(,)分隔
  	 * @return bool 布尔类型的返回结果
  	 */
    public function read($file){
      str_replace('，',',',$file);
      $tmp = explode(',',$file);
      foreach ($tmp as $v){
        $tmp_file = $this->getSavePath().DIRECTORY_SEPARATOR.$this->getLanguage().DIRECTORY_SEPARATOR.$v.'.php';
        if (file_exists($tmp_file)){
          require($tmp_file);
          if (!empty($lang) && is_array($lang)){
            $this->language_content = array_merge($this->language_content,$lang);
          }
          unset($lang);
        }
      }
      return true;
    }
    /**
     * 获取全部
     * @param  string $charset [description]
     * @return [type]          [description]
     */
    public function getLanguageContent($charset=''){
      $result = $this->language_content;
      if (strtoupper(Easy::$app->charset) == 'UTF-8' || strtoupper($charset) == 'UTF-8') return $result;//json格式时不转换
  	   //转码
  		if (strtoupper(Easy::$app->charset) == 'GBK' && !empty($result)){
  			if (is_array($result)){
  				foreach ($result as $k => $v){
  					$result[$k] = iconv('UTF-8','GBK',$v);
  				}
  			}
  		}
  		return $result;
    }

    /**
  	 * 取指定下标的数组内容
  	 * @todo 这个方法重写了component方法 ，待改进
  	 * @param string $key 数组下标
  	 * @return string 字符串形式的返回结果
  	 */
  	public function get($key,$charset = ''){
      if(strpos($key,',') !== false){
        $tmp = explode(',',$key);
        $result = '';
        foreach($tmp as $val){
          $result .= $this->get($val);
        }
      }else{
  		    $result = $this->language_content[$key] ? $this->language_content[$key] : '';
      }
  		if (strtoupper(easy::$app->getConfig('charset')) == 'UTF-8' || strtoupper($charset) == 'UTF-8') return $result;//json格式时不转换
  		/**
  		 * 转码
  		 */
  		if (strtoupper(easy::$app->getConfig('charset')) == 'GBK' && !empty($result)){
  			$result = iconv('UTF-8','GBK',$result);
  		}
  		return $result;
  	}
    /**
     * [funciton description]
     * @var [type]
     */
    /**
     * 设置存储路径
     * default:@app/language;
     * @var [type]
     */
    public function getSavePath(){
      return isset($this->savePath) ? $this->savePath : easy::$app->getBasePath().DIRECTORY_SEPARATOR.'language';
    }
    /**
     * 设置存储路径
     *
     * 支持路径别名 此方法不检查路径是否存在，提前创建好
     * @param string|alias $path 存储路径
     */
    public function setSavePath($path){
      $this->savePath = easy::getAlias($path);
    }
    /**
     * 设置语言类型
     * @param string $language 语言类型
     */
    public function setLanguage($language){
      $this->lanuage = $language;
    }
    /**
     * 获取语言
     * 默认 加载 /to/loanguage/default文件夹下的文件
     * @return string 获取语言
     */
    public function getLanguage(){
      return $this->language != '' ?  $this->language : 'default';
    }
    /**
     * to gbk
     */
    public function getGbk($key){
      if (strtoupper(easy::$app->getConfig('charset')) == 'GBK' && !empty($key)){
        if (is_array($key)){
          $result = var_export($key, true);//变为字符串
          $result = iconv('UTF-8','GBK',$result);
          eval("\$result = $result;");//转换回数组
        }else {
          $result = iconv('UTF-8','GBK',$key);
        }
      }
      return $result;
    }
    /**
     * to utf8
     **/
    public function getUTF8($key){
      if (!empty($key)){
  			if (is_array($key)){
  				$result = var_export($key, true);//变为字符串
  				$result = iconv('GBK','UTF-8',$result);
  				eval("\$result = $result;");//转换回数组
  			}else {
  				$result = iconv('GBK','UTF-8',$key);
  			}
  		}
  		return $result;
    }
}
