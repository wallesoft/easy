<?php
namespace easy\web;

use easy;
use easy\base\Component;

/**
 * formatter
 */
class HtmlResponseFormatter extends Component implements ResponseFormatterInterface
{

    public $contentType = 'text/html';

    public function format($response)
    {
      $response->setHeaders('Content-Type',$contentType);
      if($response->data !== null){
        $response->content = $response->data;
      }
    }
}
