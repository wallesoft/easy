<?php
/**
 * 模板驱动
 *
 * 模板驱动
 *
 * @copyright  Copyright (c) 2007-2015 WeBetter Inc.
 * @license
 * @link
 * @since      File available since Release v1.0
 */

namespace easy\web;

use easy;
use easy\helpers\Helper;
class Tpl{
    /**
     * 单件对象
     */
    private static $instance = null;
    /**
     * 输出模板内容的数组，其他的变量不允许从程序中直接输出到模板
     */
    private static $output_value = array();
    /**
     * 模板路径设置
     */
    private static $tpl_dir = '';
    private static $tpl_dirtpl = '';
    /**
     * 默认layout
     */
    private static $layout_file = 'layout';

    private function __construct(){}

    /**
     * 实例化
     *
     * @return obj
     */
    public static function getInstance(){
        if (self::$instance === null || !(self::$instance instanceof Tpl)) {
            self::$instance = new Tpl();
        }
        return self::$instance;
    }

    /**
     * 设置模板目录
     *
     * @param string $dir
     * @return bool
     */
    public static function setDir($dir){
        self::$tpl_dir = $dir;
        return true;
    }
    public static function setDirAdmin($dirtpl){
        self::$tpl_dirtpl = $dirtpl;
        return true;
    }
    /**
     * 设置布局
     *
     * @param string $layout
     * @return bool
     */
    public static function setLayout($layout){
        self::$layout_file = $layout;
        return true;
    }

    /**
     * 抛出变量
     *
     * @param mixed $output
     * @param  void
     */
    public static function output($output, $input = ''){
        self::getInstance();

        self::$output_value[$output] = $input;
    }

    /**
     * 调用显示模板
     *
     * @param string $page_name
     * @param string $layout
     * @param int $time
     */
    public static function showpage($page_name = '', $layout = '', $time = 2000){
        if (!defined('TPL_NAME'))
            define('TPL_NAME', 'default');
        self::getInstance();
        if (!empty(self::$tpl_dir)) {
            $tpl_dir = self::$tpl_dir . '/';
        }
        //默认是带有布局文件
        if (empty($layout)) {
            $layout = 'layout' . '/' . self::$layout_file . '.php';
        } else {
            $layout = 'layout' . '/' . $layout . '.php';
        }

        $layout_file = BASE_PATH . '/views/' . TPL_NAME . '/' . $layout;
        if (empty(self::$tpl_dirtpl)) {
            $tpl_file = BASE_PATH . '/views/' . TPL_NAME . '/' . $tpl_dir . $page_name . '.php';
        } else {
          // TODO: 此地方处理不准确
            $tpl_file = BASE_PATH . '/modules/' . self::$tpl_dirtpl . '/views/' . TPL_NAME . '/' . $tpl_dir . $page_name . '.php';
        }

        if (file_exists($tpl_file)) {
            //对模板变量进行赋值
            $output                    = self::$output_value;
            //页头
            $output['html_title']      = $output['html_title'] != '' ? $output['html_title'] : $GLOBALS['setting_config']['site_name'];
            $output['seo_keywords']    = $output['seo_keywords'] != '' ? $output['seo_keywords'] : $GLOBALS['setting_config']['site_name'];
            $output['seo_description'] = $output['seo_description'] != '' ? $output['seo_description'] : $GLOBALS['setting_config']['site_name'];
            $output['ref_url']         =Easy::getReferer();

            easy\helpers\Language::read('common');
            $lang = easy\helpers\Language::getLangContent();

            @header("Content-type: text/html; charset=" . CHARSET);
            //判断是否使用布局方式输出模板，如果是，那么包含布局文件，并且在布局文件中包含模板文件
            if ($layout != '') {
                if (file_exists($layout_file)) {
                    include_once($layout_file);
                } else {
                    $error = 'View ERROR:' . 'Layout:'. $layout . ' is not exists';
                    throw_exception($error);
                }
            } else {
                include_once($tpl_file);
            }
        } else {
            $error = 'View ERROR:' . 'Template:' . $page_name . ' is not exists';
            easy\helpers\Helper::throwException($error);
        }
    }

    /**
     * 显示页面Trace信息
     *
     * @return array
     */
    public static function showTrace(){
        $trace                                                   = array();
        //当前页面
        $trace[easy\helpers\Language::get('nc_debug_current_page')]           = $_SERVER['REQUEST_URI'] . '<br>';
        //请求时间
        $trace[easy\helpers\Language::get('nc_debug_request_time')]           = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']) . '<br>';
        //系统运行时间
        $query_time                                              = number_format((microtime(true) - StartTime), 3) . 's';
        $trace[easy\helpers\Language::get('nc_debug_execution_time')]         = $query_time . '<br>';
        //内存
        $trace[easy\helpers\Language::get('nc_debug_memory_consumption')]     = number_format(memory_get_usage() / 1024 / 1024, 2) . 'MB' . '<br>';
        //请求方法
        $trace[easy\helpers\Language::get('nc_debug_request_method')]         = $_SERVER['REQUEST_METHOD'] . '<br>';
        //通信协议
        $trace[easy\helpers\Language::get('nc_debug_communication_protocol')] = $_SERVER['SERVER_PROTOCOL'] . '<br>';
        //用户代理
        $trace[easy\helpers\Language::get('nc_debug_user_agent')]             = $_SERVER['HTTP_USER_AGENT'] . '<br>';
        //会话ID
        $trace[easy\helpers\Language::get('nc_debug_session_id')]             = session_id() . '<br>';
        //执行日志
        $log                                                     = Log::read();
        $trace[easy\helpers\Language::get('nc_debug_logging')]                = count($log) ? count($log) . easy\helpers\Language::get('nc_debug_logging_1') . '<br/>' . implode('<br/>', $log) : easy\helpers\Language::get('nc_debug_logging_2');
        $trace[easy\helpers\Language::get('nc_debug_logging')]                = $trace[easy\helpers\Language::get('nc_debug_logging')] . '<br>';
        //文件加载
        $files                                                   = get_included_files();
        $trace[easy\helpers\Language::get('nc_debug_load_files')]             = count($files) . str_replace("\n", '<br/>', substr(substr(print_r($files, true), 7), 0, -2)) . '<br>';
        return $trace;
    }
    public static function flexigridXML($flexigridXML){

        $page  = $flexigridXML['now_page'];
        $total = $flexigridXML['total_num'];
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: text/xml");
        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        $xml .= "<rows>";
        $xml .= "<page>$page</page>";
        $xml .= "<total>$total</total>";
        if (empty($flexigridXML['list'])) {
            $xml .= "<row id=''>";
            $xml .= "<cell></cell>";
            $xml .= "</row>";
        } else {
            foreach ($flexigridXML['list'] as $k => $v) {
                $xml .= "<row id='" . $k . "'>";
                foreach ($v as $kk => $vv) {
                    $xml .= "<cell><![CDATA[" . $v[$kk] . "]]></cell>";
                }
                $xml .= "</row>";
            }
        }
        $xml .= "</rows>";
        echo $xml;
    }
    public static function flexigridfXML($flexigridXML){
        $page  = $flexigridXML['now_page'];
        $total = $flexigridXML['total_num'];

        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: text/xml");
        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        $xml .= "<rows>";
        $xml .= "<page>$page</page>";
        $xml .= "<total>$total</total>";
        foreach ($flexigridXML['list'] as $k => $v) {
            $xml .= "<row id='" . $k . "'>";
            $xml .= "<cell><![CDATA[" . $v['operation'] . "]]></cell>";
            $xml .= "<cell><![CDATA[" . $v['channel_name'] . "]]></cell>";
            $xml .= "<cell><![CDATA[" . $v['channel_style'] . "]]></cell>";
            $xml .= "<cell><![CDATA[" . $v['gc_name'] . "]]></cell>";
            $xml .= "<cell><![CDATA[" . $v['channel_show'] . "]]></cell>";
            $xml .= "</row>";
        }

        $xml .= "</rows>";
        echo $xml;
    }

    public static function flexigridXMLfloor($flexigridXML){
        $page  = $flexigridXML['now_page'];
        $total = $flexigridXML['total_num'];

        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: text/xml");
        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        $xml .= "<rows>";
        $xml .= "<page>$page</page>";
        $xml .= "<total>$total</total>";
        foreach ($flexigridXML['list'] as $k => $v) {
            $xml .= "<row id='" . $k . "'>";
            $xml .= "<cell><![CDATA[" . $v['operation'] . "]]></cell>";
            $xml .= "<cell><![CDATA[" . $v['web_name'] . "]]></cell>";
            $xml .= "<cell><![CDATA[" . $v['web_page'] . "]]></cell>";
            $xml .= "<cell><![CDATA[" . $v['update_time'] . "]]></cell>";
            $xml .= "<cell><![CDATA[" . $v['web_show'] . "]]></cell>";
            $xml .= "</row>";
        }

        $xml .= "</rows>";
        echo $xml;
    }

    public static function flexigroupbuyXML($flexigridXML){
        $page  = $flexigridXML['now_page'];
        $total = $flexigridXML['total_num'];
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
        header("Cache-Control: no-cache, must-revalidate");
        header("Pragma: no-cache");
        header("Content-type: text/xml");
        $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        $xml .= "<rows>";
        $xml .= "<page>$page</page>";
        $xml .= "<total>$total</total>";
        if (empty($flexigridXML['list'])) {
            $xml .= "<row id=''>";
            $xml .= "<cell></cell>";
            $xml .= "</row>";
        } else {
            foreach ($flexigridXML['list'] as $k => $v) {
                $xml .= "<row id='" . $k . "'>";
                $xml .= "<cell><![CDATA[" . '--' . "]]></cell>";
                foreach ($v as $kk => $vv) {
                    $xml .= "<cell><![CDATA[" . $v[$kk] . "]]></cell>";
                }
                $xml .= "</row>";
            }
        }
        $xml .= "</rows>";
        echo $xml;
    }

    public static function groupedValues($data, $cou_id, $sk_id){

        foreach ($data as $k => $v) {
            $groupvalue[$v[$cou_id]][$v[$sk_id]] = $v[$sk_id];
        }
        return $groupvalue;

    }
    public static function indexedValues($data, $sku_id, $cou_id){
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $groupvalue[$v[$sku_id]] = $v[$cou_id];
            }
        } else {
            $groupvalue = $data;

        }
        return $groupvalue;
    }
    public static function groupIndexed($data, $cou_id, $sk_id){

        foreach ($data as $k => $v) {
            if ($sk_id == 'sku_id') {
                $groupvalue[$v[$cou_id]][$v[$sk_id]] = array(
                    'price' => $v['price']
                );
            } else if ($sk_id == 'xlevel') {
                $groupvalue[$v[$cou_id]][$v[$sk_id]] = $v;
            }
        }
        return $groupvalue;

    }
    public static function indexed($indexed_data, $indexed_xlevel){
        if ($indexed_xlevel == 'xlevel') {
            foreach ($indexed_data as $indexed_k => $indexed_v) {
                $data[$indexed_v[$indexed_xlevel]] = $indexed_v;
            }

        } elseif ($indexed_xlevel == 'goods_id') {
            foreach ($indexed_data as $index_k => $index_v) {
                $data[$index_v[$indexed_xlevel]] = $index_v;
            }

        }
        return $data;
    }
    public static function uniqueValues($data, $sku_id){
        print_r($data);
        return $data;

    }
    public static function showDialog($message='',$url='',$alert_type='error',$extrajx='',$time=2)
    {
      if (empty($_GET['inajax'])){
    		if ($url == 'reload') $url = '';
    		Helper::showMessage($message.$extrajs,$url,'html',$alert_type,1,$time*1000);
    	}
    	$message = str_replace("'", "\\'", strip_tags($message));

    	$paramjs = null;
    	if ($url == 'reload'){
    		$paramjs = 'window.location.reload()';
    	}elseif ($url != ''){
    		$paramjs = 'window.location.href =\''.$url.'\'';
    	}
    	if ($paramjs){
    		$paramjs = 'function (){'.$paramjs.'}';
    	}else{
    		$paramjs = 'null';
    	}
    	$modes = array('error' => 'alert', 'succ' => 'succ', 'notice' => 'notice','js'=>'js');
    	$cover = $alert_type == 'error' ? 1 : 0;
    	$extra .= 'showDialog(\''.$message.'\', \''.$modes[$alert_type].'\', null, '.($paramjs ? $paramjs : 'null').', '.$cover.', null, null, null, null, '.(is_numeric($time) ? $time : 'null').', null);';
    	$extra = $extra ? '<script type="text/javascript" reload="1">'.$extra.'</script>' : '';
    	if ($extrajs != '' && substr(trim($extrajs),0,7) != '<script'){
    		$extrajs = '<script type="text/javascript" reload="1">'.$extrajs.'</script>';
    	}
    	$extra .= $extrajs;
    	ob_end_clean();
    	@header("Expires: -1");
    	@header("Cache-Control: no-store, private, post-check=0, pre-check=0, max-age=0", FALSE);
    	@header("Pragma: no-cache");
    	@header("Content-type: text/xml; charset=".CHARSET);

    	$string =  '<?xml version="1.0" encoding="'.CHARSET.'"?>'."\r\n";
    	$string .= '<root><![CDATA['.$message.$extra.']]></root>';
    	echo $string;
    	exit();
    }
      public static function showMessage($msg,$url='',$show_type='html',$msg_type='succ',$is_show=1,$time=2000){
    	// if (!class_exists('Language')) easy::$app->get('core')->import('libraries.language');
    	easy\helpers\Language::read('core_lang_index');
    	$lang	= easy\helpers\Language::getLangContent();
    	/**
    	 * 如果默认为空，则跳转至上一步链接
    	 */
    	$url = ($url!='' ? $url : easy::getReferer());

    	$msg_type = in_array($msg_type,array('succ','error')) ? $msg_type : 'error';

    	/**
    	 * 输出类型
    	 */
    	switch ($show_type){
    		case 'json':
    			$return = '{';
    			$return .= '"msg":"'.$msg.'",';
    			$return .= '"url":"'.$url.'"';
    			$return .= '}';
    			echo $return;
    			break;
    		case 'exception':
    			echo '<!DOCTYPE html>';
    			echo '<html>';
    			echo '<head>';
    			echo '<meta http-equiv="Content-Type" content="text/html; charset='.CHARSET.'" />';
    			echo '<title></title>';
    			echo '<style type="text/css">';
    			echo 'body { font-family: "Verdana";padding: 0; margin: 0;}';
    			echo 'h2 { font-size: 12px; line-height: 30px; border-bottom: 1px dashed #CCC; padding-bottom: 8px;width:800px; margin: 20px 0 0 150px;}';
    			echo 'dl { float: left; display: inline; clear: both; padding: 0; margin: 10px 20px 20px 150px;}';
    			echo 'dt { font-size: 14px; font-weight: bold; line-height: 40px; color: #333; padding: 0; margin: 0; border-width: 0px;}';
    			echo 'dd { font-size: 12px; line-height: 40px; color: #333; padding: 0px; margin:0;}';
    			echo '</style>';
    			echo '</head>';
    			echo '<body>';
    			echo '<h2>'.$lang['error_info'].'</h2>';
    			echo '<dl>';
    			echo '<dd>'.$msg.'</dd>';
    			echo '<dt><p /></dt>';
    			echo '<dd>'.$lang['error_notice_operate'].'</dd>';
    			echo '<dd><p /><p /><p /><p /></dd>';
    			echo '<dd><p /><p /><p /><p />Copyright 2007-2015 NShop, All Rights Reserved '.$lang['company_name'].'</dd>';
    			echo '</dl>';
    			echo '</body>';
    			echo '</html>';
    			exit;
    			break;
    		case 'javascript':
    			echo "<script>";
    			echo "alert('". $msg ."');";
    			echo "location.href='". $url ."'";
    			echo "</script>";
    			exit;
    			break;
    		case 'tenpay':
    			echo "<html><head>";
    			echo "<meta name=\"TENCENT_ONLINE_PAYMENT\" content=\"China TENCENT\">";
    			echo "<script language=\"javascript\">";
    			echo "window.location.href='" . $url . "';";
    			echo "</script>";
    			echo "</head><body></body></html>";
    			exit;
    			break;
    		default:
    		    /**
    		     * 不显示右侧工具条
    		     */
    		    self::output('hidden_nctoolbar', 1);
    			if (is_array($url)){
    				foreach ($url as $k => $v){
    					$url[$k]['url'] = $v['url']?$v['url']:getReferer();
    				}
    			}
    			/**
    			 * 读取信息布局的语言包
    			 */
    			easy\helpers\Language::read("msg");
    			/**
    			 * html输出形式
    			 * 指定为指定项目目录下的error模板文件
    			 */
                self::setDir('');
    			self::output('html_title',easy\helpers\Language::get('nc_html_title'));
    			self::output('msg',$msg);
    			self::output('url',$url);
    			self::output('msg_type',$msg_type);
    			self::output('is_show',$is_show);
    			self::showpage('msg','msg_layout',$time);
    	}
    	exit;
    }
}
