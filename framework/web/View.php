<?php
/**
 * 模板驱动引擎
 *
 * 只是简单的实现参数传递，并没有大多数模板引擎的再编译以及复杂的东西
 * 具体的流程也很简单，说明如下：
 * 1、控制器中将参数传递到此类的$outputValue中，以数组的方式调用
 * 2、当控制器要渲染模板是，更具渲染模板的规则查找文件位置，并将文件路径赋值给$viewFile;。
 * 3、如果当前存在layout的话 ，加载layout并在layout文件中 require_once $viewFile;
 * 过程就是如此简单，相应的开发中使用的也是php语法，不存在重新学习新的模板引擎的规则方法等等
 * @copyright
 * @license Gpl3
 * @author jackhunx
 * @since since v1.0
 */
namespace easy\web;

use easy;
use easy\web\Language;
use easy\base\Exception;
use easy\base\Component;
class View extends Component
{
    private  $outputValue = [];
    /**
     * @var string default extension
     */
    public $defaultExtension = 'php';

    /**
     * 渲染模板文件
     *
     * 简单粗暴 直接通过include 方法加载页面.
     * $view 4种方式
     * e.g.  “@site/inde” 直接指定模板文件位置
     * or "//site/index" application模板文件
     * or "/site/index"  相应的module目录下模板文件
     * or 'index'  对应app下controller id 下的模板文件
     * @param $view
     * @param $params
     * @return string
     */
    public function render($view='',$layoutFile='',$time = 2000)
    {
         @header("Content-type: text/html; charset=" . easy::$app->getConfig('charset'));
         $contentFile = $this->findViewFile($view);
         $output = $this->outputValue;
         // language
         $lang = easy::$app->language->getLanguageContent();
         if(file_exists($contentFile)){
           if($layoutFile !== ''){
              if(file_exists($layoutFile)){
                include_once($layoutFile);
              }else{
                throw new Exception('布局文件：'.$layoutFile.'不存在');
              }
           }else{
             include_once($contentFile);
           }
       }else{
         // TODO: 抛出错误重新处理
         throw new Exception('模板:'.$contentFile.' 文件不存在');
       }
    }

    /**
     * 输出
     * @param  mixed $output 输出变量名
     * @param  void $input  输出变量值
     */
    public function output($output,$input='')
    {
      $this->outputValue[$output] = $input;
    }

    /**
     * 查看$this->render($view,$params) 的说明
     *
     * 此函数对本函数及子函数访问有效
     * 对外访问 请使用 findFile()
     * @param $view 视图文件
     * @return string
     */
    protected function findViewFile($view)
    {
        if(strncmp($view,'@',1) === 0){
          // e.g. "@app/views"
          $file = easy::getAlias($view);
        }elseif (strncmp($view, '//', 2) === 0) {
          // e.g. "//site/main"
            $file = easy::$app->getViewPath() . DIRECTORY_SEPARATOR . ltrim($view, '/');
        } elseif(strncmp($view,'/',1)===0) {
          // e.g. "/site/main" or 'index'
          $controller = easy::$app->controller;
          if ($controller !== null) {
            $file = $controller->module->getViewPath().DIRECTORY_SEPARATOR.ltrim($view,'/');
          }else{
            // TODO: 修改出错方式
            throw new Exception("Unable to locate view file for view '$view': no active controller.");
          }
        }elseif (is_string($view)) {
          $controller = easy::$app->controller;
          $file = $controller->getViewPath().$controller->id.DIRECTORY_SEPARATOR.trim($view);
        }else{
          throw new Exception("Unable to resolve view file for view '$view': no active view context.");
        }
        if (pathinfo($file, PATHINFO_EXTENSION) !== '') {
            return $file;
        }
        $path = $file . '.' . $this->defaultExtension;
       //如果默认格式不是php而且不是一个文件
        if($this->defaultExtension!=='php'&&!is_file($path)){
            $path=$file.'.php';
        }
        return $path;
    }
    /**
     * 此函数提供对外查找view及相关文件路径
     * 在application 以及 module 设置的相关目录下查找 方法同findViewFile
     * 只不过这个方法 提供对外访问
     */
    public function findFile($view){
      return $this->findViewFile($view);
    }
    /**
     * 显示trace信息
     */
    public function showTrace(){
      $trace = [];
      $language = easy::$app->language;
      $language->setSavePath(E_PATH.'/message');
      $language->read('debug');
      //当前页面
      $trace[$language->get('debug_current_page')] = $_SERVER['REQUEST_URI'] . '<br>';
      //请求时间
      $trace[$language->get('debug_request_time')] = date('Y-m-d H:i:s',$_SERVER['REQUEST_TIME']).'<br>';
      //系统运行时间
      $trace[$language->get('debug_execution_time')] = number_format((microtime(true)-E_BEGIN_TIME),3).'s<br>';
      //内存
      $trace[$language->get('debug_memory_consumption')] = number_format(memory_get_usage()/1024/1024,2).'MB'.'<br>';
      //请求方法
      $trace[$language->get('debug_request_method')] = $_SERVER['REQUEST_METHOD'].'<br>';
      //通信协议
      $trace[$language->get('debug_communication_protocol')] = $_SERVER['SERVER_PROTOCOL'].'<br>';
      //用户代理
      $trace[$language->get('debug_user_agent')] = $_SERVER['HTTP_USER_AGENT'].'<br>';
      //回话ID
      $trace[$language->get('debug_session_id')] = session_id().'<br>';
      //执行日志
      $log = easy::$app->log->getRecord();
      $trace[$language->get('debug_logging')] = count($log) ? count($log).$language->get('debug_logging_count').'<br>'.implode('<br>',$log).'<br>' : $language->get('debug_logging_countnull').'<br>';
      //文件加载
      $files = get_included_files();
      $trace[$language->get('debug_load_files')] = count($files).str_replace("\n", '<br/>', substr(substr(print_r($files, true), 7), 0, -2)) . '<br>';
      return $trace;
    }
    public function showException($msg){
      $language = easy::$app->language;
      $language->setSavePath(E_PATH.'/message');
      $language->read('core,msg');
      $lang = $language->getLanguageContent();
      echo '<!DOCTYPE html>';
         echo '<html>';
         echo '<head>';
         echo '<meta http-equiv="Content-Type" content="text/html; charset='.easy::$app->getConfig('charset').'" />';
         echo '<title></title>';
         echo '<style type="text/css">';
         echo 'body { font-family: "Verdana";padding: 0; margin: 0;}';
         echo 'h2 { font-size: 12px; line-height: 30px; border-bottom: 1px dashed #CCC; padding-bottom: 8px;width:800px; margin: 20px 0 0 150px;color:red;}';
         echo 'dl { float: left; display: inline; clear: both; padding: 0; margin: 10px 20px 20px 150px;}';
         echo 'dt { font-size: 14px; font-weight: bold; line-height: 40px; color: #333; padding: 0; margin: 0; border-width: 0px;}';
         echo 'dd { font-size: 12px; line-height: 40px; color: #333; padding: 0px; margin:0;}';
         echo '</style>';
         echo '</head>';
         echo '<body>';
         echo '<h2>'.$lang['error_info'].'</h2>';
         echo '<dl>';
         echo '<dd>'.$msg.'</dd>';
         echo '<dt><p /></dt>';
         echo '<dd>'.$lang['error_notice_operate'].'</dd>';
         echo '<dd><p /><p /><p /><p /></dd>';
         echo '<dd><p /><p /><p /><p />Copyright 2016-2017 Easy FrameWork, All Rights Reserved '.$lang['company_name'].'</dd>';
         echo '</dl>';
         echo '</body>';
         echo '</html>';
    }
}
