<?php
namespace easy\web;

use easy;
use easy\base\Component;
use easy\helpers\Data;
class XmlResponseFormatter extends Component implements ResponseFormatterInterface{

    /**
     * @var string the Content-Type header for the response
     */
    public $contentType = 'application/xml';

    public function format($response)
    {
      $response->setHeaders('Content-Type',$this->contentType);
      if($response->data !== null) {
        $response->content = Data::arrayToXml($response->data);
      }
    }
}
