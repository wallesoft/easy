<?php

namespace easy\web;

use easy;
use easy\base\Component;
use easy\helpers\Json;
use easy\helpers\Exception;
/**
 * json response formatter
 */
class JsonResponseFormatter extends Component implements ResponseFormatterInterface
{

  /**
     * JSON Content Type
     */
    const CONTENT_TYPE_JSONP = 'application/javascript; charset=UTF-8';

    /**
     * JSONP Content Type
     */
    const CONTENT_TYPE_JSON = 'application/json; charset=UTF-8';
    /**
     * when this is true format return jsonp string.
     * @var boolean
     */
    public $useJsonp = false;
    /**
     * content type json or jsonp
     * @var string|null
     */
    public $contentType;
    /**
     * @var int the encoding options passed to [[Json::encode()]]. For more details please refer to
     * <http://www.php.net/manual/en/function.json-encode.php>.
     * Default is `JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE`.
     * This property has no effect, when [[useJsonp]] is `true`.
     */
    public $encodeOptions = 320;
    /**
     * @var bool whether to format the output in a readable "pretty" format. This can be useful for debugging purpose.
     * If this is true, `JSON_PRETTY_PRINT` will be added to [[encodeOptions]].
     * Defaults to `false`.
     * This property has no effect, when [[useJsonp]] is `true`.
     */
    public $prettyPrint = false;

    /**
     * format
     * @param  Response $response response oci_fetch_object
     */
    public function format($response)
    {
      if ($this->contentType === null) {
        $this->contentType = $this->useJsonp ? self::CONTENT_TYPE_JSONP : self::CONTENT_TYPE_JSON;
      }
      $response->setHeaders('Content-Type',$this->contentType);
      //use jsonp
      if ($this->useJsonp) {
        $this->formatJsonp($response);
      } else {
        $this->formatJson($response);
      }
    }
    // format json
    protected function formatJson($response)
    {
      if($response->data != null) {
            $options = $this->encodeOptions;
            if ($this->prettyPrint) {
                $options |= JSON_PRETTY_PRINT;
            }
            $response->content = Json::encode($response->data, $options);
      }
    }
    //format jsonp
    protected function formatJsonp($response)
    {
      if (is_array($response->data)
            && isset($response->data['data'], $response->data['callback'])
        ) {
            $response->content = sprintf(
                '%s(%s);',
                $response->data['callback'],
                Json::htmlEncode($response->data['data'])
            );
          }elseif($reponse->data != null){
            Exception::throwException('Jsonp respone requires the data is array and [callback] elements ');
          }
    }
}
