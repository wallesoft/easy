<?php
/**
 * Session 类
 *
 *
 */

namespace easy\web;

use easy;
use easy\base\Component;
use easy\helpers\Helper;
class Session extends Component implements \ArrayAccess
{
  /**
   * 此参数为系统在初始化时是否打开session
   * default : false, not open session
   * '''
   * set config file if open when application init
   * components =[
   *   .....
   *   'session'=>[
   *        'isOpen'=>true,
   *        ...
   *   ]
   * ]
   *
   * '''
   * @var boolean
   */
  public $isOpen = false;
  /**
   * 默认情况下的设置的存储路径
   * @var [type]
   */
  private $_savePath;
  /**
   * Initializes the application component.
   * This method is required by IApplicationComponent and is invoked by application.
   */
  public function init(){
    parent::init();
    register_shutdown_function([$this,'close']);
    //设置储存地址
    if($this->_savePath){
      $this->setSavePath($this->_savePath);
    }
  }
  /**
   * open session
   * @todo 更改此方法
   * @return [type] [description]
   */
  public function open(){
    if ($this->getIsActive()) {
        return;
    }
    $this->registerSessionHandler();
    //@ini_set('session.name','PHPSESSID');
    @session_start();
  }

  /**
   * Registers session handler.
   * @todo 这里作为扩展其他session存储的
   */
  protected function registerSessionHandler(){
      //do sth here

  }
  /**
   * Sets the current session save path.
   * This is a wrapper for [PHP session_save_path()](http://php.net/manual/en/function.session-save-path.php).
   * @param string $value the current session save path. This can be either a directory name or a path alias.
   * @throws InvalidParamException if the path is not a valid directory
   */
  public function setSavePath($value)
  {
      $this->_savePath = Easy::getAlias($value);
      if (is_dir($this->_savePath)) {
          session_save_path($this->_savePath);
      } else {
          throw new easy\base\Exception("Session save path is not a valid directory: $value");
      }
  }

  /**
   * Frees all session variables and destroys all data registered to a session.
   */
  public function destroy()
  {
      if ($this->getIsActive()) {
          @session_unset();
          @session_destroy();
      }
  }
  /**
   * Gets the current session save path.
   * This is a wrapper for [PHP session_save_path()](http://php.net/manual/en/function.session-save-path.php).
   * @return string the current session save path, defaults to '/tmp'.
   */
  public function getSavePath()
  {
      return session_save_path();
  }
  /**
   * Ends the current session and store session data.
   */
  public function close()
  {
      if ($this->getIsActive()) {
          @session_write_close();
      }
  }
  /**
   * @return boolean whether the session has started
   */
  public function getIsActive()
  {
      return session_status() == PHP_SESSION_ACTIVE;
  }

  /**
   * This method is required by the interface ArrayAccess.
   * @param mixed $offset the offset to check on
   * @return boolean
   */
  public function offsetExists($offset)
  {
      $this->open();

      return isset($_SESSION[$offset]);
  }

  /**
   * This method is required by the interface ArrayAccess.
   * @param integer $offset the offset to retrieve element.
   * @return mixed the element at the offset, null if no element is found at the offset
   */
  public function offsetGet($offset)
  {
      $this->open();

      return isset($_SESSION[$offset]) ? $_SESSION[$offset] : null;
  }

  /**
   * This method is required by the interface ArrayAccess.
   * @param integer $offset the offset to set element
   * @param mixed $item the element value
   */
  public function offsetSet($offset, $item)
  {
      $this->open();
      $_SESSION[$offset] = $item;
  }

  /**
   * This method is required by the interface ArrayAccess.
   * @param mixed $offset the offset to unset element
   */
  public function offsetUnset($offset)
  {
      $this->open();
      unset($_SESSION[$offset]);
  }
}
