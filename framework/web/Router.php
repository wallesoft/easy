<?php
/**
 * class Router
 * @since 1.0
 * @todo 继承component
 */
namespace easy\web;

use Easy;
//use easy\base\Component;
use easy\helpers\Helper;
class Router
{
    /**
     * @var array router;
     */
    public $router;
    /**
     * @var $controller 控制器
     */
    public $controller;
    /**
     * @var $action 方法
     */
    public $action;
    /**
     * default controller
     * @var string
     */
    public $defaultController = 'default';
    /**
     * default action
     */
     public $defaultAction = 'index';
    /**
     * @param array $router
     */
    public function __construct(array $router)
    {
        $this->router = $router;
        $router['controller'] == null ? $this->setController($this->defaultController) : $this->setController($router['controller']);

    }

    /**
     * @return instance the controller instance
     */
    public function getControllerInstance()
    {
        //判断一下是否存在此文件
        $classFile = Easy::getAlias('@' . str_replace('\\', '/', $this->controller) . '.php');
        if($classFile === false || !is_file($classFile)){
          //$this->setController($this->defaultController);
          //访问的controller不存在时 提示并跳转到首页
          Helper::requestNotFound();
        }
        return new $this->controller;
        //return $controllerInstance;
    }

    /**
     * @param $controller controller
     */
    public function setController($controller)
    {
        $this->controller = 'app\\controllers\\' . $controller . 'Controller';
    }

    /**
     * @param $controller
     * @return string
     */
    public function setAction($controller)
    {
        $this->action = isset($this->router['action']) ? 'action' . ucfirst($this->router['action']) : (isset($controller->defaultAction) ? 'action' . $controller->defaultAction : 'action' . $this->defaultAction);
        if(!method_exists($controller,$this->action)){
          //$this->action = isset($controller->defaultAction) ? 'action' . $controller->defaultAction : 'action' . $this->defaultAction;
          //action 不存在时跳转到主页
          Helper::requestNotFound();
        }
    }

    /**
     * @return 方法
     */

    public function getAction()
    {
        return $this->action;
    }

    /**
     * @todo 此地方逻辑需要重新修改
     * @return array
     */
    public function run()
    {
        $controller = $this->getControllerInstance();
        $this->setAction($controller);
        //判断是否存在controller
        if(!is_object($controller)){
          echo "控制器不存在";
          exit;
        }
        if(method_exists($controller,$this->getAction())){
          return array($controller, $this->getAction());
        }

    }

}
