<?php

namespace easy\web;

use easy;
use easy\base\EObject;


/**
 * JsExpression
 *
 * use easy\helpers\Json::encode ...
 */
class JsExpression extends EObject
{
  public $expression;

  public function __construct($expression,$config=[])
  {
    $this->expression = $expression;
    parent::__construct($config);
  }
  /**
   * magic mathod
   * @return string [description]
   */
  public function __toString()
  {
    return $this->expression;
  }
}
