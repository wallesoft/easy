<?php
/**
 * 语言辅助类
 */

namespace easy\helpers;

use easy;
//use easy\web\Language;

class Language
{
    /**
     * easy\web\Language 对象
     * @var object
     */
    private static $instance;
    /**
     * 读取语言包内容
     * @param  string $file 语言包文件
     * @return bool
     */
    public static function read($file)
    {
      self::getInstance();
      self::$instance->read($file);
    }

    public static function getLangContent($charset='')
    {
      self::getInstance();
      return self::$instance->getLanguageContent($charset);
    }
    public static function get($key,$charset='')
    {
        self::getInstance();
        return self::$instance->get($key,$charset);
    }
    /**
     * 实例化
     *
     * @return obj
     */
    public static function getInstance(){
      if(self::$instance === null || !(self::$instance instanceof easy\web\Language)){
        self::$instance = Easy::$app->getLanguage();
       }
       return self::$instance;
    }
    /**
     * 快速获取language
     * @param  string $key p.s
     * @return string      language
     */
    public static function getL($key=''){
      if (strpos($key, ',') !== false) {
  			$tmp = explode(',', $key);
  			$str = self::get($tmp[0]) . self::get($tmp[1]);
  			return isset($tmp[2]) ? $str . self::get($tmp[2]) : $str;
  		}
  		else {
  			return self::get($key);
  		}
  	}
    /**
	 * 得到数组变量的UTF-8编码
	 *
	 * @param array $key GBK编码数组
	 * @return array 数组类型的返回结果
	 */
	public static function getUTF8($key){
		/**
		 * 转码
		 */
		if (!empty($key)){
			if (is_array($key)){
				$result = var_export($key, true);//变为字符串
				$result = iconv('GBK','UTF-8',$result);
				eval("\$result = $result;");//转换回数组
			}else {
				$result = iconv('GBK','UTF-8',$key);
			}
		}
		return $result;
	}

}
