<?php

/**
 * Helper类 一些辅助方法
 *
 */

namespace easy\helpers;

use easy;
//use easy\base\exception;

class Helper
{
  /**
   * 取得随机数
   * @param  int  $length  生成随机数长度
   * @param  int $numeric 是否只产生随机数
   * @return string           生成的随机数
   */
  public static function random($length,$numeric = 0)
  {
    $seed = base_convert(md5(microtime() . $_SERVER['DOCUMENT_ROOT']), 16, $numeric ? 10 : 35);
    $seed = ($numeric ? str_replace('0', '', $seed) . '012340567890' : $seed . 'zZ' . strtoupper($seed));
    $hash = '';
    $max = strlen($seed) - 1;

    for ($i = 0; $i < $length; $i++) {
      $hash .= $seed{mt_rand(0, $max)};
    }

    return $hash;
  }
  /**
   * 加密函数
   *
   * @param string $txt 需要加密的字符串
   * @param string $key 密钥
   * @return string 返回加密结果
   */
   public static function encrypt($txt,$key='')
   {
     if (empty($txt)) return $txt;
   	if (empty($key)) $key = md5(MD5_KEY);


   	$chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.';
   	$ikey = '-x6g6ZWm2G9g_vr0Bo.pOq3kRIxsZ6rm';
   	$nh1 = rand(0, 64);
   	$nh2 = rand(0, 64);
   	$nh3 = rand(0, 64);
   	$ch1 = $chars{$nh1};
   	$ch2 = $chars{$nh2};
   	$ch3 = $chars{$nh3};
   	$nhnum = $nh1 + $nh2 + $nh3;
   	$knum = 0;
   	$i = 0;

   	while(isset($key{$i})) {
   		$knum +=ord($key{$i++});
   	}

   	$mdKey = substr(md5(md5(md5($key . $ch1) . $ch2 . $ikey) . $ch3), $nhnum % 8, ($knum % 8) + 16);
   	$txt = base64_encode(time().'_'.$txt);
   	$txt = str_replace(array('+','/','='),array('-','_','.'),$txt);
   	$tmp = '';
   	$j = 0;
   	$k = 0;
   	$tlen = strlen($txt);
   	$klen = strlen($mdKey);

   	for ($i = 0; $i < $tlen; $i++) {
   		$k = ($k == $klen ? 0 : $k);
   		$j = ($nhnum+strpos($chars,$txt{$i})+ord($mdKey{$k++}))%64;
   		$tmp .= $chars{$j};
   	}

   	$tmplen = strlen($tmp);
   	$tmp = substr_replace($tmp, $ch3, $nh2 % ++$tmplen, 0);
   	$tmp = substr_replace($tmp, $ch2, $nh1 % ++$tmplen, 0);
   	$tmp = substr_replace($tmp, $ch1, $knum % ++$tmplen, 0);
   	return $tmp;
   }

   /**
    * 解密函数
    *
    * @param string $txt 需要解密的字符串
    * @param string $key 密匙
    * @return string 字符串类型的返回结果
    */
   public static function decrypt($txt, $key = '', $ttl = 0){
   	if (empty($txt)) {
   		return $txt;
   	}

   	if (empty($key)) {
   		$key = md5(MD5_KEY);
   	}

   	$chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.";
   	$ikey = "-x6g6ZWm2G9g_vr0Bo.pOq3kRIxsZ6rm";
   	$knum = 0;
   	$i = 0;
   	$tlen = @strlen($txt);

   	while(isset($key{$i})) {
   		$knum +=ord($key{$i++});
   	}

   	$ch1 = @$txt{$knum % $tlen};
   	$nh1 = strpos($chars,$ch1);
   	$txt = @substr_replace($txt,'',$knum % $tlen--,1);
   	$ch2 = @$txt{$nh1 % $tlen};
   	$nh2 = @strpos($chars,$ch2);
   	$txt = @substr_replace($txt,'',$nh1 % $tlen--,1);
   	$ch3 = @$txt{$nh2 % $tlen};
   	$nh3 = @strpos($chars,$ch3);
   	$txt = @substr_replace($txt,'',$nh2 % $tlen--,1);
   	$nhnum = $nh1 + $nh2 + $nh3;
   	$mdKey = substr(md5(md5(md5($key.$ch1).$ch2.$ikey).$ch3),$nhnum % 8,$knum % 8 + 16);
   	$tmp = '';
   	$j = 0;
   	$k = 0;
   	$tlen = @strlen($txt);
   	$klen = @strlen($mdKey);

   	for ($i = 0; $i < $tlen; $i++) {
   		$k = ($k == $klen ? 0 : $k);
   		$j = strpos($chars,$txt{$i})-$nhnum - ord($mdKey{$k++});

   		while ($j < 0) {
   			$j += 64;
   		}

   		$tmp .= $chars{$j};
   	}

   	$tmp = str_replace(array('-','_','.'),array('+','/','='),$tmp);
   	$tmp = trim(base64_decode($tmp));

   	if (preg_match("/\d{10}_/s", substr($tmp, 0, 11))) {
   		if ((0 < $ttl) && ($ttl < (time() - substr($tmp, 0, 11)))) {
   			$tmp = NULL;
   		} else {
   			$tmp = substr($tmp, 11);
   		}
   	}

   	return $tmp;
   }
   /**
    * 当访问的act或op不存在时调用此函数并退出脚本
    *
    * @param string $act
    * @param string $op
    * @return void
    * @todo 相应的扩展
    */
    public static function requestNotFound($act = null, $op = null) {
   	self::showMessage('您访问的页面不存在!', SHOP_SITE_URL, 'exception', 'error', 1, 3000);
   	exit();
   }
   /**
    * 输出信息
    *
    * @param string $msg 输出信息
    * @param string/array $url 跳转地址 当$url为数组时，结构为 array('msg'=>'跳转连接文字','url'=>'跳转连接');
    * @param string $show_type 输出格式 默认为html
    * @param string $msg_type 信息类型 succ 为成功，error为失败/错误
    * @param string $is_show  是否显示跳转链接，默认是为1，显示
    * @param int $time 跳转时间，默认为2秒
    * @return string 字符串类型的返回结果
    */
   public static function showMessage($msg,$url='',$show_type='html',$msg_type='succ',$is_show=1,$time=2000){
   	$language = Easy::$app->getLanguage();
   	$language->read('core_lang_index');
   	$lang	= $language->getLanguageContent();
   	/**
   	 * 如果默认为空，则跳转至上一步链接
   	 */
   	$url = ($url!='' ? $url : Easy::$app->request->getReferer());

   	$msg_type = in_array($msg_type,array('succ','error')) ? $msg_type : 'error';

   	/**
   	 * 输出类型
   	 */
   	switch ($show_type){
   		case 'json':
   			$return = '{';
   			$return .= '"msg":"'.$msg.'",';
   			$return .= '"url":"'.$url.'"';
   			$return .= '}';
   			echo $return;
   			break;
   		case 'exception':
   			echo '<!DOCTYPE html>';
   			echo '<html>';
   			echo '<head>';
   			echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
   			echo '<title></title>';
   			echo '<style type="text/css">';
   			echo 'body { font-family: "Verdana";padding: 0; margin: 0;}';
   			echo 'h2 { font-size: 15px; line-height: 30px; border-bottom: 1px dashed #CCC; padding-bottom: 8px;width:800px; margin: 20px 0 0 150px;}';
   			echo 'dl { float: left; display: inline; clear: both; padding: 0; margin: 10px 20px 20px 150px;}';
   			echo 'dt { font-size: 14px; font-weight: bold; line-height: 40px; color: #333; padding: 0; margin: 0; border-width: 0px;}';
   			echo 'dd { font-size: 12px; line-height: 40px; color: #333; padding: 0px; margin:0;}';
   			echo '</style>';
   			echo '</head>';
   			echo '<body>';
   			echo '<h2>'.$lang['error_info'].'</h2>';
   			echo '<dl>';
   			echo '<dd>'.$msg.'</dd>';
   			echo '<dt><p /></dt>';
   			echo '<dd>'.$lang['error_notice_operate'].'</dd>';
   			echo '<dd><p /><p /><p /><p /></dd>';
   			echo '<dd><p /><p /><p /><p />Copyright 2007-2015 EasyPHP, All Rights Reserved '.$lang['company_name'].'</dd>';
   			echo '</dl>';
   			echo '</body>';
   			echo '</html>';
   			exit;
   			break;
   		case 'javascript':
   			echo "<script>";
   			echo "alert('". $msg ."');";
   			echo "location.href='". $url ."'";
   			echo "</script>";
   			exit;
   			break;
   		case 'tenpay':
   			echo "<html><head>";
   			echo "<meta name=\"TENCENT_ONLINE_PAYMENT\" content=\"China TENCENT\">";
   			echo "<script language=\"javascript\">";
   			echo "window.location.href='" . $url . "';";
   			echo "</script>";
   			echo "</head><body></body></html>";
   			exit;
   			break;
   		default:
   		    /**
   		     * 不显示右侧工具条
   		     */
   		    Tpl::output('hidden_nctoolbar', 1);
   			if (is_array($url)){
   				foreach ($url as $k => $v){
   					$url[$k]['url'] = $v['url']?$v['url']:Easy::getReferer();
   				}
   			}
   			/**
   			 * 读取信息布局的语言包
   			 */
   			Language::read("msg");
   			/**
   			 * html输出形式
   			 * 指定为指定项目目录下的error模板文件
   			 */
               Tpl::setDir('');
   			Tpl::output('html_title',Language::get('nc_html_title'));
   			Tpl::output('msg',$msg);
   			Tpl::output('url',$url);
   			Tpl::output('msg_type',$msg_type);
   			Tpl::output('is_show',$is_show);
   			Tpl::showpage('msg','msg_layout',$time);
   	}
   	exit;
   }
   /**
    * 抛出异常
    *
    * @param string $error 异常信息
    */

   public static function throwException($error){
     if (!defined('IGNORE_EXCEPTION')){
   		self::showMessage($error, '', 'exception');
   	}else{
   		exit();
   	}
   }
   /**
    * 输出错误信息
    *
    * @param string $error 错误信息
    */
   public static function halt($error){
   	self::showMessage($error,'','exception');
   }
   /**
    * 文件数据读取和保存 字符串、数组
    *
    * @param string $name 文件名称（不含扩展名）
    * @param mixed $value 待写入文件的内容
    * @param string $path 写入cache的目录
    * @param string $ext 文件扩展名
    * @return mixed
    */
   public static function file($name, $value = null, $path = 'cache', $ext = '.php') {
     if (strtolower(substr($path,0,5)) == 'cache'){
       $path  = 'data/'.$path;
     }
     static $_cache = array();
     if (isset($_cache[$name.$path])) return $_cache[$name.$path];
       $filename = $path.'/'.$name.$ext;
       if (!is_null($value)) {
           $dir = dirname($filename);

           if (!is_dir($dir)) mkdir($dir);
           return File::write($filename,$value);
       }

       if (is_file($filename)) {
           $_cache[$name.$path] = $value = include $filename;
       } else {
           $value = false;
       }
       return $value;
   }
   /**
    * 取得商品默认大小图片
    *
    * @param string $key	图片大小 small tiny
    * @return string
    */
   public static function defaultGoodsImage($key)
   {
   	$file = str_ireplace('.', '_' . $key . '.', easy::$app->getConfig('default_goods_image'));
   	return ATTACH_COMMON . '/' . $file;
   }
   /**
 * 返回模板文件所在完整目录
 * @todo 弃用
 * @param str $tplpath
 * @return string
 */
public static function template($tplpath){
	if (strpos($tplpath,':') !== false){
		$tpltmp = explode(':',$tplpath);
		return BASE_DATA_PATH.'/'.$tpltmp[0].'/tpl/'.$tpltmp[1].'.php';
    } else if (defined('MODULE_NAME')) {
        return MODULES_BASE_PATH . '/views/' . TPL_NAME . '/' . $tplpath . '.php';
	}else{
		return BASE_PATH.'/views/'.TPL_NAME.'/'.$tplpath.'.php';
	}
}
/**
 * 通知邮件/通知消息 内容转换函数
 *
 * @param string $message 内容模板
 * @param array $param 内容参数数组
 * @return string 通知内容
 */
public static function replaceText($message, $param) {
	if (!is_array($param)) {
		return false;
	}

	foreach ($param as $k => $v ) {
		$message = str_replace('{$'.$k.'}',$v,$message);
	}

	return $message;
}
public static function readFileList($path,$ignore = []){
  //添加 DIRECTORY_SEPARATOR 在 $path 末尾
  $path .= substr($path,-1) == DIRECTORY_SEPARATOR ? '' : DIRECTORY_SEPARATOR;
  $list = [];
  foreach(glob($path . "*") as $value) {
    if(!in_array($value, $ignore)) {
      if(is_dir($value)) {
        $list = array_merge($list,self::readFileList($value,$ignore));
      } else {
        $list[] = $value;
      }
    }
  }

  return $list;
}

/**
 * 返回以原数组某个值为下标的新数据
 *
 * @param array $array
 * @param string $key
 * @param int $type 1一维数组2二维数组
 * @return array
 */
public static function array_under_reset($array, $key, $type=1){
	if (is_array($array)){
		$tmp = array();
		foreach ($array as $v) {
			if ($type === 1){
				$tmp[$v[$key]] = $v;
			}elseif($type === 2){
				$tmp[$v[$key]][] = $v;
			}
		}
		return $tmp;
	}else{
		return $array;
	}
}
    /**
     * Converts a CamelCase name into an ID in lowercase.
     * Words in the ID may be concatenated using the specified character (defaults to '-').
     * For example, 'PostTag' will be converted to 'post-tag'.
     * @param string $name the string to be converted
     * @param string $separator the character used to concatenate the words in the ID
     * @param bool|string $strict whether to insert a separator between two consecutive uppercase chars, defaults to false
     * @return string the resulting ID
     */
public static function camel2id($name, $separator = '-', $strict = false)
   {
       $regex = $strict ? '/[A-Z]/' : '/(?<![A-Z])[A-Z]/';
       if ($separator === '_') {
           return strtolower(trim(preg_replace($regex, '_\0', $name), '_'));
       }
       return strtolower(trim(str_replace('_', $separator, preg_replace($regex, $separator . '\0', $name)), $separator));
   }
   /**
        * Returns the trailing name component of a path.
        * This method is similar to the php function `basename()` except that it will
        * treat both \ and / as directory separators, independent of the operating system.
        * This method was mainly created to work on php namespaces. When working with real
        * file paths, php's `basename()` should work fine for you.
        * Note: this method is not aware of the actual filesystem, or path components such as "..".
        *
        * @param string $path A path string.
        * @param string $suffix If the name component ends in suffix this will also be cut off.
        * @return string the trailing name component of the given path.
        * @see http://www.php.net/manual/en/function.basename.php
        */
       public static function basename($path, $suffix = '')
       {
           if (($len = mb_strlen($suffix)) > 0 && mb_substr($path, -$len) === $suffix) {
               $path = mb_substr($path, 0, -$len);
           }
           $path = rtrim(str_replace('\\', '/', $path), '/\\');
           if (($pos = mb_strrpos($path, '/')) !== false) {
               return mb_substr($path, $pos + 1);
           }
           return $path;
       }
}
