<?php
/**
 * 数据处理类
 */
namespace easy\helpers;

use easy;
class Data{
  public static function flexigridXML($flexigridXML){

      $page  = $flexigridXML['now_page'];
      $total = $flexigridXML['total_num'];
      header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
      header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
      header("Cache-Control: no-cache, must-revalidate");
      header("Pragma: no-cache");
      header("Content-type: text/xml");
      $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
      $xml .= "<rows>";
      $xml .= "<page>$page</page>";
      $xml .= "<total>$total</total>";
      if (empty($flexigridXML['list'])) {
          $xml .= "<row id=''>";
          $xml .= "<cell></cell>";
          $xml .= "</row>";
      } else {
          foreach ($flexigridXML['list'] as $k => $v) {
              $xml .= "<row id='" . $k . "'>";
              foreach ($v as $kk => $vv) {
                  $xml .= "<cell><![CDATA[" . $v[$kk] . "]]></cell>";
              }
              $xml .= "</row>";
          }
      }
      $xml .= "</rows>";
      echo $xml;
  }
  public static function flexigridfXML($flexigridXML){
      $page  = $flexigridXML['now_page'];
      $total = $flexigridXML['total_num'];

      header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
      header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
      header("Cache-Control: no-cache, must-revalidate");
      header("Pragma: no-cache");
      header("Content-type: text/xml");
      $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
      $xml .= "<rows>";
      $xml .= "<page>$page</page>";
      $xml .= "<total>$total</total>";
      foreach ($flexigridXML['list'] as $k => $v) {
          $xml .= "<row id='" . $k . "'>";
          $xml .= "<cell><![CDATA[" . $v['operation'] . "]]></cell>";
          $xml .= "<cell><![CDATA[" . $v['channel_name'] . "]]></cell>";
          $xml .= "<cell><![CDATA[" . $v['channel_style'] . "]]></cell>";
          $xml .= "<cell><![CDATA[" . $v['gc_name'] . "]]></cell>";
          $xml .= "<cell><![CDATA[" . $v['channel_show'] . "]]></cell>";
          $xml .= "</row>";
      }

      $xml .= "</rows>";
      echo $xml;
  }

  public static function flexigridXMLfloor($flexigridXML){
      $page  = $flexigridXML['now_page'];
      $total = $flexigridXML['total_num'];

      header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
      header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
      header("Cache-Control: no-cache, must-revalidate");
      header("Pragma: no-cache");
      header("Content-type: text/xml");
      $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
      $xml .= "<rows>";
      $xml .= "<page>$page</page>";
      $xml .= "<total>$total</total>";
      foreach ($flexigridXML['list'] as $k => $v) {
          $xml .= "<row id='" . $k . "'>";
          $xml .= "<cell><![CDATA[" . $v['operation'] . "]]></cell>";
          $xml .= "<cell><![CDATA[" . $v['web_name'] . "]]></cell>";
          $xml .= "<cell><![CDATA[" . $v['web_page'] . "]]></cell>";
          $xml .= "<cell><![CDATA[" . $v['update_time'] . "]]></cell>";
          $xml .= "<cell><![CDATA[" . $v['web_show'] . "]]></cell>";
          $xml .= "</row>";
      }

      $xml .= "</rows>";
      echo $xml;
  }

  public static function flexigroupbuyXML($flexigridXML){
      $page  = $flexigridXML['now_page'];
      $total = $flexigridXML['total_num'];
      header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
      header("Last-Modified: " . gmdate("D, d M Y H:i:s") . "GMT");
      header("Cache-Control: no-cache, must-revalidate");
      header("Pragma: no-cache");
      header("Content-type: text/xml");
      $xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
      $xml .= "<rows>";
      $xml .= "<page>$page</page>";
      $xml .= "<total>$total</total>";
      if (empty($flexigridXML['list'])) {
          $xml .= "<row id=''>";
          $xml .= "<cell></cell>";
          $xml .= "</row>";
      } else {
          foreach ($flexigridXML['list'] as $k => $v) {
              $xml .= "<row id='" . $k . "'>";
              $xml .= "<cell><![CDATA[" . '--' . "]]></cell>";
              foreach ($v as $kk => $vv) {
                  $xml .= "<cell><![CDATA[" . $v[$kk] . "]]></cell>";
              }
              $xml .= "</row>";
          }
      }
      $xml .= "</rows>";
      echo $xml;
  }

  public static function groupedValues($data, $cou_id, $sk_id){

      foreach ($data as $k => $v) {
          $groupvalue[$v[$cou_id]][$v[$sk_id]] = $v[$sk_id];
      }
      return $groupvalue;

  }
  public static function indexedValues($data, $sku_id, $cou_id){
      if (!empty($data)) {
          foreach ($data as $k => $v) {
              $groupvalue[$v[$sku_id]] = $v[$cou_id];
          }
      } else {
          $groupvalue = $data;

      }
      return $groupvalue;
  }
  public static function groupIndexed($data, $cou_id, $sk_id){

      foreach ($data as $k => $v) {
          if ($sk_id == 'sku_id') {
              $groupvalue[$v[$cou_id]][$v[$sk_id]] = array(
                  'price' => $v['price']
              );
          } else if ($sk_id == 'xlevel') {
              $groupvalue[$v[$cou_id]][$v[$sk_id]] = $v;
          }
      }
      return $groupvalue;

  }
  public static function indexed($indexed_data, $indexed_xlevel){
      if ($indexed_xlevel == 'xlevel') {
          foreach ($indexed_data as $indexed_k => $indexed_v) {
              $data[$indexed_v[$indexed_xlevel]] = $indexed_v;
          }

      } elseif ($indexed_xlevel == 'goods_id') {
          foreach ($indexed_data as $index_k => $index_v) {
              $data[$index_v[$indexed_xlevel]] = $index_v;
          }

      }
      return $data;
  }
  public static function uniqueValues($data, $sku_id){
      print_r($data);
      return $data;

  }
  /**
 * 数组转XML格式
 *
 * @param array $arr 数组
 * @param string $root 根节点名称
 * @param int $num 回调次数
 *
 * @return string xml
 */
public static function arrayToXml($arr, $root='xml', $num=0){
    $xml = '';
    if(!$num){
        $num += 1;
        $xml .= '<?xml version="1.0" encoding="utf-8"?>';
    }
    $xml .= "<$root>";
    foreach ($arr as $key=>$val){
        if(is_array($val)){
            $xml.=self::arrayToXml($val,"$key",$num);
        } else {
            $xml.="<".$key."><![CDATA[".$val."]]></".$key.">";
        }
    }
    $xml .="</$root>";
    return $xml;
}
/**
 * XML格式转数组
 *
 * @param  string $xml
 *
 * @return mixed|array
 */
public static function xmlToArray($xml){
    //禁止引用外部xml实体
    libxml_disable_entity_loader(true);
    $xmlstring = simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA);
    $arr = json_decode(json_encode($xmlstring),true);
    return $arr;
}
}
