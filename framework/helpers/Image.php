<?php

/**
 * 图片操作类
 */

namespace easy\helpers;

use easy;

class Image {
  /**
   * 获取图片高度
   * @param  image $image 图片
   * @return int        图片高度
   */
  public function getHeight($image){
    $size = getimagesize($image);
    $height = $size[1];
    return $height;
  }
  /**
   * 获取宽度
   * @param  [type] $image [description]
   * @return [type]        [description]
   */
  public function getWidth($image){
    $size = getimagesize($image);
    $width = $size[0];
    return $width;
  }
  /**
   * 重新生成图片
   * @param  [type] $imageName 新图片名次
   * @param  [type] $image     图片
   * @param  [type] $width     宽度
   * @param  [type] $height    高度
   * @param  [type] $startW    开始宽度
   * @param  [type] $startH    开始高度
   * @param  [type] $scale     比例
   * @return [type]            新图片
   */
  public function resize($imageName,$image,$width,$height,$startW,$startH,$scale){
    $newImageWidth = ceil($width * $scale);
    $newImageHeight = ceil($height * $scale);
    if(easy::$app->getConfig('thumb.cut_type') == 'im'){
      //use imageMagick
      $exec_str = rtrim(easy::$app->getConfig('thumb.impath'),'/').'/convert -quality 100 -crop '.$width.'x'.$height.'+'.$startW.'+'.$startH.' -resize '.$newImageWidth.'x'.$newImageHeight.' '.$image.' '.$imageName;
      exec($exec_str);
    }else{
      //use gd
      list($imageWidth,$imageHeight,$imageType) = getimagesize($image);
      $imageType = image_type_to_mime_type($imageType);
      $newImage= imagecreatetruecolor($newImageWidth, $newImageHeight);
      $white = imagecolorallocate($newImage, 255, 255, 255);
      imagefill($newImage,0,0,$whitee);
      switch ($imageType) {
        case 'image/gif':
          $source = imagecreatefromgif($image);
          break;
        case "image/jpeg":
        case "image/pjpeg":
        case "image/jpg":
          $source = imagecreatefromjpeg($image);
          break;
        case "image/png":
        case "image/x-png":
          $source = imagecreatefrompng($image);
          break;
      }
      $dst_w = $dst_h = 0;
      if($newImageWidth > $width){
        $dst_w = ($newImageWidth-$width)/2;
      }
      if($newImageHeight > $height){
        $dst_h = ($newImageHeight-$height)/2;
      }
      if($dst_w > 0 ){
         imagecopyresampled($newImage,$source,$dst_w,$dst_h,$startW,$startH,$width,$height,$width,$height);
      }else{
        imagecopyresampled($newImage, $source, 0, 0, $startW, $startH, $newImageWidth, $newImageHeight, $width, $height);
      }
      switch ($imageType) {
        case 'image/gif':
          imagegif($newImage,$imageName);
          break;
        case "image/pjpeg":
        case "image/jpeg":
        case "image/jpg":
		  		imagejpeg($newImage,$imageName,100);
				break;
        case "image/png":
  			case "image/x-png":
  				imagepng($newImage,$imageName);
  			break;
      }
    }
    return $imageName;
  }
}
