<?php

/**
 * url.
 * @todo 此类弃用，暂时保存 待处理
 */
namespace easy\helpers;

use easy;

class Url
{
    /**
   * 根据给定路由规则生成url网址
   *
   * * Below are some examples of using this method:
   *
   * ```php
   * // /index?r=site/index
   * echo Url::toRoute('site/index');
   *
   * // /index?r=site/index&src=ref1#name
   * echo Url::toRoute(['site/index', 'src' => 'ref1', '#' => 'name']);
   *
   * // http://www.example.com/index.php?r=site/index
   * echo Url::toRoute('site/index', true);
   *
   * // https://www.example.com/index.php?r=site/index
   * echo Url::toRoute('site/index', 'https');
   * ```
   *
   * @param  string|array  $route  路由规则
   * @param  bool|string $scheme url请求头
   * -‘false’  默认 去相对url地址
   * -’true’   绝对地址
   * -‘string’ http or https 请求的绝对地址
   *
   * @return string          请求的url地址
   */
  public static function toRoute($route, $scheme = false)
  {
      $route = (array) $route;
      //$route[0] = static::normalizeRoute($route[0]);

      if ($scheme) {
          return easy::$app->getUrlManager()->createAbsoluteUrl($route, is_string($scheme) ? $scheme : null);
      } else {
          return easy::$app->getUrlManager()->createUrl($route);
      }
  }
  /**
   * 通过给定的参数生成URL
   * Below are some examples of using this method:.
   *
   * ```php
   * // /index?r=site/index
   * echo Url::to(['site/index']);
   *
   * // /index?r=site/index&src=ref1#name
   * echo Url::to(['site/index', 'src' => 'ref1', '#' => 'name']);
   *
   * // the currently requested URL
   * echo Url::to();
   *
   * // /images/logo.gif
   * echo Url::to('@web/images/logo.gif');
   *
   * // images/logo.gif
   * echo Url::to('images/logo.gif');
   *
   * // http://www.example.com/images/logo.gif
   * echo Url::to('@web/images/logo.gif', true);
   *
   * // https://www.example.com/images/logo.gif
   * echo Url::to('@web/images/logo.gif', 'https');
   * ```
   *
   *
   * @param array|string $url the parameter to be used to generate a valid URL
   * @param bool|string $scheme the URI scheme to use in the generated URL:
   *
   * - `false` (default): generating a relative URL.
   * - `true`: returning an absolute base URL whose scheme is the same as that in [[\yii\web\UrlManager::hostInfo]].
   * - string: generating an absolute URL with the specified scheme (either `http` or `https`)
   * @param bool|string $host absolute url app and must head of 'http://' or 'https://' because here not do judgement;
   * this is [$scheme] must not to be false
   *   $host Below are some examples how to use
   * 1. default: = false
   *  this mode is to use create under application,the absolute url willcreate by this app.
   * 2.othe value
   * this mode will be parse by easylias. if the alias is registered
   *
   * @return string the generated URL
   */
  public static function to($url = '', $scheme = false, $host = false)
  {
      if (is_array($url)) {
          if (!$host === false) {
              // TODO: 二级域名处理方式待优化
            if (intval(easy::$app->getConfig('enabled_subdomain')) == 1 && strpos($host, '.') === false) {
                return 'http://'.$store_domain.'.'.SUBDOMAIN_SUFFIX.'/';
            }
              //  $host = easy::getAlias($host);
                $url = self::_createOtherUrl($url);

              if (!$scheme) {
                  return $host.$url;
              }
                //$url = $url;
          } else {
              return static::toRoute($url, $scheme);
          }
      }

      $url = easy::getAlias($url);
      if ($url === '') {
          $url = easy::$app->getRequest()->getUrl();
      }

      if (!$scheme) {
          return $url;
      }
      if ($host) {
          $host = easy::getAlias($host);
          $url = $host.'/'.ltrim($url, '/');
      }
      if (strncmp($url, '//', 2) === 0) {
          // e.g. //hostname/path/to/resource
          return is_string($scheme) ? "$scheme:$url" : $url;
      }

      if (($pos = strpos($url, ':')) == false || !ctype_alpha(substr($url, 0, $pos))) {
          if($host){
            $url = "http://".'/'.ltrim($url,'/');
        }else{
          // turn relative URL into absolute
          $url = easy::$app->getUrlManager()->getHostInfo().'/'.ltrim($url, '/');
        }
      }

      if (is_string($scheme) && ($pos = strpos($url, ':')) !== false) {
          // replace the scheme with the specified one
          $url = $scheme.substr($url, $pos);
      }

      return $url;
  }
  /**
   * 用于生成 入口文件带路由的地址
   *
   *@todo 重新设计如何生成其他app地址
   */
  protected static function _createOtherUrl($params)
  {
      $params = (array) $params;
      $anchor = isset($params['#']) ? '#'.$params['#'] : '';
      unset($params['#'], $params['r']);

      $route = trim($params[0], '/'); //过滤掉首位 ‘/’ 字符
      unset($params[0]);

      $url = '/index.php?r='.urlencode($route);
      if (!empty($params) && ($query = http_build_query($params)) !== '') {
          $url .= '&'.$query;
      }

      return $url.$anchor;
  }
  /**
   * 获取上一步来源地址
   *
   * @return string 返回结果
   */
  public static function getReferer()
  {
      return empty($_SERVER['HTTP_REFERER']) ? '' : $_SERVER['HTTP_REFERER'];
  }
}
