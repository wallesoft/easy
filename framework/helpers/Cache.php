<?php

/**
 * 缓存辅助类
 *
 * @todo 此类将弃用
 */

namespace easy\helpers;

use easy;
use easy\helpers\Helper;
use easy\base\Exception;
use easy\base\Component;
class Cache extends Component
{

  /**
   * 缓存 读
   * @param  string  $key      缓存名称
   * @param  boolean $callback 缓存读取失败时是否使用回调 true代表使用cache.model中预定义的缓存项 默认不使用回调
   * @return mixed            [description]
   */
  public static function read($key,$callback=false)
  {
    $cacher = Easy::$app->getCache();
    if (!$cacher) {
        throw new Exception('Cannot fetch cache object!');
    }
    $value = $cacher->get($key);

    if ($value === false && $callback !== false) {
        if ($cacher->getCallbackClass()) {
            $callback = array($cacher->getCallBackClassInstance(), 'call');
        }
        if (!is_callable($callback)) {
            Helper::throwException('Error：Cache callback error！！！');
        }

        $value = call_user_func($callback, $key);
        self::write($key, $value);
    }
    return $value;
  }

  /**
   * KV缓存 写
   *
   * @param string $key 缓存名称
   * @param mixed $value 缓存数据 若设为否 则下次读取该缓存时会触发回调（如果有）
   * @param int $expire 缓存时间 单位秒 null代表不过期
   * @return boolean
   */

  public static function write($key,$value,$expire=null)
  {
      $cacher = Easy::$app->getCache();

      if (!$cacher) {
          throw new Exception('Cannot fetch cache object!');
      }

      return $cacher->set($key, $value, NULL, $expire);

  }
  /**
   * delete cache
   * @param  [type] $key [description]
   * @return [type]      [description]
   */
  public static function rm($key)
  {
    $cacher = Easy::$app->getCache();
    if (!$cacher) {
  		throw new Exception('Cannot fetch cache object!');
  	}

  	return $cacher->rm($key);

  }
  /**
   * 删除缓存目录下的文件或子目录文件
   * @param string $dir 目录名
   */
  public static function rmFiles($dir){
    //防止删除cache以外的文件
    if (strpos($dir,'..') !== false) return false;
    $path = Easy::$app->cache->path.'/'.$dir;
    if(is_dir($path)){
      $file_list = Helper::readFileList($path);
      if (!empty($file_list)){
  			foreach ($file_list as $v){
  				if (basename($v) != 'index.html')@unlink($v);
  			}
  		}
    }else{
      if (basename($path) != 'index.html') @unlink($path);
    }
    return true;
  }
}
