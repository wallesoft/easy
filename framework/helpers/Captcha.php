<?php

/**
 * 验证码类
 */
namespace easy\helpers;

use easy;
use easy\helpers\Helper;
use easy\base\Exception;

class Captcha
{
  /**
   * get captcha hash value
   *
   * use route like 'site/login'
   * @param  string $router route value
   * @return string         hash value or ''
   */
  public static function getHash($router='')
  {
      $route = $router ? $router : $_GET['r'];
      // if use captcha when login
      if(easy::$app->getConfig('captcha_status_login')){
        return substr(md5(easy::$app->getConfig('shop_site_url').$route),0,8);
      } else {
        return '';
      }
  }
  /**
   * 产生验证码
   * @param  string $hash 哈希数
   * @return string       验证码
   */
  public static function makeSeccode($hash)
  {
    $seccode = Helper::random(6, 1);//产生随机数
  	$seccodeunits = '';
  	$s = sprintf('%04s', base_convert($seccode, 10, 23));
  	$seccodeunits = 'ABCEFGHJKMPRTVXY2346789';

  	if ($seccodeunits) {
  		$seccode = '';

  		for ($i = 0; $i < 4; $i++) {
  			$unit = ord($s{$i});
  			$seccode .= ((48 <= $unit) && ($unit <= 57) ? $seccodeunits[$unit - 48] : $seccodeunits[$unit - 87]);
  		}
  	}
  	easy::$app->cookie->setCookie('seccode'.$hash, easy::$app->security->encrypt(strtoupper($seccode)."\t".(time())."\t".$hash,MD5_KEY),3600);
  	return $seccode;

  }
  /**
   * 验证验证码
   *
   * @param string $ehash 哈希数
   * @param string $value 待验证值
   * @return boolean
   */
  public static function checkSeccode($ehash, $value) {
  	list($checkvalue, $checktime, $checkidhash) = explode("\t", easy::$app->security->decrypt(easy::$app->cookie->getCookie('seccode'.$ehash),MD5_KEY));
  	$return = $checkvalue == strtoupper($value) && $checkidhash == $ehash;
  	if (!$return) {
  		easy::$app->cookie->setCookie('seccode'.$ehash,'',-3600);
  	}

  	return $return;
  }

}
