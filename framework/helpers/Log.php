<?php
/**
 * 日志记录类
 */
namespace easy\helpers;

use easy;
use easy\base\Component;
class Log extends Component{

  protected $log = []; // 一次执行的记录存储
  public $savePath;    // 日志存储路径
  private $_timeRecord = [];
  /**
   * sql 记录
   * @param  string $message 记录
   */
  public function sqlRecord($message){
    $now = date('Y-m-d H:i:s',time());
    $this->log[] = "[{$now}] SQL: {$message}\r\n";
    $logFile = $this->savePath.'/log/query_'.date('Ymd',time()).'.log';
    $url = "Url:{$_GET['r']}";
    $content = $url.'\r\n';
    foreach($this->log as $val){
      $content .= $val;
    }
    file_put_contents($logFile,$content,FILE_APPEND);
  }

  public function errRecord($message){
    $now = date('Y-m-d H:i:s',time());
    $logFile = $this->savePath.'/log/'.date('Ymd',time()).'.ERR.log';
    $url = $_SERVER['REQUEST_URI'] ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];
    $url .= "( r={$_GET['r']} )";
    $content = "[{$now}] {$url}\r\n ERR: {$message}\r\n";
    file_put_contents($logFile,$content,FILE_APPEND);
  }

  public function runRecord($message){
    $now = date('Y-m-d H:i:s',time());
    $logFile = $this->savePath.'/log/'.date('Ymd',time()).'.RUN.log';
    $url = $_SERVER['REQUEST_URI'] ? $_SERVER['REQUEST_URI'] : $_SERVER['PHP_SELF'];
    $url .= "(r={$_GET['r']})";
    $content = "[{$now}] {$url}\r\n RUN: {$message}\r\n";
    file_put_contents($logFile,$content, FILE_APPEND);
  }

  public function getRecord(){
    return $this->log;
  }
  /**
   * 记录时间精度微秒
   */
  public function timeRecord($start , $end='',$dec = 3){
    if(!empty($end)){
      if(!isset($this->_timeRecord[$end])){
        $this->_timeRecord[$end] = microtime(true);
        }
      return number_format(($this->_timeRecord[$end]-$this->_timeRecord[$start]),$dec);
    }else{
      $this->_timeRecord[$start] = microtime(true);
    }
  }
}
