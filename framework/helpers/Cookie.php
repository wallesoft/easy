<?php

/**
 * Cookie 类
 */

namespace easy\helpers;

use easy;
use easy\helpers\Helper;
use easy\base\Exception;
use easy\base\Component;
class Cookie extends Component
{
  public $cookiePre = '77AC_';
  public $expire = '3600';
  /**
   * 设置cookie
   *
   * @param string $name cookie 的名称
   * @param string $value cookie 的值
   * @param int $expire cookie 有效周期
   * @param string $path cookie 的服务器路径 默认为 /
   * @param string $domain cookie 的域名
   * @param string $secure 是否通过安全的 HTTPS 连接来传输 cookie,默认为false
   */
  public function setCookie($name,$value,$expire='3600',$path = '',$domain='',$secure=false){
    if (empty($path)) {
      $path = '/';
    }

    // if (empty($domain)) {
    //   $domain = (SUBDOMAIN_SUFFIX ? SUBDOMAIN_SUFFIX : '');
    // }

    $name = $this->cookiePre.$name;//(defined('COOKIE_PRE') ? COOKIE_PRE . $name : strtoupper(substr(md5(easy::$app->security->md5Key), 0, 4)) . '_' . $name);
    $expire = intval($expire) ? intval($expire) : intval($this->expire);
    $result = setcookie($name, $value, time() + $expire, $path, $domain, $secure);
    $_COOKIE[$name] = $value;
  }


  /**
   * 获取Cookie值
   * @param  string $name cookie名称
   * @return [type]       [description]
   */
  public function getCookie($name=''){
    $name = $this->cookiePre.$name;
  	return $_COOKIE[$name];
  }

}
