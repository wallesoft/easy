<?php
 /**
  * file 辅助类
  *
  * 文件的读取，写入相关类
  */
 namespace easy\helpers;

 use eays;

 class File {

   /**
    * 内容写入文件
    *
    * @param string $filepath 待写入内容的文件路径
    * @param string/array $data 待写入的内容
    * @param  string $mode 写入模式，如果是追加，可传入“append”
    * @return bool
    */
   public static function write($filepath, $data, $mode = null)
   {
       if (!is_array($data) && !is_scalar($data)) {
           return false;
       }
       $dirname = dirname($filepath);
       if(!is_dir($dirname)){
         mkdir($dirname);
       }
       $data = var_export($data, true);

       $data = "<?php return ".$data.";";
       $mode = $mode == 'append' ? FILE_APPEND : NULL;
       if (false === file_put_contents($filepath,($data),$mode)){
           return false;
       }else{
           return true;
       }
   }


 }
