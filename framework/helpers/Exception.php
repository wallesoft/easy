<?php

/**
 * exception 系统辅助类
 * @todo 待开发
 */
namespace easy\helpers;

use easy;

class Exception
{
  /**
   * 抛出异常
   *
   * @param string $error 异常信息
   */

  public static function throwException($error){
    if (!defined('IGNORE_EXCEPTION')){
  		Helper::showMessage($error, '', 'exception');
  	}else{
  		exit();
  	}
  }
  /**
   * 输出错误信息
   *
   * @param string $error 错误信息
   */
  public static function halt($error){
  	helper::showMessage($error,'','exception');
  }
}
