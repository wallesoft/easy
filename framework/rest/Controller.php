<?php
namespace easy\rest;

use easy;
use easy\web\Response;
//use easy\base\Controller;

class Controller extends easy\base\Controller{
  //default use json
  public $responeFormatter = 'json';
  /**
   * rewrite parent afterAction
   * @param  object $action action id
   * @param  mixed $result after run action ruturn result
   */
  public function afterAction($action,$result){

    try {
        $data = parent::afterAction($action,$result);
        $response = easy::$app->getResponse();
        //$response->data = $data;
        $response->format = $this->responeFormatter;

    } catch (Exception $e) {
        $response->setStatusCode($e->getCode(),$e->getMessage());
    } catch (\Exception $e) {
        $response->setStatusCode($e->getCode(),$e->getMessage());
    }
    $response->data = [
      'status'=>$response->getStatusCode(),
      'data'=>$data,
      'msg'=>$response->statsText,
    ];
    $response->send();
  }
}
