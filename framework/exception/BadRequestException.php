<?php
namespace easy\exception;

use easy\base\Exception;

class BadRequestException extends Exception{
  public function __construct($message, $code = 0) {
    parent::__construct("Bad Request: {$message}", 400+$code);
  }
}
