<?php
namespace easy\exception;

use easy\base\Exception;

class RedirectException extends Exception {

  public function __construct($message,$code = 0) {
    parent::__construct("Redirect: {$message}", 300+$code);
  }
}
