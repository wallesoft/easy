<?php
namespace easy\exception;

use easy\base\Exception;

/**
 * Internal Server Error Exception 服务器错误
 *
 * code 500 501 ...
 * @author jackhunx <43555015@qq.com>
 */
class InternalServerErrorException extends Exception
{
  public function __construct($message, $code = 0){
    parent::__construct("Internal Server Error:{$message}",500+$code);
  }
}
