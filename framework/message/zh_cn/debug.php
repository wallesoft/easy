<?php
//调试模式语言包
$lang['debug_current_page']			= "当前页面";
$lang['debug_request_time']			= "请求时间";
$lang['debug_execution_time']		= "页面执行时间";
$lang['debug_memory_consumption']	= "占用内存";
$lang['debug_request_method']		= "请求方法";
$lang['debug_communication_protocol']= "通信协议";
$lang['debug_user_agent']			= "用户代理";
$lang['debug_session_id']			= "会话ID";
$lang['debug_logging']				= "日志记录";
$lang['debug_logging_count']				= "条日志";
$lang['debug_logging_countnull']				= "无日志记录";
$lang['debug_load_files']			= "加载文件";
$lang['debug_trace_title']			= "页面Trace信息";
