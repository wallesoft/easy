<?php

/**
 * title
 */
$lang['ws_hello']	= "您好";
$lang['ws_logout']	= "退出";
$lang['ws_guest']	= "游客";
$lang['ws_login']	= "登录";
$lang['ws_register']	= "注册";
$lang['ws_usercenter']	= "用户中心";
$lang['ws_message']		= "站内信";
$lang['ws_helpcenter']	= "帮助中心";
$lang['ws_index']		= "首页";
$lang['ws_searchgoods']	= "搜索商品";
$lang['ws_searchstore']	= "搜索店铺";
$lang['ws_searchgroupbuy']	= "搜索团购";
$lang['ws_searchdefault']=" 搜索其实很容易！";
$lang['ws_iwantbuy']	= "我要买";
$lang['ws_iwantsell']	= "我要卖";

/**
 * showMessage跳转中的文字
 */
$lang['ws_html_title']	= "系统信息";

/**
 * footer中的文字
 */
$lang['ws_index']		= "首页";
$lang['ws_pageexecute']	= "页面执行";
$lang['ws_second']		= "秒";
$lang['ws_query']		= "查询";
$lang['ws_times']		= "次";
$lang['ws_online']		= "在线";
$lang['ws_person']		= "人";
$lang['ws_enabled']		= "已启用";
$lang['ws_disabled']	= "已禁用";
$lang['ws_memorycost']	= "占用内存";
?>
