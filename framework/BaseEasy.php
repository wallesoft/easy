<?php
/**
 * 基础类
 * @author jackhunx
 * @version since  v1.0
 * Copyright (c) 2016 WebBetter.Inc. All Rights Reserved.
 *
 */

namespace easy;

use ReflectionClass;
use easy\base\Exception;
/*
 * 设置应用开始时时间
 */
defined('E_BEGIN_TIME') or define('E_BEGIN_TIME', microtime(true));
/**
 * 设置时间戳
 */
defined('E_TIME') or define('E_TIME',time());
/*
 * 设置框架路径
 */
defined('E_PATH') or define('E_PATH', __DIR__);
/*
 * 调试开关
 */
defined('E_DEBUG') or define('E_DEBUG', false);

/**
 * 系统辅助基类.
 */
class BaseEasy
{
    /**
     * 用于类自动加载.
     * array key value is class name,value is file path.
     */
    public static $classMap = [];
    /**
     * @var array 注册的路径别名信息
     */
    public static $aliases = [];
    /**
     * @var \easy\web\application 的实例化对象
     */
    public static $app;
    /**
     * Class autoload loader.
     *
     * 1.search $classMap.
     * 2.by alias .
     *
     * @todo 如果没有此文件加载，如何处理
     */
    public static function autoload($class)
    {
        //search classMap
        if (isset(static::$classMap[$class])) {
            $classFile = static::$classMap[$class];
        } elseif (strpos($class, '\\') !== false) {
            $classFile = static::getAlias('@'.str_replace('\\', '/', $class).'.php',false);
            if ($classFile === false || !is_file($classFile)) {
                return;
            }
        } else {//dosomething
            return;
        }
        include $classFile;
    }

    /**
     * 根据别名获取真实路径.
     *
     * @todo 是否添加抛出异常
     *
     * @param $alias 路径别名
     *
     * @return string 真实路径
     */
    public static function getAlias($alias,$throwException = true)
    {
        if (strncmp($alias, '@', 1)) {
            // not an alias
          return $alias;
        }
        $pos = strpos($alias, '/');
        $root = $pos === false ? $alias : substr($alias, 0, $pos);

        if (isset(static::$aliases[$root])) {
            if (is_string(static::$aliases[$root])) {
                return $pos === false ? static::$aliases[$root] : static::$aliases[$root].substr($alias, $pos);
            } else {
                foreach (static::$aliases[$root] as $name => $path) {
                    if (strpos($alias.'/', $name.'/') === 0) {
                        return $path.substr($alias, strlen($name));
                    }
                }
            }
        }

        if ($throwException) {
            throw new Exception("Invalid path alias: $alias");
        } else {
            return false;
        }
    }

    /**
     * 获取配置信息.
     *
     * @param string $name 配置名称
     *
     * @return mix 成功返回配置信息，失败返回false
     */
    public static function getConfig($name)
    {
        $config = self::$app->getConfig();
        if (isset($config[$name])) {
            return $config[$name];
        }

        return false;
    }
    /**
     * @param $object
     * @param $properties
     *
     * @return mixed
     */
    public static function configure($object, $properties)
    {
        foreach ($properties as $name => $value) {
            $object->$name = $value;
        }

        return $object;
    }

    /**
     * @param $type
     * @param array $params
     *
     * @return mixed
     *
     * @throws Exception
     */
    public static function createObject($type, $params = [])
    {
        if (is_string($type)) {
            //type like 'easy\base\Log'.
            $reflector = new ReflectionClass($type);

            return $reflector->newInstanceArgs($params);
        } elseif (is_array($type) && isset($type['class'])) {
            $class = $type['class'];
            unset($type['class']);
           //利用反射机制生成实例
           $reflector = new ReflectionClass($class);

            return $reflector->newInstanceArgs([$type]);
        } elseif (is_array($type)) {
            throw new Exception("Object configuration is must be an array containing with a 'class' element. ");
        } else {
            throw new Exception('Unsupported configration type :'.gettype($type));
        }
    }
    /**
     * 设置路径别名.
     *
     * @param $alias 路径别名
     * @param $path 真实路径
     */
    public static function setAlias($alias, $path)
    {
        if (strncmp($alias, '@', 1)) {
            $alias = '@'.$alias;
        }
        $pos = strpos($alias, '/');
        $root = $pos === false ? $alias : substr($alias, 0, $pos);
        if ($path !== null) {
            $path = strncmp($path, '@', 1) ? rtrim($path, '\\/') : static::getAlias($path);
            if (!isset(static::$aliases[$root])) {
                if ($pos === false) {
                    static::$aliases[$root] = $path;
                } else {
                    static::$aliases[$root] = [$alias => $path];
                }
            } elseif (is_string(static::$aliases[$root])) {
                if ($pos === false) {
                    static::$aliases[$root] = $path;
                } else {
                    static::$aliases[$root] = [
                      $alias => $path,
                      $root => static::$aliases[$root],
                  ];
                }
            } else {
                static::$aliases[$root][$alias] = $path;
                krsort(static::$aliases[$root]);
            }
        } elseif (isset(static::$aliases[$root])) {
            if (is_array(static::$aliases[$root])) {
                unset(static::$aliases[$root][$alias]);
            } elseif ($pos === false) {
                unset(static::$aliases[$root]);
            }
        }
    }
}
