<?php
/**
 * 引导文件
 *
 * 初始化
 */

 error_reporting(E_ALL & ~E_NOTICE);

 require(__DIR__.'/BaseEasy.php');

 /**
  * BaseEasy 的继承类
  *
  * 如需扩展BaseEasy类请在此类中操作
  * @author Jack Hunx <jack.hunx@gmail.com>
  * @since 1.0.0
  */

  class Easy extends \easy\BaseEasy
  {
  }
  //导入类对应列表
  Easy::$classMap = include(__DIR__.'/classes.php');
  //注册自动加载函数
  spl_autoload_register(['Easy','autoload'],true,true);
