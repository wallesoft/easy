<?php
namespace easy\db;

use easy;
use easy\base\Exception;
class QueryBuild extends easy\base\EObject{
  //db connection
  public $db;
  public $tablePrefix;
  public $sortDesc = 'desc';
  /**
   * 查询表达式
   * @var string
   */
  protected $sqlQuery  =     'SELECT%DISTINCT% %FIELD% FROM %TABLE%%INDEX%%JOIN%%WHERE%%GROUP%%HAVING%%ORDER%%LIMIT% %UNION%';
  /**
   *bulilders
   */
  protected $conditionBuilders = [
      'NOT' => 'buildNotCondition',
      'AND' => 'buildAndCondition',
      'OR' => 'buildAndCondition',
      'BETWEEN' => 'buildBetweenCondition',
      'NOT BETWEEN' => 'buildBetweenCondition',
      'IN' => 'buildInCondition',
      'NOT IN' => 'buildInCondition',
      'LIKE' => 'buildLikeCondition',
      'NOT LIKE' => 'buildLikeCondition',
      'OR LIKE' => 'buildLikeCondition',
      'OR NOT LIKE' => 'buildLikeCondition',
      'EXISTS' => 'buildExistsCondition',
      'NOT EXISTS' => 'buildExistsCondition',
  ];
  /**
   * @var array map of chars to their replacements in LIKE conditions.
   * By default it's configured to escape `%`, `_` and `\` with `\`.
   */
  protected $likeEscapingReplacements = [
      '%' => '\%',
      '_' => '\_',
      '\\' => '\\\\',
  ];
  /**
   * @var string|null character used to escape special characters in LIKE conditions.
   * By default it's assumed to be `\`.
   */
  protected $likeEscapeCharacter;


  public function __construct($connection,$config=[]){
    $this->db=$connection;
    $this->tablePrefix = $connection->tablePrefix;
    parent::__construct($config);
  }
  public function build(Query $query){
    $query->prepare($this);
      $sql = str_replace(
        ['%TABLE%','%DISTINCT%','%FIELD%','%JOIN%','%WHERE%','%GROUP%','%HAVING%','%ORDER%','%LIMIT%','%UNION%','%INDEX%'],[
            $this->buildTable($query),
            $this->buildDistinct(isset($query->distinct)?$query->distinct:false),
            $this->buildField(isset($query->select)?$query->select:'*'),
            $this->buildJoin($query),
            $this->buildWhere(isset($query->where)?$query->where:''),
            $this->buildGroupBy(isset($query->groupBy)?$query->groupBy:''),
            $this->buildHaving(isset($query->having)?$query->having:''),
            $this->buildOrderBy(isset($query->orderBy)?$query->orderBy:''),
            $this->buildLimit($query),
            $this->buildUnion(isset($query->union)?$query->union:''),
            $this->buildIndex(isset($query->index)?$query->index:'')
        ],$this->sqlQuery);
        $query->setSql($sql);
        return $sql;
    }

  public function buildTable(Query $query){
    $tables = $query->table;
    if(is_array($tables)){
      $array = [];
      foreach($tables as $table=>$alias){
        if(!is_numeric($table)){
          $array[] = $this->getTableName($table).' AS '.$alias;
        }else{
          $array[] = $this->getTableName($alias);
        }
      }
      $tables = $array;
    }elseif(is_string($tables)){
        $tables = explode(',',$tables);
        foreach ($tables as $key => $value) {
          $tables[$key] = $this->getTableName($value);
        }
      }
    return implode(',',$tables);

  }
  public function buildDistinct($distinct){
    return !empty($distinct)?   ' DISTINCT '.$distinct.',' :'';
  }
  public function buildField($columns)
  {
    if(is_string($columns) && strpos($fields,',')){
      //like 'id,name,title' ['id','name','title']
      $columns = explode(',',$columns);
    }
    if(is_array($columns)){
        $array = [];
        foreach($columns as $key=>$column){
          if(!is_numeric($key)){
            $array[] = $key.' AS '.$column;
          }else{
            $array[] = $column;
          }
        }
        $columnStr = implode(',',$array);
    }elseif(is_string($columns) && !empty($columns)){
      $columnStr = $columns;
    }else{
      $columnStr = '*';
    }
     return $columnStr;
  }
  /**
   * JOIN
   * @param  Array $join array [$type,$table,$on]
   * @return [type]       [description]
   */
  public function buildJoin($query){
    if(!isset($query->join)){
      return '';
    }
    $joins = $query->join;
    foreach($joins as $i=>$join){
      if(!is_array($join) || !isset($join[0],$join[1])){
        throw new Exception("A join must special as an array of join type,join table");
      }
      // 0:join type, 1:join table, 2:on-condition (optional)
      list($joinType,$table) = $join;
      //set alias
      $table = $this->getTableName($table).' AS '.$table;
      $joins[$i]=" $joinType $table";
      //on-condition
      if(isset($join[2])){
        $joins[$i] .= $join[2]?' ON '.$join[2]:'';
      }
    }
    return implode(' ',$joins);
  }
  public function getTableName($table){
    return '`'.$this->db->tablePrefix.$table.'`';
  }
  /**
   * build where
   * 两种模式
   * [operator,operator1,operator2,...]
   * ['name'=>value,....] hash mode
   */
  public function buildWhere($condition){
    $where = $this->buildCondition($condition);
    return $where === '' ? '' :' WHERE '.$where;
  }

  public function buildCondition($condition){
    if(!is_array($condition)){
      return (string)$condition;
    }elseif(empty($condition)){
      return '';
    }
    //operator format:[条件，参数，参数，...]
    if(isset($condition[0])){
        $operator = strtoupper($condition[0]);
        if(isset($this->conditionBuilders[$operator])){
          $method = $this->conditionBuilders[$operator];
        }else{
          $method = 'buildSimpleCondition';
        }
        //array shift condition[0]
        array_shift($condition);
        return $this->$method($operator,$condition);
    }
    //hash format:['name'=>'value',....]
    return $this->buildHashCondition($condition);
  }
  //hash format condition buildHashCondition
  public function buildHashCondition($condition){
    $parts = [];
    foreach($condition as $column=>$value){
      //['column' =>['value','value1',....]] is in condition like where column IN ('value','value1',...)
      //will use [buildInCondition]
      if(is_array($value) || $value instanceof Query){
        //IN condition
        $parts[] = $this->buildInCondition('IN',[$column,$value]);
      }else{
        if(strpos($column,'(') === false){
            $column = $this->db->quoteColumnName($column); // add quote for column like `name`
        }
        // null
        if($value === null ){
          $parts[]="$column IS NULL";
        }else{
          $value = $this->parseValue($value);
          $parts[]="$column=$value";
        }

      }

    }
    return count($parts) === 1 ? $parts[0] : '('.implode(') AND (',$parts).')';
  }
  //parseValue
  public function parseValue($value){
    if(is_string($value) || is_numeric($value)){
      $value =  '\''.$this->escapeString($value).'\'';
    }elseif(is_null($value)){
        $value   =  'NULL';
    }
    return $value;
  }
  public function escapeString($value){
    $value = addslashes(stripcslashes($value));
    return $value;
  }
  /**
   * bulid simple sql condtion like `"column" operator value`
   * @param  string $operator  use e.g. '<','>','<>','<=', etc.
   * @param  array $operands contains two column names.
   * @return string sql
   */
  public function buildSimpleCondition($operator,$operands)
  {
    if(count($operands) !==2 ){
      throw new Exception('Operator '.$operator.' requires two operands.');
    }
    list($column,$value) = $operands;
    if(strpos($column,'(') === false){
      $column = $this->db->quoteColumnName($column);
    }
    if($value === null){
      return "$column $operator NULl";
    }elseif($value instanceof Query){
      $sql = $this->build($value);
      return "$column $operator ($sql)";
    }
    return "$column $operator $value";
  }
  //not condition builder
  public function buildNotCondition($operator,$operands)
  {
    if(count($operands)!==1){
      throw new Exception("Operator '$operator' requiers exactly one operand");
    }
    $operand = reset($operands);// reset array to first
    if(is_array($operand)){
       $operand = $this->buildCondition($operand);
    }
    if($operand === ''){
      return '';
    }
    return "$operator ($operand)";
  }
  //and condition builder
  public function buildAndCondition($operator,$operands)
  {
    $parts = [];
    foreach($operands as $operand){
      if(is_array($operand)){
        $operand = $this->buildCondition($operand);
      }
      if($operand !== ''){
        $parts[] = $operand;
      }
    }
    if(!empty($parts)){
      return '('.implode(") $operator (",$parts).')';
    }
    return '';
  }
  //Between condition builder
  public function buildBetweenCondition($operator,$operands)
  {
    // $operator between $value1 and $value2;
    if(!isset($operands[0],$operands[1],$operands[2])){
      throw new Exception("Operator '$operator' requires three operands");
    }
    list($column,$value1,$value2) = $operands;
    if(strpos($column,'(') === false){
      $this->db->quoteColumnName($column);
    }
    return "$column $operator $value1 AND $value2";
  }

  /**
   * bulid in and not in
   * e.g. ... column IN (values) or ...column NOT IN (values)
   * @param  string $operator IN or NOT IN
   * @param  Array $operands require two operands [column,$values]
   * @return string           sql string
   */
  public function buildInCondition($operator,$operands)
  {
    if(!isset($operands[0],$operands[1])){
      throw new Exception("Operator '$operator' requires two operands.");
    }
    list($column,$values) = $operands;
    if($column === []){
      return $operator === 'IN' ? '0=1' : '';
    }
    if($values instanceof Query){
      return $this->buildSubqueryInCondition($operator,$column,$values);
    }
    if (!is_array($values) && !$values instanceof \Traversable) {
        // ensure values is an array
        $values = (array) $values;
    }
    if (is_array($column) && count($column) > 1) {
        return $this->buildCompositeInCondition($operator, $column, $values);
    } elseif (is_array($column)) {
        $column = reset($column);
    }
    $sqlValues = [];
    foreach($values as $i=>$value){
      if (is_array($value) || $value instanceof \ArrayAccess) {
          $value = isset($value[$column]) ? $value[$column] : null;
      }
      if ($value === null) {
          $sqlValues[$i] = 'NULL';
      }else{
        $sqlValues[$i] = $value;
      }
    }
    if(empty($sqlValues)){
      return $operator === 'IN' ? '0=1': '';
    }

    if (strpos($column, '(') === false) {
        $column = $this->db->quoteColumnName($column);
    }
    if(count($sqlValues)>1){
    return "$column $operator".' ("'.implode('", "',$sqlValues).'")';
    }
    //if $sqlValues just one value
    $operator = $operator === 'IN' ? '=':'<>';
    return $column.$operator.reset($sqlValues);
  }
  //build sql for IN condition
  public function buildSubqueryInCondition($operator,$columns,$values){

    $sql = $this->build($values);
    if (is_array($columns)) {
        foreach ($columns as $i => $col) {
            if (strpos($col, '(') === false) {
                $columns[$i] = $this->db->quoteColumnName($col);
            }
        }

        return '("' . implode('", "', $columns) . '") '. $operator.' '. ($sql);
    }
    if (strpos($columns, '(') === false) {
        $columns = $this->db->quoteColumnName($columns);
    }

    return "$columns $operator ($sql)";
  }
  // bulids sql fon in condtion
  public function buildCompositeInCondition($operator,$columns,$values){
    $vss = [];
    foreach($values as $value){
      $vs =[];
      foreach($columns as $column){
        if(isset($value[$column])){
          $vs[]=$value[$column];
        }else{
          $vs[] ='NULL';
        }
      }
       $vss[] = '(' . implode(', ', $vs) . ')';
    }
    if(empty($vss)){
      return $operator === 'IN' ? '0=1' : '';
    }
    $sqlColumns = [];
    foreach ($columns as $i => $column) {
        $sqlColumns[] = strpos($column, '(') === false ? $this->db->quoteColumnName($column) : $column;
    }

    return '(' . implode(', ', $sqlColumns) . ") ". $operator ." (" . implode(', ', $vss) . ')';
  }
  //like condition builder
  public function buildLikeCondition($operator,$operands)
  {
    if(!isset($operands[0],$operands[1])){
      throw new Exception("Operator '$operator' requires two operands.");
    }
    $escape = isset($operands[2]) ? $operands[2] : $this->likeEscapingReplacements;
    unset($operands[2]);
    if(!preg_match('/^(AND |OR |)(((NOT |))I?LIKE)/',$operator,$matches)){
      throw new Exception("Invalid operator '$operator'.");
    }
    list($column, $values) = $operands;

    if (!is_array($values)) {
        $values = [$values];
    }

    if (empty($values)) {
        return $not ? '' : '0=1';
    }

    if (strpos($column, '(') === false) {
        $column = $this->db->quoteColumnName($column);
    }
    $parts = [];
    foreach($values as $value){
      $val = empty($escape) ? $value : ('"%' . strtr($value, $escape) . '%"');
      //@todo escape characte
      $escapeSql = '';
      if ($this->likeEscapeCharacter !== null) {
          $escapeSql = " ESCAPE '{$this->likeEscapeCharacter}'";
      }
      $parts[] = "{$column} {$operator} {$val}{$escapeSql}";
    }
    return implode($andor, $parts);
  }
  //exists condtion builder
  public function buildExistsCondition($operator,$operands)
  {
    if ($operands[0] instanceof Query) {
        list($sql, $params) = $this->build($operands[0]);
        return "$operator ($sql)";
    }
    throw new Exception('SubQuery for EXISTS operator must be a Query Object');
  }

  /**
   * build group by
   * @param  Array $columns
   * @return string          sql string by groupby
   */
  public function buildGroupBy($columns){
    if(empty($columns)){
      return '';
    }
    foreach ($columns as $i => $column) {
      if(strpos($column,'(') === false){
        $columns[$i] = $this->db->quoteColumnName($column);
      }
    }
    return 'GROUP BY '.implode(', ',$columns);
  }
  //build having
  public function buildHaving($condition){
    $having = $this->buildCondition($condition);
    return $having ==='' ? '' : ' HAVING '.$having;
  }
  //build order
  public function buildOrderBy($columns){
    if(empty($columns)){
      return '';
    }
    $orders = [];
    foreach($columns as $name=>$direction){
      $orders[] = $this->db->quoteColumnName($name).($direction === $this->sortDesc ? ' DESC' :'');
    }
    return 'ORDER BY '.implode(', ',$orders);
  }
  public function buildLimit(Query $query){
    if($query->page != null){
      $this->buildPage($query);
    }
    $sql = '';
    if(is_string($query->limit) && !is_set($query->offset))
     $sql = ' LIMIT '.$qurey->limit.' ';
    if(ctype_digit((string) $query->limit)){
      $sql .= ' LIMIT '.$query->limit;
    }
    if(ctype_digit((string) $query->offset) && (string) $query->offset !== '0'){
      $sql .=' OFFSET '.$query->offset;
    }
    return $sql;
  }
  public function buildPage(Query $query){
    list($num,$total) = $query->page;
    if(!is_numeric($num) || $num <= 0){
      $num = 10;
    }
    if($total != false && is_numeric($total) && $total > 0){
        easy::$app->page->cmd('setEachNum',$num);
        easy::$app->page->cmd('setTotalNum',$total);
    }else{
      easy::$app->page->cmd('setEachNum',$num);
      $query1 = $query;
      $query1->select(['COUNT(*)'=>'total']);
      $query1->page = null; //形成死循环
      $val = $query1->createCommond($this->build($query1));
      easy::$app->page->cmd('setTotalNum',$val[0]['total']);
     }
    $page = easy::$app->page->cmd('obj');
    $query->limit = $page->getEachNum();
    $query->offset = $page->getLimitStart();
  }
  //build union
  public function buildUnion($union){
    if(empty($unions)){
      return '';
    }
    $result = '';
    foreach($unions as $i => $union){
      $query = $union['query'];
      if($query instanceof Query){
        $unions[$i]['query'] = $this->build($query);
      }
      $result .= 'UNION ' . ($union['all'] ? 'ALL' : '').'( ' . $unions[$i]['query'] . ' ) ';
    }
    return trim($result);
  }
  //build index
  public function buildIndex($value){
    return empty($value) ? '' :' USE INDEX ('.$value.') ';
  }
  public function getDb(){
    return $this->db;
  }
  public function parseAttr($attr){
      if(in_array($attr,array('LOW_PRIORITY','QUICK','IGNORE','HIGH_PRIORITY','SQL_CACHE','SQL_NO_CACHE'))){
        return $attr.' ';
    }else{
      return '';
    }
  }
  public function buildInsert(Query $query,$attr,$replace){
    $query->prepare($this);
    $values = $fields = [];
    foreach($query->insertVaules as $key=>$val){
      $value = $this->parseValue($val);
      if(is_scalar($value)){
        $values[] = $value;
        $fields[] = $key;
      }
    }
    $sql = ($replace?'REPLACE ':'INSERT ').$this->parseAttr($attr).' INTO '.$this->buildTable($query).' (`'.implode('`,`', $fields).'`) VALUES ('.implode(',', $values).')';
    return $sql;
  }
  public function buildUpdate($value,Query $query,$attr){
    $query->prepare($this);
    if(empty($value)) return false;
    //$model = $query->getModelClass();
    $sql = 'UPDATE '.$this->parseAttr($attr).$this->buildTable($query).$this->buildSet($value).$this->buildWhere($query->where);
    if(stripos($sql,'where') === false && $query->where !== true){
      return false;
    }
    return $sql;
  }
  public function buildDelete(Query $query,$attr = ''){
    $query->prepare($this);
    $sql = 'DELETE '.$this->parseAttr($attr).' FROM '.$this->buildTable($query).$this->buildWhere($query->where).$this->buildOrderBy($query->orderBy).$this->buildLimit($query);
    if(stripos($sql,'where') === false && $query->where !== true){
      return false;
    }
    return $sql;
  }
  public function buildSet($values){
    foreach($values as $key=>$val){
      $value = $this->parseValue($val);
      if(is_scalar($value)){
        $set[] = $key.'='.$value;
      }
    }
    return ' SET '.implode(',',$set);
  }

}
