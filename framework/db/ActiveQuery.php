<?php
namespace easy\db;

use easy;

class ActiveQuery extends Query{
  protected $model;
  public function __construct($model,$config = []){
     $this->model = $model;
     parent::__construct($config);
  }
  //prepare
  public function prepare($build)
  {
    if(isset($this->join)){
      //set alias
      $this->table = [$this->getPrimaryTableName()=>$this->getPrimaryTableName()]; 
    }elseif(empty($this->table)){
      $this->table = [$this->getPrimaryTableName()];
    }
  }
  //get table name
  protected function getPrimaryTableName(){
    $modelClass = $this->model;
    return $modelClass::tableName();
  }
  public function all(){
    $queryBuild = new QueryBuild(easy::$app->getDb());
    $query = $queryBuild->build($this);
    $result = $queryBuild->getDb()->getAll($query);
    return $result;
  }
  public function one($id=false){
    if($id != false && (is_numeric($id) || is_string($id))){
      $model = $this->getModelClass();
      $this->where[$model->getPk()] = $id;
    }
    $this->limit=1;
    $queryBuild = new QueryBuild(easy::$app->getDb());
    $query = $queryBuild->build($this);
    $result = $queryBuild->getDb()->getAll($query);
    if(empty($result)){
      return [];
    }
    return $result[0];
  }
  public function getModelClass(){
    return new $this->model;
  }
}
