<?php
/**
 * @link http://git.oschina.com/jackhunx/easyPHP
 */
namespace easy\db\mysql;

use PDO;
use easy;
use easy\base\Component;
use easy\base\Exception;
use easy\web\Page;
use easy\helpers\Helper;

/**
 * Class Connection.
 *@todo 添加主从数据库支持
 * this class just use for mysql by pdo extends.
 */
class Connection extends Component
{
    /**
     * @var string
     */
    public $dsn;
    /**
     * @var string
     */
    public $username;
    /**
     * @var string
     */
    public $password;
    /**
     * @var string utf-8 is default
     */
    public $charset;
    /**
     * @var
     */
    public $tablePrefix;
    /**
     * @var instance of pdo class
     */
    public $pdo;
    /**
     * @var bool
     */
    public $isOpen = false;
    /**
     * @var
     */
    public $pdoStatement;
    /**
     * array like [':parameter'=>'value'].
     *
     * @var
     */
    public $parameters;
     /**
      * [
      *   'mysql',     // MySQL
      *   'oci',       // Oracle driver
      */
    private $_driverName;
    /**
     * the number of rows affected by the last DELETE, INSERT, or UPDATE statement executed by the corresponding PDOStatement object.
     * @var int
     */
    public $_rowCount;
    /**
     * open a connection to mysql by pdo class.
     */
    public function open()
    {
        if ($this->pdo !== null) {
            return;
        }
        if (empty($this->dsn)) {
            throw new Exception('Connection::dsn can not be empty.');
        }
        try {
            $this->pdo = $this->createPdoInstace();
            $this->initConection();
        } catch (\PDOException $e) {
            throw new Exception($e->getMessage(), (int) $e->getCode(), $e);
        }
    }

    /**
     * close pdo connection.
     */
    public function close()
    {
        if ($this->pdo !== null) {
            $this->pdo = null;
        }
    }
    /**
    * Quotes a simple column name for use in a query.
    * A simple column name should contain the column name only without any prefix.
    * If the column name is already quoted or is the asterisk character '*', this method will do nothing.
    * @param string $name column name
    * @return string the properly quoted column name
    */
   public function quoteColumnName($name)
   {
       return strpos($name, '`') !== false ? $name : "`$name`";
   }

    /**
     * create an instance of pdo class.
     */
    public function createPdoInstace()
    {
        return new PDO($this->dsn, $this->username, $this->password, [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8']);
    }

    /**
     * initial connection after create pdo instance.
     */
    public function initConection()
    {
        // We can now log any exceptions on Fatal error.
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // Disable emulation of prepared statements, use REAL prepared statements instead.
        $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $this->isOpen = true;
    }

    /**
     * 1. open db connection ,if db connection not open.
     * 2. prepare query.
     * 3. bind param value.
     * 4. execute query.
     * 5. throw exception.
     * 6. reset parameters.
     *
     * @param $query query string
     * @param array $parameters
     *
     * @return true or false when execute the sql
     *
     * @throws \easy\base\Exception
     */
    public function initialize($query, $parameters = [])
    {
      //print_r($query);
        if (!$this->isOpen) {
            //if not conneted, connect.
            $this->open();
        }
        try {
            //get object pdo statement.

            $this->pdoStatement = $this->pdo->prepare($query);

            // bind param.
            $this->bindMore($parameters);
            if (!empty($this->parameters)) {
                //execute
                //$this->pdoStatement->execute($this->parameters);
                foreach ($this->parameters as $key => &$value) {
                    $this->pdoStatement->bindParam($key, $value);
                }
            }
            //destroy the reference
            unset($value); //this is important @see http://php.net/manual/en/control-structures.foreach.php#92116
            //execute.
        if(E_DEBUG) easy::$app->log->timeRecord('queryStartTime');
         $success = $this->pdoStatement->execute();
        if(E_DEBUG) easy::$app->log->timeRecord('queryEndTime');
        } catch (\PDOException $e) {
            if(E_DEBUG){
              $error = 'Db Error: '.$e->getMessage();
              easy::$app->log->errRecord($error.'<br/>'.$query);
            }else{
              throw new Exception($e->getMessage(), (int) $e->getCode(), $e);
            }
        }
        //reset paramters.
        $this->parameters = [];
        if(E_DEBUG){
          easy::$app->log->sqlRecord($query."[ RunTime:".easy::$app->log->timeRecord('queryStartTime','queryEndTime',6)."s ]");
        }
        return $success;
    }
    public function _execute($query, $parameters = [])
    {
        //prepare query
        if (!$this->isOpen) {
            $this->open();
        }
        try {
            //get object pdo statement.
            $this->pdoStatement = $this->pdo->prepare($query);

            // bind param.
            foreach ($parameters as $key => &$param) {
                if (is_null($param)) {
                    $type = PDO::PARAM_NULL;
                } elseif (is_bool($param)) {
                    $type = PDO::PARAM_BOOL;
                } elseif (is_int($param)) {
                    $type = PDO::PARAM_INT;
                } else {
                    $type = PDO::PARAM_STR;
                }
                $this->pdoStatement->bindParam(is_int($key) ? ++$key : $key, $param, $type);
            }
           //this is important @see http://php.net/manual/en/control-structures.foreach.php#92116
            //execute.
            $success = $this->pdoStatement->execute();
        } catch (\PDOException $e) {
            Helper::throwException($e->getMessage());
            //throw new Exception($e->getMessage(),(int) $e->getCode(), $e);
        }
        //reset paramters.
        //$this->parameters = [];
        return $success;
    }
    public function getLastStatement()
    {
        return $this->pdoStatement;
    }
    /**
     * @param $parameter
     * @param $value
     */
    public function bind($parameter, $value)
    {
        $this->setParameters($parameter, $value);
    }

    /**
     * @param $parameters
     */
    public function bindMore($parameters)
    {
        if (is_array($parameters) && empty($this->parameters)) {
            $this->setParameters($parameters);
        }
    }

    /**
     * @param $parameter
     * @param null $value
     *
     * @throws \easy\base\Exception
     */
    protected function setParameters($parameter, $value = null)
    {
        if (is_string($parameter) && !is_null($value)) {
            $this->parameters[$parameter] = $value;
        } elseif (is_array($parameter)) {
            $this->parameters = $parameter;
        } else {
            throw new Exception('Connection::Parameters type is not alowed this type'.gettype($parameter));
        }
        // $parameter = ":".$parameter;
    }

    /**
     *  If the SQL query  contains a SELECT or SHOW statement it returns an array containing all of the result set row
     *  If the SQL statement is a DELETE, INSERT, or UPDATE statement it returns true and write in [self::$rowCount] can get rowCount after query.
     *
     * @param $query
     * @param null $parameters
     * @param $fetchMode
     *
     * @return null|array|number
     */
    public function query($query, $parameters = null, $fetchMode = PDO::FETCH_ASSOC)
    {
        //去除前后空白字符
        $query = trim($query);
        //init
        $result = $this->initialize($query, $parameters);
        if($result){
          //get method
          $method = substr($query, 0, strpos($query, ' '));
          $statement = strtolower($method);
          if ($statement === 'select' || $statement === 'show') {
              return $this->pdoStatement->fetchAll($fetchMode);
          } elseif ($statement === 'insert'||$statement === 'delete' || $statement === 'update') {
              $this->_rowCount = 0;//防止上次查询未清除
              $this->_rowCount=$this->pdoStatement->rowCount();
              return true;
          } else {
              return false;
          }
        }else{
          return false;
        }

    }
    /**
     * returns the number of rows affected by the last DELETE, INSERT, or UPDATE statement executed by the corresponding PDOStatement object.
     * @return int
     */
    public function getRowCount(){
      return $this->_rowCount;
    }
    /**
     * 返回所有数组.
     *
     * @param string $query sql语句
     *
     * @return array 包含所有记录的数组值
     */
    public function getAll($query)
    {
        return $this->query($query);
    }
    /**
     * select 查询方法.
     *
     * @param array  $param    参数数组
     * @param object $obj_page 分页对象
     *
     * @return array 查询结果
     */
    public function select($param, $obj_page = '')
    {
        if (empty($param['field'])) {
            $param['field'] = '*';
        }
        if (empty($param['count'])) {
            $param['count'] = 'count(*)';
        }

        if (isset($param['index'])) {
            $param['index'] = 'USE INDEX ('.$param['index'].')';
        }
      //print_r($param);
      if (trim($param['where']) != '') {
          if (strtoupper(substr(trim($param['where']), 0, 5)) != 'WHERE') {
              if (strtoupper(substr(trim($param['where']), 0, 3)) == 'AND') {
                  $param['where'] = substr(trim($param['where']), 3);
              }
              $param['where'] = 'WHERE '.$param['where'];
          }
      } else {
          $param['where'] = '';
      }
        $param['where_group'] = '';
        if (!empty($param['group'])) {
            $param['where_group'] .= ' group by '.$param['group'];
        }
        $param['where_order'] = '';
        if (!empty($param['order'])) {
            $param['where_order'] .= ' order by '.$param['order'];
        }
      //判断是否是联表
        $tmp_table = explode(',', $param['table']);
        if (!empty($tmp_table) && count($tmp_table) > 1) {
            //判断join表数量和join条件是否一致
        if ((count($tmp_table) - 1) != count($param['join_on'])) {
            throw new Exception('Db Error: join number is wrong!');
        }

        //trim 掉空白字符
        foreach ($tmp_table as $key => $val) {
            $tmp_table[$key] = trim($val);
        }
        //拼join on 语句
        for ($i = 1; $i < count($tmp_table); ++$i) {
            $tmp_sql .= $param['join_type'].' `'.$this->tablePrefix.$tmp_table[$i].'` as `'.$tmp_table[$i].'` ON '.$param['join_on'][$i - 1].' ';
        }
            $sql = 'SELECT '.$param['field'].' FROM `'.$this->tablePrefix.$tmp_table[0].'` as `'.$tmp_table[0].'` '.$tmp_sql.' '.$param['where'].$param['where_group'].$param['where_order'];

        //如果有分页，那么计算信息总数
        $count_sql = 'SELECT '.$param['count'].' as count FROM `'.$this->tablePrefix.$tmp_table[0].'` as `'.$tmp_table[0].'` '.$tmp_sql.' '.$param['where'].$param['where_group'];
        } else {
            $sql = 'SELECT '.$param['field'].' FROM `'.$this->tablePrefix.$param['table'].'` as `'.$param['table'].'` '.$param['index'].' '.$param['where'].$param['where_group'].$param['where_order'];
            $count_sql = 'SELECT '.$param['count'].' as count FROM `'.$this->tablePrefix.$param['table'].'` as `'.$param['table'].'` '.$param['index'].' '.$param['where'];
        }
        //limit ，如果有分页对象的话，那么优先分页对象
        if ($obj_page instanceof Page) {
            //$count_query = self::query($count_sql,$host);
            $count_fetch = $this->query($count_sql);
        // mysqli_fetch_array($count_query,MYSQLI_ASSOC);
            $obj_page->setTotalNum($count_fetch['count']);
            $param['limit'] = $obj_page->getLimitStart().','.$obj_page->getEachNum();
        }
        if ($param['limit'] != '') {
            $sql .= ' limit '.$param['limit'];
        }
        if ($param['cache'] !== false) {
            $key = is_string($param['cache_key']) ? $param['cache_key'] : md5($sql);
            if (isset($_cache[$key])) {
                return $_cache[$key];
            }
        }

        $result = $this->query($sql);
        //self::query($sql,$host);
        if ($result === false) {
            $result = array();
        }
        // TODO: cache what
        // if ($param['cache'] !== false && !isset($_cache[$key])) {
        //     $_cache[$key] = $result;
        // }

        return $result;
    }

    public function insert($tableName, $insertArray = array())
    {
        if (!is_array($insertArray)) {
            return false;
        }
        $fields = [];
        $value = [];
        foreach ($insertArray as $key => $val) {
            $fields[] = $this->parseKey($key);
            $value[] = $this->parseValue($val);
        }
        $sql = 'INSERT INTO `'.$this->tablePrefix.$tableName.'` ('.implode(',', $fields).') VALUES('.implode(',', $value).')';
        $result = $this->query($sql);
        $insertId = $this->lastInsertId();

        return $insertId ? $insertId : $result;
    }
    /**
     * 批量插入.
     *
     * @param string $tableName   表名
     * @param array  $insertArray 待插入数据
     *
     * @return mixed
     */
    public function insertAll($tableName, $insertArray = [])
    {
        if (!is_array($insertArray[0])) {
            return false;
        }
        $fields = array_keys($insertArray[0]);
        array_walk($fields, array(self, 'parseKey'));
        $values = array();
        foreach ($insertArray as $data) {
            $value = array();
            foreach ($data as $key => $val) {
                $val = $this->parseValue($val);
                if (is_scalar($val)) {
                    $value[] = $val;
                }
            }
            $values[] = '('.implode(',', $value).')';
        }
        $sql = 'INSERT INTO `'.$this->tablePrefix.$tableName.'` ('.implode(',', $fields).') VALUES '.implode(',', $values);
        $result = $this->query($sql);
        $insertId = $this->lastInsertId();

        return $insertId ? $insertId : $result;
    }

    /**
     * 更新操作.
     *
     * @param string $tableName   [description]
     * @param array  $updateArray 更新数据
     * @param string $where       条件
     *
     * @return bool [description]
     */
    public function update($tableName, $updateArray = [], $where = '')
    {
        if (!is_array($updateArray)) {
            return false;
        }
        $stringValue = '';
        foreach ($updateArray as $k => $v) {
            if (is_array($v)) {
                switch ($v['sign']) {
            case 'increase': $stringValue .= " $k = $k + ".$v['value'].','; break;
            case 'decrease': $stringValue .= " $k = $k - ".$v['value'].','; break;
            case 'calc': $stringValue .= " $k = ".$v['value'].','; break;
            default: $stringValue .= " $k = ".$this->parseValue($v['value']).',';
          }
            } else {
                $stringValue .= " $k = ".$this->parseValue($v).',';
            }
        }

        $stringValue = trim(trim($stringValue), ',');
        if (trim($where) != '') {
            if (strtoupper(substr(trim($where), 0, 5)) != 'WHERE') {
                if (strtoupper(substr(trim($where), 0, 3)) == 'AND') {
                    $where = substr(trim($where), 3);
                }
                $where = ' WHERE '.$where;
            }
        }
        $sql = 'UPDATE `'.$this->tablePrefix.$tableName.'` AS `'.$tableName.'` SET '.$stringValue.' '.$where;
        $result = $this->query($sql);
        return $result;
    }
    /**
     * 删除.
     *
     * @param string $tableName 表名
     * @param string $where     条件
     *
     * @return bool
     */
    public function delete($tableName, $where = '')
    {
        if (trim($where) != '') {
            if (strtoupper(substr(trim($where), 0, 5)) != 'WHERE') {
                if (strtoupper(substr(trim($where), 0, 3)) == 'AND') {
                    $where = substr(trim($where), 3);
                }
                $where = ' WHERE '.$where;
            }
            $sql = 'DELETE FROM `'.$this->tablePrefix.$tableName.'` '.$where;

            return $this->query($sql);
        } else {
            throw new exception('PDO Error: the condition of delete is empty!');
        }
    }

    /**
     * 格式化键值
     *
     * @param [type] $key [description]
     *
     * @return [type] [description]
     */
    public function parseKey(&$key)
    {
        $key = trim($key);
        if (!preg_match('/[,\'\"\*\(\)`.\s]/', $key)) {
            $key = '`'.$key.'`';
        }

        return $key;
    }
    /**
     * 格式化值
     *
     * @param mixed $value
     *
     * @return mixed
     */
    public function parseValue($value)
    {
        $value = addslashes(stripslashes($value)); //重新加斜线，防止从数据库直接读取出错
      return "'".$value."'";
    }
    /**
     * return the last inserted id.
     *
     * @return mixed
     */
    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();
    }
    /**
     * 获取一行信息.
     *
     * @param array  $param
     * @param string $fields
     *
     * @return array 返回结果
     */
    public function getRow($param, $fields = '*')
    {
        $table = $param['table'];
        $wfield = $param['field'];
        $value = $param['value'];

        if (is_array($wfield)) {
            $where = array();
            foreach ($wfield as $k => $v) {
                $where[] = $v."='".$value[$k]."'";
            }
            $where = implode(' and ', $where);
        } else {
            $where = $wfield."='".$value."'";
        }

        $sql = 'SELECT '.$fields.' FROM `'.$this->tablePrefix.$table.'` WHERE '.$where;

        return $this->row($query);
    }
    /**
     * 执行REPLACE操作.
     *
     * @param string $table_name    表名
     * @param array  $replace_array 待更新的数据
     *
     * @return bool
     */
    public function replace($tableName, $replaceArray = [])
    {
        if (!empty($replaceArray)) {
            foreach ($replaceArray as $k => $v) {
                $string_field .= " $k ,";
                $string_value .= " '".$v."',";
            }
            $sql = 'REPLACE INTO `'.$this->tablePrefix.$tableName.'` ('.trim($string_field, ', ').') VALUES('.trim($string_value, ', ').')';

            return $this->query($sql);
        } else {
            return false;
        }
    }

    /**
     * 显示表结构信息.
     *
     * @param string $table
     *
     * @return array
     */
    public function showColumns($table)
    {
        $sql = 'SHOW COLUMNS FROM `'.$this->tablePrefix.$table.'`';
        $result = $this->query($sql);
        if ($result === false) {
            return array();
        }
        $array = array();
        foreach ($result as $tmp) {
            $array[$tmp['Field']] = array(
                  'name' => $tmp['Field'],
                  'type' => $tmp['Type'],
                  'null' => $tmp['Null'],
                  'default' => $tmp['Default'],
                  'primary' => (strtolower($tmp['Key']) == 'pri'),
                  'autoinc' => (strtolower($tmp['Extra']) == 'auto_increment'),
              );
        }

        return $array;
    }

    public function column($query, $params = null)
    {
        $this->initialize($query, $params);
        $columns = $this->pdoStatement->fetchColumn(PDO::FETCH_NUM);
        $column = null;
        foreach ($columns as $cells) {
            $column[] = $cells[0];
        }

        return $column;
    }

    /**
     * 返回单表查询记录数量.
     *
     * @param string $table 表名
     * @param $condition mixed 查询条件，可以为空，也可以为数组或字符串
     *
     * @return int
     */
    public function getCount($table, $condition = null)
    {
        if (!empty($condition) && is_array($condition)) {
            $where = '';
            foreach ($condition as $key => $val) {
                $this->parseKey($key);
                $val = $this->parseValue($val);
                $where .= ' AND '.$key.'='.$val;
            }
            $where = ' WHERE '.substr($where, 4);
        } elseif (is_string($condition)) {
            if (strtoupper(substr(trim($condition), 0, 3)) == 'AND') {
                $where = ' WHERE '.substr(trim($condition), 4);
            } else {
                $where = ' WHERE '.$condition;
            }
        }
        $sql = 'SELECT COUNT(*) as `count` FROM `'.$this->tablePrefix.$table.'` as `'.$table.'` '.(isset($where) ? $where : '');
        $result = $this->query($sql);
        if ($result === false) {
            return 0;
        }
        //$result = mysqli_fetch_array($result,MYSQLI_ASSOC);
        return $result['count'];
    }

    /**
     * 执行SQL语句.
     *
     * @param string $sql 待执行的SQL
     *
     * @return
     */
    public function execute($sql)
    {
        $result = $this->query($sql);

        return $result;
    }

    /**
     * 列出所有表.
     *
     * @return array
     */
    public function showTables()
    {
        $sql = 'SHOW TABLES';
        $result = $this->query($sql, $host);
        if ($result === false) {
            return array();
        }

        return $result;
    }

    /**
     * Returns an array which represents a row from the result set.
     *
     * @param $query
     * @param null $params
     * @param $fetchmode
     *
     * @return mixed
     */
    public function row($query, $params = null, $fetchmode = PDO::FETCH_ASSOC)
    {
        $this->initialize($query, $params);

        return $this->pdoStatement->fetch($fetchmode);
    }

    /**
     * Returns the value of one single field/column.
     *
     * @param $query
     * @param null $params
     *
     * @return mixed
     */
    public function single($query, $params = null)
    {
        $this->initialize($query, $params);

        return $this->pdoStatement->fetchColumn();
    }

    /**
     * 返回服务器信息.
     *
     * @return string 服务器信息
     */
    public function getServerInfo()
    {
        if ($this->isOpen === false) {
            $this->open();
        }

        return $this->pdo->getAttribute(PDO::ATTR_SERVER_INFO);
    }
    /**
     * 以下是事务处理.
     */
    public $iftransacte = true;
    /**
     * 开始事务处理 关闭自动处理.
     *
     * @return none
     */
    public function beginTransaction()
    {
        if ($this->isOpen === false) {
            $this->open();
        }
        if ($this->iftransacte) {
            $this->pdo->beginTransaction();
        }
        $this->iftransacte = false;
    }
    /**
     * 事务提交.
     *
     * @return none
     * @exception pdo error
     */
    public function commit()
    {
        if (!$this->iftransacte) {
            $result = $this->pdo->commit();
            $this->iftransacte = true;
            if (!$result) {
                $error = $this->pod->errorInfo();
                throw new Exception('PDO error :', $error[2]);
            }
        }
    }
    public function rollback()
    {
        if (!$this->iftransacte) {
            $result = $this->pdo->rollback();
            $this->iftransacte = true;
            if (!$result) {
                $error = $this->pdo->errorInfo();
                throw new Exception('PDO error :'.$error[2]);
            }
        }
    }
    public function getDriverName(){
      if($this->_driverName === null){
        if (($pos = strpos($this->dsn, ':')) !== false) {
            $this->_driverName = strtolower(substr($this->dsn, 0, $pos));
          }
      }else{
          $this->open();
          $this->_driverName=strtolower($this->pdo->getAttribute(PDO::ATTR_DRIVER_NAME));
      }
      return $this->_driverName;
    }

}
