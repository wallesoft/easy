<?php
namespace easy\db;

use easy;
use easy\base\Model;
use easy\helpers\Helper;
class ActiveRecord extends Model{

  public function __construct(){
    $this->tableInfo();
    parent::__construct();
  }
  public static function find(){
    return easy::createObject(ActiveQuery::className(),[get_called_class()]);
  }
  public function tableInfo(){
    $cache = easy::$app->get('cache',false);
    if($cache !== null){
      $tables = $cache->read('fields/_pk');
      if($tables == false){
        $tables = $this->getTablesPk();
        $cache->write('fields/_pk',$tables);
      }
    }else{
      // TODO: do better
      return null;
    }
    return $this->pk = $tables[self::tableName()];
  }
  public function getTablesPk(){
    $full_table = easy::$app->getDb()->showTables();

    $_pk_array = array();
    $count = strlen(easy::$app->db->tablePrefix);
    foreach ($full_table as $v_table) {
        $v = array_values($v_table);
        if (substr($v[0],0,$count) != easy::$app->db->tablePrefix) continue;
        $tb = preg_replace('/^'.easy::$app->db->tablePrefix.'/', '', $v[0]);
        $fields = easy::$app->getDb()->showColumns($tb);

        foreach ((array)$fields as $k=>$v) {
            if($v['primary']) {
                $_pk_array[$tb] = $k;break;
            }
        }
    }
    return $_pk_array;
  }
  //此方法根据类名返回表名，如果表名与类名不一致，在子类中重写此方法返回与表名对应的名称
  public static function tableName(){
    return Helper::camel2id(Helper::basename(get_called_class()),'_');
  }
  /**
   * update function
   */
  public static function update($value='',$condition=[],$attr = '')
  {
   $query = easy::createObject(ActiveQuery::className(),[get_called_class()]);
   $model = easy::createObject(get_called_class());
   //优先根据主键查询数据
   if(isset($value[$model->getPk()])){
     $condition[$model->getPk()] = $value[$model->getPk()];
     unset($value[$model->getPk()]);
   }
   $query->where($condition);
   $queryBuild = new QueryBuild(self::getDb());
   $sql = $queryBuild->buildUpdate($value,$query,$attr);
   return self::getDb()->execute($sql);
  }
  //delete
  public static function delete($condition,$attr = ''){
    $query = easy::createObject(ActiveQuery::className(),[get_called_class()]);

    if(is_numeric($condition) || is_string($conditon)){
      //p.s. '1' or '1,2,3,4'
      //delete by pk
      $model = easy::createObject(get_called_class());
      if(strpos($condition,',')){
        $condition = ['IN',$model->getPk(),explode(',',$condition)];
      }else{
        $condition = [$model->getPk()=>$condition];
      }
    }
    $query->where($condition);
    $queryBuild = new QueryBuild(self::getDb());
    $sql = $queryBuild->buildDelete($query,$attr);
    $result = self::getDb()->execute($sql);
    return false !== $result ? true : false;
  }


  public static function findOne($condition)
  {
    $query = easy::createObject(ActiveQuery::className(),[get_called_class()]);
    $model = easy::createObject(get_called_class());
    if(is_numeric($condition) || is_string($condtiong)){
      $conditon[$model->getPk()] = $condition;
    }
    $query->where($condtion);
    $query->limit(1);
    $queryBuild = new QueryBuild(self::getDb());
    $sql = $queryBuild->build($query);
    $result = self::getDb()->execute($sql);
    return $result[0];
  }
  /**
	 * 显示分页链接
	 *
	 * @param int $style 分页风格
	 * @return string
	 */
    public static function showPage($style = null){
    	return easy::$app->page->cmd('show',$style);
    }

	/**
	 * 获取分页总数
	 *
	 * @return string
	 */


    public static function showNowPage(){
    	return easy::$app->page->cmd('getnowpage');
    }
	/**
	 * 获取总页数
	 *
	 * @return string
	 */
    public static function getTotalNum(){
    	return easy::$app->page->cmd('gettotalnum');
    }

}
