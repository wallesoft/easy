<?php
/**
 * 核心文件
 *
 * 模型类
 * @since      File available since Release v1.0
 */
//defined('InEasyPHP') or exit('Access Invalid!');
namespace easy\db;

use easy;
use easy\base\Model;
use easy\helpers\Helper;
use easy\helpers\Cache;
//use easy\base\Exception;
class ActiveRecord extends Model{

	protected $name = '';
	protected $table_prefix = '';
	protected $init_table = null;
	protected $table_name = '';
	protected $options = array();
	protected $pk = 'id';
	protected $fields = array();
	protected $unoptions = true;	//是否清空参数项，默认清除

	public function __construct($table = null){

		if (!is_null($table)){
			$this->table_name = $table;
			$this->init_table = $this->table_name;
			$this->tableInfo($table);
		}
		$this->table_prefix = easy::$app->db->tablePrefix;
      //print_r($table);
	     parent::__construct();

	}

    /**
     * 删除表主键缓存
     */
    public static function dropTablePkArrayCache()
    {
          Cache::rm('field/_pk');
    }

    /**
     * 生成表结构信息
     *
     * @param string $table
     * @return
     */
    public function tableInfo($table)
    {
        if (empty($table)) return false;
        //只取主键,find(2)等自动匹配主键时使用
				// TODO: 此处需要优化
				$cache = easy::$app->get('cache',false);
				if($cache !== null){
					if(file_exists($cache->path.'/fields/_pk.php')){
						$this->fields = require($cache->path.'/fields/_pk.php');
					}else{
						$_pk_array = self::fetchTablePkArray();
						 Helper::file('_pk', $_pk_array, $cache->path.'/fields');
						$this->fields = $_pk_array;
					}
				}else{
					// TODO: 暂时这么处理
					return null;
				}
        return $this->fields[$table];
    }

    public static function fetchTablePkArray()
    {
        $full_table = easy::$app->getDb()->showTables();

        $_pk_array = array();
        $count = strlen(easy::$app->db->tablePrefix);
        foreach ($full_table as $v_table) {
            $v = array_values($v_table);
            if (substr($v[0],0,$count) != easy::$app->db->tablePrefix) continue;
            $tb = preg_replace('/^'.easy::$app->db->tablePrefix.'/', '', $v[0]);
            $fields = easy::$app->getDb()->showColumns($tb);

            foreach ((array)$fields as $k=>$v) {
                if($v['primary']) {
                    $_pk_array[$tb] = $k;break;
                }
            }
        }
        return $_pk_array;
    }

    public function __call($method,$args) {
        if(in_array(strtolower($method),array('table','order','where','on','limit','having','group','lock','master','distinct','index','attr','pk','key'),true)) {
              $this->options[strtolower($method)] =   $args[0];

            if (strtolower($method) == 'table'){
            	if (strpos($args[0],',') !== false){
            		$args[0] = explode(',',$args[0]);
            		$this->table_name = '';
            		foreach ((array)$args[0] as $value) {
            			$this->tableInfo($value);
            		}
            	}else{
            		$this->table_name = $args[0];
								$this->fields = array();
								$this->tableInfo($args[0]);
            	}
            }
            return $this;
        }elseif(in_array(strtolower($method),array('page'),true)){
            if ($args[0] == null){
				return $this;
			}elseif(!is_numeric($args[0]) || $args[0] <= 0){
				$args[0] = 10;
			}

			if (is_numeric($args[1]) && $args[1] > 0){
			    //page(2,30)形式，传入了每页显示数据和总记录数
			    if ($args[0] > 0){
			        $this->options[strtolower($method)] =   $args[0];
			        easy::$app->page->cmd('setEachNum',	$args[0]);
			        $this->unoptions = false;
			        easy::$app->page->cmd('setTotalNum',	$args[1]);
			        return $this;
			    }else{
			        $args[0] = 10;
			    }
			}
			$this->options[strtolower($method)] =   $args[0];
			easy::$app->page->cmd('setEachNum',	$args[0]);
			$this->unoptions = false;
			easy::$app->page->cmd('setTotalNum',	$this->get_field('COUNT(*) AS nc_count'));
        	return $this;
        }elseif(in_array(strtolower($method),array('min','max','count','sum','avg'),true)){
            $field =  isset($args[0])?$args[0]:'*';
            return $this->get_field(strtoupper($method).'('.$field.') AS nc_'.$method);
        }elseif(strtolower($method)=='count1'){
            $field =  isset($args[0])?$args[0]:'*';
            $options['field'] = ('count('.$field.') AS nc_count');
            $options =  $this->parse_options($options);
            $options['limit'] = 1;
            $result = $this->_select($options);
            if(!empty($result)) {
                return reset($result[0]);
            }
        }elseif(strtolower(substr($method,0,6))=='getby_') {
            $field   =   substr($method,6);
            $where[$field] =  $args[0];
            return $this->where($where)->find();
        }elseif(strtolower(substr($method,0,7))=='getfby_') {
            $name   =   substr($method,7);
            $where[$name] =$args[0];
            //getfby_方法只返回第一个字段值
            if (strpos($args[1],',') !== false){
            	$args[1] = substr($args[1],0,strpos($args[1],','));
            }
            return $this->where($where)->get_field($args[1]);
		}
		else if (in_array($method, array('beginTransaction','commit','rollback','checkActive'))) {
			$this->$method();
		}
		else if (in_array($method, array('gettotalnum','gettotalpage'))) {
			return easy::$app->page->cmd($method);
        }else{
            $error = 'Model: ['.get_called_class().'] Error:  Function ['.$method.'] is not exists!';
            easy\helpers\Exception::throwException($error);
            return NULL;
        }
    }
	/**
	 * 查询
	 *
	 * @param array/int $options
	 * @return null/array
	 */
    public function select($options=array()) {
        if(is_string($options) || is_numeric($options)) {
            // 默认根据主键查询
            $pk   =  $this->get_pk();
            if(strpos($options,',')) {
                $where[$pk] =  array('IN',$options);
            }else{
                $where[$pk]   =  $this->fields[$this->table_name]['_pk_type'] == 'int' ? intval($options) : $options;
            }
            $options =  array();
            $options['where'] =  $where;
        }
        $options =  $this->parse_options($options);
        if ($options['limit'] !== false) {
            if (empty($options['where']) && empty($options['limit'])){
            	//如果无条件，默认检索30条数据
            	$options['limit'] = 30;
            }elseif ($options['where'] !== true && empty($options['limit'])){
            	//如果带WHERE，但无LIMIT，最多只检索1000条记录
            	$options['limit'] = 1000;
            }
        }

        $resultSet = $this->_select($options);

        if(empty($resultSet)) {
            return array();
        }
        if ($options['key'] != '' && is_array($resultSet)){
        	$tmp = array();
        	foreach ($resultSet as $value) {
        		$tmp[$value[$options['key']]] = $value;
        	}
        	$resultSet = $tmp;
        }
        return $resultSet;
    }

	/**
	 * 取得第N列内容
	 *
	 * @param array/int $options
	 * @return null/array
	 */
    public function getfield($col = 1) {
    	if (intval($col)<=1) $col = 1;
        $options =  $this->parse_options();
        if (empty($options['where']) && empty($options['limit'])){
        	//如果无条件，默认检索30条数据
        	$options['limit'] = 30;
        }elseif ($options['where'] !== true && empty($options['limit'])){
        	//如果带WHERE，但无LIMIT，最多只检索1000条记录
        	$options['limit'] = 1000;
        }

        $resultSet = $this->_select($options);
        if(false === $resultSet) {
            return false;
        }
        if(empty($resultSet)) {
            return null;
        }
        $return = array();
        $cols = array_keys($resultSet[0]);
        foreach ((array)$resultSet as $k => $v) {
        	$return[$k] = $v[$cols[$col-1]];
        }
        return $return;
    }

    protected function parse_options($options=array()) {
        if(is_array($options)) $options =  array_merge($this->options,$options);
        if(!isset($options['table'])){
        	$options['table'] =$this->getTableName();
        }elseif(false !== strpos(trim($options['table'],', '),',')){
        	foreach(explode(',', trim($options['table'],', ')) as $val){
        		$tmp[] = $this->getTableName($val).' AS `'.$val.'`';
        	}
        	$options['table'] = implode(',',$tmp);
        }else{
        	$options['table'] =$this->getTableName($options['table']);
        }
        if ($this->unoptions === true){
        	$this->options  =   array();
        }else{
        	$this->unoptions = true;
        }
        return $options;
    }

    public function get_field($field,$sepa=null) {
        $options['field']    =  $field;
        $options =  $this->parse_options($options);
        if(strpos($field,',')) { // 多字段
            $resultSet = $this->_select($options);
            if(!empty($resultSet)) {
                $_field = explode(',', $field);
                $field  = array_keys($resultSet[0]);
                $move   =  $_field[0]==$_field[1]?false:true;
                $key =  array_shift($field);
                $key2 = array_shift($field);
                $cols   =   array();
                $count  =   count($_field);
                foreach ($resultSet as $result){
                    $name   =  $result[$key];
                    if($move) { // 删除键值记录
                        unset($result[$key]);
                    }
                    if(2==$count) {
                        $cols[$name]   =  $result[$key2];
                    }else{
                        $cols[$name]   =  is_null($sepa)?$result:implode($sepa,$result);
                    }
                }
                return $cols;
            }
        }else{
            $options['limit'] = 1;
            $result = $this->_select($options);
            if(!empty($result)) {
                return reset($result[0]);
            }
        }
        return null;
    }

	/**
	 * 返回一条记录
	 *
	 * @param string/int $options
	 * @return null/array
	 */
    public function find($options=null) {
        if(is_numeric($options) || is_string($options)) {
            $where[$this->get_pk()] = $options;
            $options = array();
            $options['where'] = $where;
        }elseif(!empty($options)) {
        	return false;
        }
        $options['limit'] = 1;
        $options =  $this->parse_options($options);
        $result = $this->_select($options);
        if(empty($result)) {
            return array();
        }
        return $result[0];
    }
	/**
	 * 删除
	 *
	 * @param array $options
	 * @return bool/int
	 */
    public function delete($options=array()) {
        if(is_numeric($options)  || is_string($options)) {
            // 根据主键删除记录
            $pk   =  $this->get_pk();
            if(strpos($options,',')) {
                $where[$pk]   =  array('IN', $options);
            }else{
                $where[$pk]   =  $this->fields['_pk_type'] == 'int' ? intval($options) : $options;
                $pkValue = $options;
            }
            $options =  array();
            $options['where'] =  $where;
        }
        $options =  $this->parse_options($options);
        $result =   $this->_delete($options);
        return false !== $result ? true : false;
    }
	/**
	 * 更新
	 *
	 * @param array $data
	 * @param array $options
	 * @return boolean
	 */
    public function update($data='',$options=array()) {
        if(empty($data)) return false;
        // 分析表达式
        $options =  $this->parse_options($options);
        if(!isset($options['where'])) {
            // 如果存在主键,自动作为更新条件
            if(isset($data[$this->get_pk()])) {
                $pk   =  $this->get_pk();
                $where[$pk]   =  $data[$pk];
                $options['where']  =  $where;
                $pkValue = $data[$pk];
                unset($data[$pk]);
            }else{
                return false;
            }
        }
        $result = $this->_update($data,$options);
        return false !== $result ? true : false;
    }

	/**
	 * 插入
	 *
	 * @param array $data
	 * @param bool $replace
	 * @param array $options
	 * @return mixed int/false
	 */
    public function insert($data='', $replace=false, $options=array()) {
        if(empty($data)) return false;
        $options =  $this->parse_options($options);
        $result = $this->_insert($data,$options,$replace);
        if(false !== $result ) {
            $insertId   =   $this->getLastId();
            if($insertId) {
                return $insertId;
            }
        }
        return $result;
    }



	/**
	 * 批量插入
	 *
	 * @param array $dataList
	 * @param array $options
	 * @param bool $replace
	 * @return boolean
	 */
    public function insertAll($dataList,$replace=false,$options=array()){
        if(empty($dataList)) return false;
        // 分析表达式
        $options =  $this->parse_options($options);
        // 写入数据到数据库
        $result = $this->_insertAll($dataList,$options,$replace);
        if(false !== $result ) return true;
        return $result;
    }

    /**
     * 取得表名
     *
     * @param string $table
     * @return string
     */
	protected function getTableName($table = null){
		if (is_null($table)){
			$return = '`'.$this->table_prefix.$this->table_name.'`';
		}else{
			$return = '`'.$this->table_prefix.$table.'`';
		}
		return $return;
	}
    /**
     * 指定查询字段 支持字段排除
     *
     * @param mixed $field
     * @param boolean $except
     * @return Model
     */

	public function field($field)
	{
		if (true === $field) {
			$field = "*";
		}

		$this->options['field'] = $field;
		return $this;
	}

	/**
     * 组装join
     *
     * @param string $join
     * @return Model
     */
    public function join($join) {
    	if (false !== strpos($join,',')){
    		foreach (explode(',',$join) as $key=>$val) {
		    	if (in_array(strtolower($val),array('left','inner','right'))){
		    		$this->options['join'][] = strtoupper($val).' JOIN';
		    	}else{
		    		$this->options['join'][] = 'LEFT JOIN';
		    	}
    		}
    	}elseif (in_array(strtolower($join),array('left','inner','right'))){
    		$this->options['join'][] = strtoupper($join).' JOIN';
    	}
        return $this;
    }
	public function setInc($field, $step=1) {
        return $this->set_field($field,array('exp',$field.'+'.$step));
    }

    public function setDec($field,$step=1) {
        return $this->set_field($field,array('exp',$field.'-'.$step));
    }

    public function set_field($field,$value='') {
        if(is_array($field)) {
            $data = $field;
        }else{
            $data[$field]   =  $value;
        }
        return $this->update($data);
    }

	/**
	 * 显示分页链接
	 *
	 * @param int $style 分页风格
	 * @return string
	 */
    public function showpage($style = null){
    	return easy::$app->page->cmd('show',$style);
    }

	/**
	 * 获取分页总数
	 *
	 * @return string
	 */


    public function shownowpage(){
    	return easy::$app->page->cmd('getnowpage');
    }
	/**
	 * 获取总页数
	 *
	 * @return string
	 */
    public function gettotalnum(){
    	return easy::$app->page->cmd('gettotalnum');
    }

    /**
     * 清空MODEL中的options、table_name属性
     *
     */
    public function cls(){
    	$this->options = array();
    	$this->table_name = '';
    	return $this;
    }
    /**
	 * 直接SQL查询,返回查询结果
	 *
	 * @param string $sql
	 * @return array
	 */
	public function query($sql){
		return $this->dbConnection->getAll($sql);
	}

	/**
	 * 执行SQL，用于 更新、写入、删除操作
	 *
	 * @param string $sql
	 * @return
	 */
    public function execute($sql){
    	return $this->dbConnection->execute($sql);
    }

    /**
     * 开始事务
     *
     * @param string $host
     */
    public  function beginTransaction($host = 'master'){
    	$this->dbConnection->beginTransaction($host);
    }

    /**
     * 提交事务
     *
     * @param string $host
     */
    public function commit($host = 'master'){
    	$this->dbConnection->commit($host);
    }

    /**
     * 回滚事务
     *
     * @param string $host
     */
    public function rollback($host = 'master'){
    	$this->dbConnection->rollback($host);
    }

    /**
     * 清空表
     *
     * @return boolean
     */
    public function clear(){
    	if (!$this->table_name && !$this->options['table']) return false;
    	$options =  $this->parse_options();
    	return $this->_clear($options);
    }


    /**
     * 取得最后插入的ID
     *
     * @return int
     */
    public function getLastId() {
        return $this->_getLastId();
    }

    /**
     * 取得数据表字段信息
     *
     * @return mixed
     */
    public function getFields(){
        if($this->fields) {
            $fields   =  $this->fields;
            unset($fields['_autoinc'],$fields['_pk'],$fields['_type']);
            return $fields;
        }
        return false;
    }



	/**
	 * 取得主键
	 *
	 * @return string
	 */
    public function get_pk() {
    	return isset($this->fields[$this->table_name])?$this->fields[$this->table_name]:$this->pk;
    }

   /**
    * 检查非数据字段
    *
    * @param array $data
    * @return array
    */
     protected function chk_field($data) {
        if(!empty($this->fields[$this->table_name])) {
            foreach ($data as $key=>$val){
                if(!in_array($key,$this->fields[$this->table_name],true)){
                    unset($data[$key]);
                }
            }
        }
        return $data;
     }



    public function checkActive($host = 'master') {
        $this->_checkActive($host);
    }

}
