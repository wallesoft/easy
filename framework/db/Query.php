<?php
/**
 * Query Build
 */
namespace easy\db;

use easy;
use easy\base\Component;

class Query extends Component {

  protected $sql; // sql

  public $select; // select conditio

  public $table;

  public $where;

  public $orderBy;

  public $groupBy;

  public $having;

  public $limit;

  public $offset;

  public $join;

  public $distinct;

  public $index;

  public $page;

  public $insertVaules;
  // prepare
  public function prepare($build)
  {
    //if(!empty($this->table)){
      //  $this->getTabaleName($this->table,$build);
    //}
    return $this;
  }
  //real table name require table prefix
  //protected function getTabaleName($table,$build){
  //     return '`'.$build->tablePrefix.$table.'`';
   //}
  /**
   * select columns
   * @param  string $columns 'id,name,title' or '*' select columns all
   * @return Object          return this object
   */
  public function select($columns = '*',$distinct = false){
    if(true === $columns){
      $columns = '*';
    }
    $this->select = $columns;
    $this->distinct = $distinct;
    return $this;
  }
  /**
   * table
   * @param  string $table tableName
   * @return object        this object
   */
  public function from($table){
    $this->table = $table;
    return $this;
  }
  /**
   * where
   * @param  Array $condition condition
   * @return object            this object
   */
  public function where($condition,$params=[]){
      $this->where = $condition;
      return $this;
  }
  /**
   * order by
   */
  public function orderBy($columns){
    $this->orderBy = $columns;
    return $this;
  }
  /**
   * group by
   */
  public function groupBy($columns){
    if(!is_array($columns)){
      $columns = preg_split('/\s*,\s*/', trim($columns), -1, PREG_SPLIT_NO_EMPTY);
    }
    $this->groupBy = $columns;
    return $this;
  }
  /**
   * having
   */
  public function having($condition){
    $this->having = $condition;
    return $this;
  }
  /**
   * limit
   */
  public function limit($limit)
  {
    $this->limit = $limit;
    return $this;
  }
  //offset
  public function offset($offset){
    $this->offset = $offset;
    return $this;
  }
  /**
   * page
   * 1.just set num that every page show p.s. page(10)
   * 2.set num and total p.s. page(10,20)
   * @param  num  $num   每页显示 default 10
   * @param  mixed $total 总数
   * @return object   this
   */
  public function page($num = 10,$total = false){
    $this->page = [$num,$total];
    return $this;
  }
  /**
   * join
   */
  public function join($type,$table,$on='')
  {
    $this->join[] = [$type,$table,$on];
    return $this;
  }
  //index
  public function index($value)
  {
    $this->index = $value;
    return $this;
  }

  public function toSql()
  {
   (new QueryBuild(easy::$app->getDb()))->build($this);
   return $this;
  }
  public function getRawSql(){
    return $this->sql;
  }
  public function setSql($sql){
    $this->sql = $sql;
    return $this;
  }
  public function createCommond($sql=null){
   return easy::$app->getDb()->query($sql);
  }
  public function setInsertValues($values){
    $this->insertVaules = $values;
  }
  // the last function
  public function update($data,$attr=''){
    $queryBuild = new QueryBuild(easy::$app->getDb());
    $sql  = $queryBuild->buildUpdate($data,$this,$attr);
    return $queryBuild->getDb()->execute($sql);
  }
  public function all(){
    $queryBuild = new QueryBuild(easy::$app->getDb());
    $query = $queryBuild->build($this);
    $result = $queryBuild->getDb()->getAll($query);
    return $result;
  }
  public function one(){
    $this->limit=1;
    $queryBuild = new QueryBuild(easy::$app->getDb());
    $query = $queryBuild->build($this);
    $result = $queryBuild->getDb()->getAll($query);
    if(empty($result)){
      return [];
    }
    return $result[0];
  }
  public function delete($attr = ''){
    $queryBuild = new QueryBuild(easy::$app->getDb());
    $sql  = $queryBuild->buildDelete($this,$attr);
    $result =  $queryBuild->getDb()->execute($sql);
    return false !== $result ? true : false;
  }
}
