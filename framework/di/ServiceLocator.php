<?php

/**
 * serveic locator to use before module after components
 */
namespace easy\di;

use easy;
use easy\base\Component;

/**
 *
 * * To use ServiceLocator, you first need to register component IDs with the corresponding component
 * definitions with the locator by calling [[set()]] or [[setComponents()]].
 * You can then call [[get()]] to retrieve a component with the specified ID. The locator will automatically
 * instantiate and configure the component according to the definition.
 *
 * For example,
 *
 * ```php
 * $locator = new \easy\di\ServiceLocator;
 * $locator->setComponents([
 *     'db' => [
 *         'class' => 'easy\db\Connection',
 *         'dsn' => 'sqlite:path/to/file.db',
 *     ],
 *     'cache' => [
 *         'class' => 'easy\caching\FileCache',
 *     ],
 * ]);
 *
 * $db = $locator->get('db');  // or $locator->db
 * $cache = $locator->get('cache');  // or $locator->cache
 * ```
 *
 * Because [[\easy\base\Module]] extends from ServiceLocator, modules and the application are all service locators.
 *
 */
class ServiceLocator extends Component{
  /**
   * @var 根据描述生成的component实例.
   */
  private $_components = [];
  /**
   * @var component 的相关配置信息.
   */
  private $_definitions = [];
  /**
   * return an object
   *
   * if definitions[$id] is a configuration array
   * create object the return.
   *
   * @param $id component id.
   * @param bool $throwException default is true.
   * @throws exception
   * @return mixed object component instance.
   */
  public function get($id,$throwException=true){

      if(isset($this->_components[$id])){
          //instance
          return $this->_components[$id];
      }

      if(isset($this->_definitions[$id])){
         $definition = $this->_definitions[$id];
          if(is_object($definition)){
              return $this->_components[$id] = $definition;
          }else{
              return $this->_components[$id] = easy::createObject($definition);
          }
      }elseif($throwException){
          // TODO: 更改此处抛出异常
          throw new \Exception("Unknow Component ID:$id");
      }else{
          return null;
      }



  }
  /**
   * 1、配置app的相关配置
   * 2、生成相关app实例
   * 3、获取app instance 请用[[get()]]
   *
   * @param string $id the component id .
   * @param mixed $definition array or an object.
   * @throws Exception if configuration is unexpected.
   */
  public function set($id,$definition){
      if($definition===null){
          //如果没有任何配置去除相关配置
          unset($this->_components[$id],$this->_definitions[$id]);
      }
      //是否已经实例化，去除原实例化对象
      unset($this->_components[$id]);
      if(is_object($definition)){
          //a object
          $this->_definitions[$id]=$definition;
      }elseif(is_array($definition)&&isset($definition['class'])){
          //a configuration array value with class name
          $this->_definitions[$id]=$definition;
      }else{
          //not set use definition
          throw new Exception("Unexpected configuration.");
      }
  }

  /**
   * @param $components
   */
  public function setComponents($components){
      foreach($components as $id=>$component){
          //是否已经生成实例
          $this->set($id,$component);
      }

  }

}
